'use strict'

const gulp = require('gulp');
const gaze = require('gaze');
const parameterized = require('gulp-parameterized');
const del           = require('del');
const edit = require('gulp-json-editor');
const $    = require('gulp-load-plugins')();


const form = 'src/assets';
const to = 'dist/assets';

function lazyRequireTask(taskName, path, options){
    options = options || {}; 
    options.taskName = taskName;
    gulp.task(taskName, function(callback){
        let task = require(path).call(this, options);
        return task(callback)
    })
}

function getManifestFileName(params){
    return params.manifest_file_name || 'manifest.json';
}


/************GET PLUGINS START************/

lazyRequireTask('app:plugins', './tasks/get.plugins.js', {
    dest: './plugins/npm'
});

lazyRequireTask('app:grid', './tasks/smart.grid.js', {
    dest: `${form}/sass/mixins`,
    settings: {
        outputStyle: 'sass',
        columns: 12, 
        offset: '30px', 
        mobileFirst: false, 
        container: {
            maxWidth: '1200px', 
            fields: '15px' 
        },
        breakPoints: {
            lg: {
                width: '1199px', 
            },
            md: {
                width: '991px'
            },
            sm: {
                width: '767px'
            },
            xs: {
                width: '480px'
            }
        }
    }
});

/************************/


/************GET MEDIA START************/

lazyRequireTask('ico', './tasks/ico.js', {
    src: `${form}/ico/ico.png`,
    dest: `${to}/ico`
});
lazyRequireTask('img', './tasks/copy.file.js', {
    src: `${form}/img/**/*.{png,jpg,gif}`,
    dest: `${to}/img`

});

lazyRequireTask('img:svg', './tasks/copy.file.js', {
    src: `${form}/img/**/*.svg`,
    dest: `${to}/img`,
    path: ''
});

lazyRequireTask('pictures', './tasks/copy.file.js', {
    src: `${form}/pictures/**/*.{png,jpg,gif}`,
    dest: `${to}/pictures`
});

lazyRequireTask('pictures:svg', './tasks/copy.file.js', {
    src: `${form}/pictures/**/*.svg`,
    dest: `${to}/pictures`
});

lazyRequireTask('sprite:svg', './tasks/sprites.svg.js', {
    src: `${form}/sprites-svg/*.svg`,
    dest: `${to}`
});

lazyRequireTask('json', './tasks/copy.file.js', {
    src: `${form}/json/*.json`,
    dest: `${to}/json`
});

lazyRequireTask('sprite:clean', './tasks/sprite.clean.js', {
    del: [
        'dist/assets/img/sprite-*.svg' 
    ],
});


lazyRequireTask('fonts:local', './tasks/copy.file.js', {
    src: `${form}/fonts/**/*`,
    dest: `${to}/fonts`
});

lazyRequireTask('fonts:icons', './tasks/icons.js', {
    src: `${form}/icons/*.svg`,
    dest: `${to}/fonts`,
    path: `template.fonts.icons/_fonts.icons.scss`,
});


gulp.task('app:media', 
    gulp.parallel(
		'ico',
		'img',
		'img:svg',
        'sprite:svg',
        'json',
        'pictures',
		'pictures:svg',
        'fonts:icons',
        'fonts:local',
    ) 
);

/************************/

/*************GET STYLES START*************/
lazyRequireTask('lib:styles', './tasks/sass.lib.js', {
    src: `${form}/sass/libs.sass`,
    dest: `${to}/css`
});


lazyRequireTask('css:styles', './tasks/sass.js', {
    src: [`${form}/sass/app.sass`],
    dest: `${to}/css`
});



gulp.task('app:styles', 
    gulp.series(
        'lib:styles',
        'css:styles',
    ) 
);

/************************/


/*************GET SCRIPTS START*************/

lazyRequireTask('lib:scripts', './tasks/scripts.lib.js', {
    src: `${form}/js/libs.js`,
    uglify: `${to}/js/libs.js`,
    dest: `${to}/js`
});

lazyRequireTask('js:scripts', './tasks/scripts.js', {
    src: [`${form}/js/app.js`, `!${form}/js/libs.js`],
    dest: `${to}/js`
});

gulp.task('app:scripts', 
    gulp.series(
        'lib:scripts',
        'js:scripts'
    ) 
);

/************************/



/*************GET TEMPLATE START*************/

lazyRequireTask('app:tpl', './tasks/haml.js', {
    src: `./src/*.haml`,
    dest: `./dist`
});

/************************/



/*************CLEAN -> BUILD -> SERVER  START*************/

lazyRequireTask('clean', './tasks/clean.js', {
    del: [
        './dist', 
        './src/assets/sass/base/_fonts.icons.scss',
        './src/assets/sass/mixins/smart-grid.sass',
        './src/assets/sass/base/sprite.scss'
    ],
});

gulp.task('build', 
    gulp.series(
        'clean',
        gulp.parallel(
            'app:grid',
            'app:media',
        ),
        'app:styles',
        'app:scripts',
        'app:tpl'
    ) 
);




lazyRequireTask('server', './tasks/reload.js', {
    server: './dist',
    allFiles: './dist/**/*.*'
});


gulp.task('production', gulp.series(
    parameterized(function(cb, params) {
        return del(`${params.path}/assets/**/*`, {force: true});
    }),
    'build', 
    parameterized(function(cb, params) {
        return gulp.src(['dist/assets/css/*.*', 'dist/assets/fonts/*.*', 'dist/assets/js/*.*'], {base: 'dist/assets'})
            .pipe($.rev())
            .pipe(gulp.dest('dist/tmp/assets'))
            .pipe($.rev.manifest(getManifestFileName(params)))
            .pipe(gulp.dest('dist/tmp/original'))
            .pipe(edit(function(manifest) {
                let ret = manifest;

                if(params.base_manifest_path){
                    ret = {};

                    Object.keys(manifest).forEach(function (path, orig) {
                        ret[params.base_manifest_path+path] = manifest[path];
                    });
                }

                return ret;
            }))
            .pipe(gulp.dest('dist/tmp'))
    }),
    parameterized(function(cb, params) {
        return gulp.src(['dist/assets/ico/**/*.*', 'dist/assets/img/**/*.*'], {base: 'dist/assets'})
            .pipe(gulp.dest('dist/tmp/assets'))
    }),
    parameterized(function(cb, params) {
       return gulp.src(`dist/tmp/**/*.{css,js}`)
           .pipe($.revReplace({
               manifest: gulp.src(`dist/tmp/original/${getManifestFileName(params)}`)
           }))
           .pipe(gulp.dest(`dist/tmp`))
    }),
    parameterized(function(cb, params) {
       return gulp.src(`dist/tmp/assets/**/*.*`, {base: 'dist/tmp'})
           .pipe(gulp.dest(params.path))
    }),
    parameterized(function(cb, params) {
       return gulp.src(`dist/tmp/${getManifestFileName(params)}`)
           .pipe(gulp.dest(params.manifest))
    })
    
));




/************************/

gulp.task('watch', function(){

    /*************MEDIA WATCH*************/

    gaze(`${form}/ico/ico.png`, function(err, watcher) {
       this.on('all', gulp.series('ico'));
    });
    
    gaze(`${form}/img/**/*.{png,jpg,gif}`, function(err, watcher) {
       this.on('all', gulp.series('img'));
    });

    gaze(`${form}/img/**/*.svg`, function(err, watcher) {
       this.on('all', gulp.series('img:svg'));
    });

    gaze(`${form}/sprites-svg/**/*`, function(err, watcher) {
       this.on('all', gulp.series('sprite:clean', 'sprite:svg', 'css:styles'));
    });
    
    gaze(`${form}/pictures/**/*.{png,jpg,gif}`, function(err, watcher) {
       this.on('all', gulp.series('pictures'));
    });

    gaze(`${form}/pictures/**/*.svg`, function(err, watcher) {
       this.on('all', gulp.series('pictures:svg'));
    });

    gaze(`${form}/fonts/**/*`, function(err, watcher) {
       this.on('all', gulp.series('fonts:local'));
    });

    gaze(`${form}/icons/**/*`, function(err, watcher) {
       this.on('all', gulp.series('fonts:icons', 'css:styles'));
    });



    

    /*************STYLES WATCH*************/
    gaze(`${form}/json/*.json`, function(err, watcher) {
       this.on('all', gulp.series('json'));
    });

    gaze(`${form}/sass/libs.sass`, function(err, watcher) {
       this.on('all', gulp.series('lib:styles'));
    });

    gaze([`${form}/sass/**/*.sass`, `!${form}/sass/**/libs.sass`], function(err, watcher) {
       this.on('all', gulp.series('css:styles'));
    });



    /*************SCRIPTS WATCH*************/

    gaze(`${form}/js/libs.js`, function(err, watcher) {
       this.on('all', gulp.series('lib:scripts'));
    });

    gaze([`${form}/js/**/*.js`, `!${form}/js/libs.js`], function(err, watcher) {
       this.on('all', gulp.series('js:scripts'));
    });

    
    /*************TEMPLATE WATCH*************/

    //gaze(`./src/**/*.haml`, function(err, watcher) {
    //  this.on('all', gulp.series('app:tpl'));
    //});

    gulp.watch(['./src/*.haml', './src/components/*.haml'], gulp.series('app:tpl'));
    

});


gulp.task('default',  
    gulp.series(
        'build', 
        gulp.parallel('watch', 'server')
    )
);


