##How to build project:

Static development build:

```bash
gulp build
```

Dynamic development build (with 'watch' and frontend server):

```bash
gulp 
```

Production build:

```bash
gulp production --path "..." --manifest  "..." --manifest_file_name "..." --base_manifest_path "..."
```

Production build options:

**--path** (required) - path of the building directory 

**--manifest** (required) - path of the manifest file

**--manifest_file_name** - name of the manifest file

**--base_manifest_path** - base path of assets inside the manifest file

Example of production build :
```bash
gulp production --path "../../../public/assets/front" --manifest  "../../../public/assets/front/assets" --manifest_file_name "mix-manifest.json" --base_manifest_path "/assets/front/assets/"
```