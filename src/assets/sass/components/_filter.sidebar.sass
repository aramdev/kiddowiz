.filterSidebar
    position: relative
    z-index: 9
    padding-left: 30px
    @media(max-width: 991px)
        padding: 0 20px 30px
    .js-bordForm
        height: 100px
        background: #f00
    &__map
        padding: 80px 10px
        margin-bottom: 20px
        background: url('../img/map.jpg') 0 0 no-repeat
        background-size: cover
        text-align: center
    &__form 
        padding-bottom: 30px
        .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default
            width: 12px
            height: 12px
            border-radius: 12px
            background: $prim
            border: none
            &:focus
                outline: none
        .ui-widget-content
            height: 3px
            background: #ccc
        .ui-slider .ui-slider-range
            background: $prim
        
    &__dateRange
        position: relative
        overflow: hidden
        height: 0
        opacity: 0
        padding-bottom: 0
        transition: opacity 150ms linear
        &.active
            overflow: visible
            height: auto
            opacity: 1
            padding-bottom: 20px
        .icn-calendar-date
            margin-top: 0
            top: 2px
    &__range
        margin: 0 12px
    &__showHide
        overflow: hidden
        height: 0
        opacity: 0
        transition: all 150ms linear
        &.active
            height: auto
            overflow: visible
            opacity: 1
    &__more
        font-size: 16px
        font-weight: bold
        color: #FF5E1C
        display: -webkit-flex
        display: -moz-flex
        display: -ms-flex
        display: -o-flex
        display: flex
        align-items: center
        margin-bottom: 10px
        cursor: pointer
        span
            margin: 0 3px
        &:hover
            color: #FF5E1C
        i
            margin-right: 10px
    &__label
        font-size: 15px
        opacity: 0.4  
        color: #272727
        position: absolute
        left: 7px
        top: 50%
        margin-top: -21px
        pointer-events: none
        & + .field
            padding-left: 45px
    &__reset
        display: block
        margin-top: 15px
        text-align: center
        font-weight: bold
        &:hover
            text-decoration: none
    .field-case
        .daterangepicker 
            width: 690px
            border-radius: 5px
            background-color: #FFFFFF
            padding: 30px 30px 20px
            box-shadow: inset 0 -1px 0 0 #CCCCCC, 0 2px 5px 0 rgba(0,0,0,0.1), 0 10px 30px 0 rgba(0,0,0,0.2)
            z-index: 3000
            position: absolute
            @media(max-width: 991px)
                position: relative
                width: 100%
                padding: 10px
                top: auto !important
            .calendar-table 
                th
                    color: #81838C 
                    padding: 0
                    font-size: 12px
                    padding-bottom: 8px
                    min-width: 1px
                    @media(max-width: 991px)
                        font-size: 10px
                    &.available
                        span
                            border-color: #83858E
                        &:hover
                            background: none
                            span
                                border-color: #5642ab
                td
                    padding: 0
                    color: #272727
                    border-radius: 0
                    border: 1px solid #fff
                    height: 42px
                    min-width: 1px
                    font-size: 14px
                    @media(max-width: 991px)
                        font-size: 12px
                        height: 32px
                        width: 32px

                    &.weekend
                        color: #5642AB
                    &.off.available,
                    &[class="off off disabled"],
                    &[class="weekend off off disabled"]
                        pointer-events: none
                        opacity: 0

                    &[class="off disabled"]
                        pointer-events: auto
                        opacity: 1
                        text-decoration: none
                        color: rgba(51,51,51,0.5)
                    &[class="weekend off disabled"]
                        color: #5642AB
                        opacity: 0.5
                        text-decoration: none
                    &.active
                        background-color: #CCC4DE
                        &:hover
                            color: #272727
                    &.in-range
                        background-color: #F1EDF9
                    &.end-date, &.start-date
                        background-color: #CCC4DE
                    &.start-date.end-date
                        border-radius: 0
                thead tr:last-child
                    th
                        padding-bottom: 8px
                    th:last-child,
                    th:first-child
                        color: #5642AB
            &.show-calendar 
                .drp-buttons
                    display: -webkit-flex
                    display: -moz-flex
                    display: -ms-flex
                    display: -o-flex
                    display: flex
                    align-items: center
                    padding: 15px 0 0 0
                    @media(max-width: 991px)
                        flex-direction: column
                        flex-wrap: wrap
                    .drp-selected
                        color: #272727
                        font-size: 15px
                        @media(max-width: 991px)
                            text-align: center
                            margin: 0
                            padding: 0
                    .btns
                        margin-left: 5px
                        @media(max-width: 991px)
                            width: 100%
                            margin: 15px 0 0 0
            div.drp-calendar
                padding: 0
                width: 48%
                max-width: 48%
                margin-bottom: 20px
                @media(max-width: 991px)
                    width: 100%
                    max-width: 100%
                &.left
                    float: left
                    .calendar-table
                        padding: 0 
                &.right
                    float: right
            .drp-selected
                margin-right: auto