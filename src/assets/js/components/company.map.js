if($("#companyListMap").length > 0){
var geojson = {
    "type": "FeatureCollection",
    "features": [
        {
            "type": "Feature",
            "properties": {
                "id": "ak16994521",
                "message": {
                    "title": 'Advanture Out 3',
                    "price": '$160-$320',
                    "age": 'age: 8-14',
                },
                //"iconSize": [60, 60]
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -100.017, 45.457
                ]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "id": "ak16994527",
                "message": {
                    "title": 'Advanture Out 1',
                    "price": '$160-$320',
                    "age": 'age: 8-15',
                },
                //"iconSize": [50, 50]
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                   -90.017, 32.457
                ]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "id": "ak16994532",
                "message": {
                    "title": 'Advanture Out 6',
                    "price": '$160-$320',
                    "age": 'age: 8-15',
                },
                //"iconSize": [50, 50]
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                   -91, 33
                ]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "id": "ak16994525",
                "message": {
                    "title": 'Advanture Out 2',
                    "price": '$160-$320',
                    "age": 'age: 8-16',
                },
                //"iconSize": [40, 40]
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -111.017, 39.457
                ]
            }
        }
    ]
};


var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v9',
    center: [-100.017, 39.457],
    zoom: 3.4
    
});

map.addControl(new mapboxgl.NavigationControl());
if ("ontouchstart" in document.documentElement){
    map.dragPan.disable();
}else{
    map.dragPan.enable();
}

map.on('load', function() {
    // Add a new source from our GeoJSON data and set the
    // 'cluster' option to true. GL-JS will add the point_count property to your source data.
    map.addSource("earthquakes", {
        type: "geojson",
        // Point to GeoJSON data. This example visualizes all M1.0+ earthquakes
        // from 12/22/15 to 1/21/16 as logged by USGS' Earthquake hazards program.
        data: geojson,
        cluster: true,
        clusterMaxZoom: 14, // Max zoom to cluster points on
        clusterRadius: 50 // Radius of each cluster when clustering points (defaults to 50)
    });

    map.addLayer({
        id: "clusters",
        type: "circle",
        source: "earthquakes",
        filter: ["has", "point_count"],
        paint: {
            // Use step expressions (https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-step)
            // with three steps to implement three types of circles:
            //   * Blue, 20px circles when point count is less than 100
            //   * Yellow, 30px circles when point count is between 100 and 750
            //   * Pink, 40px circles when point count is greater than or equal to 750
            "circle-color": [
                "step",
                ["get", "point_count"],
                "#ff5e1c",
                100,
                "#ff5e1c",
                750,
                "#ff5e1c"
            ],
            "circle-radius": [
                "step",
                ["get", "point_count"],
                20,
                100,
                30,
                750,
                40
            ]
        }
    });

    map.addLayer({
        id: "cluster-count",
        type: "symbol",
        source: "earthquakes",
        filter: ["has", "point_count"],
        layout: {
            "text-field": "{point_count_abbreviated}",
            "text-font": ["DIN Offc Pro Medium", "Arial Unicode MS Bold"],
            "text-size": 12,

        }
    });

    map.addLayer({
        id: "unclustered-point",
        type: "circle",
        source: "earthquakes",
        filter: ["!", ["has", "point_count"]],
        paint: {
            "circle-color": "#ff5e1c", // 5642ab
            "circle-radius": 5,
            "circle-stroke-width": 0,
            "circle-stroke-color": "#ff5e1c"
        }
    });

    // inspect a cluster on click
    map.on('click', 'clusters', function (e) {
        var features = map.queryRenderedFeatures(e.point, { layers: ['clusters'] });
        var clusterId = features[0].properties.cluster_id;
        map.getSource('earthquakes').getClusterExpansionZoom(clusterId, function (err, zoom) {
            if (err)
                return;

            map.easeTo({
                center: features[0].geometry.coordinates,
                zoom: zoom
            });
        });

        
    });


   

    let pointId = "";
    map.on('click', 'unclustered-point', function (e) {

        let features = map.queryRenderedFeatures(e.point, { layers: ['unclustered-point'] });
        let data = JSON.parse(features[0].properties.message)
        
        let $elem = $(".eventsList__map .moreInfo")
        let $container = $elem.find(".moreInfo__content")
        if(pointId != features[0].properties.id){
            $container.html("")
            $container.html(`
                        <div class="companyBox">
                            <div class="companyBox__head">
                                <a class="companyBox__img" href="">
                                    <img alt="alt" src="assets/img/pop-logo.png" srcset="assets/img/pop-logo.png 1x, assets/img/pop-logo.png 2x" title="title">
                                </a>
                                <h4 class="companyBox__title">
                                    <a href="">${data.title}</a>
                                </h4>
                            </div>
                            <div class="companyItem__info">
                                <div class="companyItem__events">
                                    EVENTS:
                                    <span>22</span>
                                </div>
                                <div class="rate">
                                    <i class="icn-star active"></i>
                                    <i class="icn-star active"></i>
                                    <i class="icn-star active"></i>
                                    <i class="icn-star"></i>
                                    <i class="icn-star"></i>
                                </div>
                                <a class="commentsCount companyItem__comments" href="">
                                    <i class="icn-comments"></i>
                                    3
                                </a>
                            </div>
                            <div data-height="180px" data-scrollBar="slimeScroll" id="scrollText">
                                <p class="companyBox__text">
                                    Since our founding in 2004, the mission of Adventure Out has been simple: to help people fall back in love with nature. We accomplish this providing programs of the highest quality and safety standards that get people outdoors, physically active, and adventuring in nature. By providing this time and space for people to escape the hustle-and-bustle of daily life, we accomplish what we consider to be some of most important environmental work of all-helping people to reconnect with our natural world.
                                </p>
                            </div>
                            <div class="companyBox__tags">
                                <a class="tag" href="">Outdoor Activities</a>
                                <a class="tag" href="">SPORT</a>
                                <a class="tag" href="">TEAM SPORT</a>
                            </div>
                            <div class="companyBox__address" data-height="240px" data-scrollBar="slimeScroll" id="scrollAddress">
                                <span>
                                    <i class="icn-map"></i>
                                    <strong>175 Hilpert Square Apt. 059</strong>
                                </span>
                                <span>
                                    <i class="icn-map"></i>
                                    45 Wilburn Groves Suite 004
                                </span>
                                <span>
                                    <i class="icn-map"></i>
                                    18 Jensen Drives
                                </span>
                                <span>
                                    <i class="icn-map"></i>
                                    175 Hilpert Square Apt. 059
                                </span>
                                <span>
                                    <i class="icn-map"></i>
                                    45 Wilburn Groves Suite 004
                                </span>
                                <span>
                                    <i class="icn-map"></i>
                                    18 Jensen Drives
                                </span>
                                <span>
                                    <i class="icn-map"></i>
                                    175 Hilpert Square Apt. 059
                                </span>
                                <span>
                                    <i class="icn-map"></i>
                                    45 Wilburn Groves Suite 004
                                </span>
                                <span>
                                    <i class="icn-map"></i>
                                    18 Jensen Drives
                                </span>
                            </div>
                            <a class="btns-block btns btns-primary companyBox__btn" href="">Show Events</a>
                        </div>
                                           
            `)
            pointId = features[0].properties.id
        }
        moduleNumberInput.initPopovers()
        modulePopovers.init()
        

        $elem.find('[data-scrollBar="slimeScroll"]').each(function(index, el) {
            let itemId = $(this).attr('id');
            let scrollBar = new SlimeScroll(`#${itemId}`)
            scrollBar.init()
        });
        $elem.addClass('active')


        let x = 0;  
        if(window.innerWidth > 767){
            x = 200
        }

        map.flyTo({
            center: features[0].geometry.coordinates,
            offset: [x, 0],
        });

        
    });

    
    let popup = new mapboxgl.Popup({ 
            offset: 15,
            closeButton: false,
            
        })

    
    map.on('mouseenter', 'unclustered-point', function (e) {
        var features = map.queryRenderedFeatures(e.point, { layers: ['unclustered-point'] });
        let data = JSON.parse(features[0].properties.message)
        
        map.getCanvas().style.cursor = 'pointer'
        popup.setLngLat(features[0].geometry.coordinates)
           .setHTML(`
                   <div class="mapPopup">
                       <div class="mapPopup__title">${data.title}</div>
                        <div class="mapPopup__price">${data.price}</div>
                        <div class="mapPopup__age">${data.age}</div>  
                   </div>
               `)
           .addTo(map);

    });

    map.on('mouseout', 'unclustered-point', function (e) {
        map.getCanvas().style.cursor = '';
        popup.remove();

    });


    
    
    map.on('mouseenter', 'clusters', function () {
        map.getCanvas().style.cursor = 'pointer';
    });
    map.on('mouseleave', 'clusters', function () {
        map.getCanvas().style.cursor = '';
    });

    $('.js-closeMoreInfo').click(function(e) {
        let $elem = $(this).parents(".moreInfo")
        let $container = $elem.find(".moreInfo__content")
        $container.html("")
        $elem.removeClass('active')
        pointId = ""
    });

});

}

