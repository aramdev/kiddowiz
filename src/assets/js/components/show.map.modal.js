
var moduleShowMapModal = {
    cacheDom() {
        this.$btn = $('.js-showMapModal')
        this.$modal = $('#showMapModalModal')
    },
	toggleScheldules (e) {
        this.$modal.modal('show')
    },
    setMap () {

        var geojson = this.$btn.attr('data-info');
        geojson = JSON.parse(geojson)

        this.$modal.find('.modals__title').text(geojson[0].features[0].properties.message.title)
        var center = geojson[0].features[0].geometry.coordinates 
        
        var map = new mapboxgl.Map({
            container: 'mapModal',
            style: 'mapbox://styles/mapbox/streets-v9',
            center: center,
            zoom: 10,
        });
        map.addControl(new mapboxgl.NavigationControl());
        map.scrollZoom.disable();
        if ("ontouchstart" in document.documentElement){
            map.dragPan.disable();
        }
        else{
            map.dragPan.enable();
        }
        geojson[0].features.forEach(function(marker) {
            var el = document.createElement('div');
            el.className = 'marker';
            el.style.backgroundImage = 'url(assets/img/map.svg)';
            el.style.width =  '17.5px';
            el.style.height = '25px';

            let msg = marker.properties.message
            var popup = new mapboxgl.Popup({ 
                    offset: 25,
                    closeButton: false,
                    
                })
                
            el.addEventListener('mouseenter', function() {
                popup.setLngLat(marker.geometry.coordinates)
                   .setHTML(`
                           <div class="mapPopup">
                               <div class="mapPopup__title">${msg.title}</div>
                               <div class="mapPopup__price">${msg.price}</div>
                               <div class="mapPopup__age">${msg.age}</div>  
                           </div>
                       `)
                   .addTo(map);

            });  
            el.addEventListener('mouseout', function() {
                map.getCanvas().style.cursor = '';
                popup.remove();
            });   

            new mapboxgl.Marker(el)
                .setLngLat(marker.geometry.coordinates)
                .addTo(map);
        });
    },
    removeMap(){
        this.$modal.find('.modals__title').html("")
        this.$modal.find('#mapModal').removeClass('mapboxgl-map').html("")
    },
    events () {
        this.$btn.on('click', this.toggleScheldules.bind(this));
        this.$modal.on('shown.bs.modal', this.setMap.bind(this));
        this.$modal.on('hide.bs.modal', this.removeMap.bind(this));
    },
    init () {
        this.cacheDom()
        this.events()
    }
}
if($('.js-showMapModal').length > 0){
    moduleShowMapModal.init()
}