var moduleDashboard = {
    cacheDom() {
        this.$body = $('body')
        this.$closeguide = $('.js-closeguide')


        this.$win = $(window);
        this.$load = $('.loading');
        this.loadProcess = false;
    },
	
    timeDetect(){
        let $el = $('.js-title')
        let now = new Date();
        let hours = now.getHours()
        
        if(hours >= 2 && hours < 6){
            $el.prepend('Hi')
            $el.append(' Still awake?')
            return false
        }

        if(hours >= 6 && hours < 11){
            $el.prepend('Good Morning,')
            return false
        }

        if(hours >= 11 && hours < 16){
            $el.prepend('Good Afternoon,')
            return false
        }
        if(hours >= 16 && hours < 23){
            $el.prepend('Good Evening,')
            return false
        }
        if(hours = 23 || (hours >= 0 && hours < 2)){
            $el.prepend('Good Night,')
            return false
        }
    },
    removeGuide(e){
        let $el = $(e.currentTarget)
        let $parent = $el.parents('.dashboard__guides');
        let count = $parent.find('.guide').length
        
        if(count <= 1){
            $parent.remove()
        }else{
            $el.parent('.guide').remove()
        }
    },
    loading () {
        let scrollTop = this.$win.scrollTop()
        let elTop = this.$load.offset().top
        let h = this.$win.height()
        if(scrollTop > elTop - h){
            this.$load.addClass('active')
            if(this.loadProcess == false){
                this.loadProcess = true
                setTimeout(() => {
                    $('.dashboard__new').append(`
                        <div class="block">
                            <div class="h4 day">May 5, 2018</div>
                            <div class="item listItem">
                                <div class="item__head">
                                    <span class="name">Sheba K.</span>
                                    <span class="to">wrote a review to</span>
                                    <a class="link link-primary" href="">Kiddle Academy of Hamilton Park</a>
                                    <span class="time">7:02pm</span>
                                </div>
                                <div class="item__content">
                                    <p>
                                        My LO has been going to Kiddie Academy of Hamilton for over a year and half now and she absolutely loves it. I have never had a day when she said she didn’t want to go to her school…
                                    </p>
                                    <a class="reply" href="">
                                        <i class="icn-reply"></i>
                                        Read and Reply
                                    </a>
                                </div>
                            </div>
                            <div class="item listItem">
                                <div class="item__head">
                                    <span class="name">Sheba K.</span>
                                    <span class="to">wrote a review to</span>
                                    <a class="link link-primary" href="">Kiddle Academy of Hamilton Park</a>
                                    <span class="time">7:02pm</span>
                                </div>
                                <div class="item__content">
                                    <p>
                                    </p>
                                </div>
                                <a class="show" href="">
                                    <i class="icn-bars"></i>
                                    Show Current Orders
                                </a>
                            </div>
                            <div class="item listItem">
                                <div class="item__head">
                                    <span class="name">Sheba K.</span>
                                    <span class="to">wrote a review to</span>
                                    <a class="link link-primary" href="">Kiddle Academy of Hamilton Park</a>
                                    <span class="time">7:02pm</span>
                                </div>
                                <div class="item__content">
                                    <p>
                                    </p>
                                </div>
                                <a class="show" href="">
                                    <i class="icn-bars"></i>
                                    Show Current Orders
                                </a>
                            </div>
                            <div class="item listItem">
                                <div class="item__head">
                                    <span class="name">Sheba K.</span>
                                    <span class="to">wrote a review to</span>
                                    <a class="link link-primary" href="">Kiddle Academy of Hamilton Park</a>
                                    <span class="time">7:02pm</span>
                                </div>
                                <div class="item__content">
                                    <p>
                                        Thank You!
                                    </p>
                                    <a class="reply" href="">
                                        <i class="icn-reply"></i>
                                        Read and Reply
                                    </a>
                                </div>
                            </div>
                        </div>
                    `)
                    this.$load.removeClass('active')
                    this.loadProcess = false
                }, 1000)
            }
        }
    },
    events () {
        this.$win.on('scroll', this.loading.bind(this));
        this.$closeguide.on('click', this.removeGuide.bind(this));
    },
    init () {
        this.cacheDom()
        this.timeDetect()
        this.events()
    }
}
if($('.dashboard').length > 0){
    moduleDashboard.init()
}


