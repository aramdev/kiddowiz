var moduleSelect = {
    cacheDom() {
        this.$select = $('.js-select')
    },
	selectInit () {
        this.$select.each(function () {
            $(this).select2({
                dropdownParent: $(this).parents(".field-case"),
                minimumResultsForSearch: -1
            }).on("change", function (e) {
                $(this).valid(); //jquery validation script validate on change
            })
        });
	},    
    events () {},
    init () {
        this.cacheDom()
        this.selectInit()
    }
}
if($('.js-select').length > 0){
    moduleSelect.init()
}