var moduleAddEvent = {
    cacheDom() {
        this.$body = $("body")
        this.$form = $('#addCompanyForm')


        this.$addLinkModal = $('#addSocialLinkModal')
        this.$confirmLinkModal = $('#confirmSocialLinkModal')
        this.$addLinkForm = $('#addSocialLink')
        this.linkSubmit = false;
        this.links = []

        this.$addDocModal = $('#addDocModal')
        this.$confirmDocModal = $('#confirmDocModal')
        this.$addDocForm = $('#addDoc')
        this.docSubmit = false;
        this.docs = []


        this.$addLocationModal = $('#addLocationModal')
        this.$confirmLocationModal = $("#confirmLocationModal")
        this.$removeLocationModal = $("#removeLocationModal")
        this.$addLocationForm = $('#addLocation')
             

        this.locationSubmit = false;
        this.locations = []



        this.$html = $('html, body');
        this.$head = $('.header');

        

        this.map = false;
        this.marker = false;
    },
    sendForm (e){
        let $el = $(e.currentTarget)
        let status = $el.attr('data-status');
        if(status == 'new'){
            $('.js-next').remove()
            $('[data-next]').show()
            $el.attr('data-status', "old")

            setTimeout(() => {
                let head = this.$head.outerHeight();
                let pos = $('#nextStep').offset().top - head - 30
                this.$html.animate({scrollTop:pos+'px'}, 700);
            }, 20)

            return false
        }
    },
    
    
    counterString (e) {

        let $el = $(e.currentTarget)
        let $cnt =  $('.addEvent__max')
        let max =  $cnt.attr('data-max');
        max = parseInt(max)
        let count = $el.val().length
        let sum = max - count

        
        if(sum >= 0){
            $cnt.addClass('text-success').removeClass('text-danger')
        }else{
            $cnt.addClass('text-danger').removeClass('text-success')
        }

        $cnt.find('span').text(sum) 
    },
    // Add links
    addLink (e){
        e.preventDefault()
        let $el = $(e.currentTarget)
        let status = $el.attr('data-status');
        
        if($el.valid()){
            if(status == 'new'){
                let item =  {}
                item.url = $el.find('[name="url"]').val() 
                item.title = $el.find('[name="title"]').val() 
                item.ind = this.links.length + item.title
                this.links.push(item) 


            }else{
                let id = $el.find('[name="ind"]').val()
                let num = ""

                this.links.forEach(function(element, i) {
                    if(element.ind == id){
                       num = i 
                    }
                });

                this.links[num].title = $el.find('[name="title"]').val()
                this.links[num].url = $el.find('[name="url"]').val()
            }
            notific.setNote('icn-success',
                'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'reviewInfo-success')
            this.renderLink()
            this.linkSubmit = true
            this.detectLinkConfirm()
            this.$addLinkModal.modal('hide')
        }
    },
    renderLink(){
        let link = ""
        for(let item of this.links){
            link += `
                <div class="addEventSocLink">
                    <i class="icn-youtube"></i>
                    <div class="cnt">
                        <div class="title">${item.title}</div>
                        <a class="link link-primary" href="${item.url}">
                            ${item.url}
                        </a>
                    </div>
                    <div class="control">
                        <a class="addCompany__edit js-linkEdit"  data-item="${item.ind}" tabindex="">
                            <i class="icn-edit"></i>
                            Edit
                        </a>
                        <a class="addCompany__remove js-linkRemove"  data-item="${item.ind}" tabindex="">
                            <i class="icn-close"></i>
                            Remove
                        </a>
                    </div>
                </div>
            `
        }
        $('.addEventSocLinks').html('')
        $('.addEventSocLinks').html(link)
    },
    linkEdit (e) {
        let $el  = $(e.currentTarget)
        let id = $el.attr('data-item')

        let found
        this.links.forEach(function(element) {
            if(element.ind == id){
                found = element
            }
        }); 

        this.$addLinkForm.attr('data-status', 'edit');
        this.$addLinkForm.find('[name="title"]').val(found.title)
        this.$addLinkForm.find('[name="url"]').val(found.url)
        this.$addLinkForm.find('[name="ind"]').val(found.ind)
        this.$addLinkForm.find('.btns').val('Edit Link')
        this.$addLinkModal.find('.modals__title').text('Edit Link')

        

        this.$addLinkModal.modal('show')

        this.renderLink()
    },
    linkRemove (e) {
        let $el  = $(e.currentTarget)
        let item = $el.attr('data-item')
        
        $('#removeSocialLinkModal').find('.js-linkDel').attr('data-item', item);
        $('#removeSocialLinkModal').modal('show');
    },
    linkDel (e) {
        let $el  = $(e.currentTarget)
        let item = $el.attr('data-item')
        
        
        let num = "";
        this.links.forEach(function(element, i) {
            if(element.ind == item){
               num = i 
            }
        });
        this.links.splice(num, 1) 
        this.renderLink()

        $('#removeSocialLinkModal').one('hidden.bs.modal', function(){
            notific.setNote('icn-success',
                'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'reviewInfo-success')
        });
        $('#removeSocialLinkModal').modal('hide');
    },
    resetLinkForm (e) {
        this.$addLinkForm.find('.resetButton').trigger('click')
        this.$addLinkForm.find('.btns').val('Add Link').attr('disabled', 'disabled');
        this.$addLinkForm.attr('data-status', 'new');
        this.$addLinkModal.find('.modals__title').text('Add Link')
        setTimeout(() => {
            this.$addLinkForm.find('.field').removeClass('error')
            this.$addLinkForm.find('label.error').hide()
        }, 300)
    },
    detectLinkConfirm () {
        if(this.linkSubmit == true){
            setTimeout(() => {
                this.resetLinkForm()
                this.linkSubmit = false
            }, 1000)
        }else{
            if(!(this.$addLinkForm.find('input[type="submit"]').is('[disabled]'))){
                this.$confirmLinkModal.modal('show')
            }else{
                setTimeout(() => {
                    this.resetLinkForm()
                    this.linkSubmit = false
                }, 1000)
            }
        }
    },
    doneLink () {
        this.$confirmLinkModal.modal('hide')
        this.resetLinkForm()
    },
    cancelLink () {
        this.$confirmLinkModal.one('hidden.bs.modal', () => {
            this.$addLinkModal.modal('show')
        });
        this.$confirmLinkModal.modal('hide')
    },
    // Add links

    
    // Add Location
    createNewMap () {
        this.$addLocationModal.one('shown.bs.modal', () => {
            this.setNewMap()
        });
        this.$addLocationModal.modal('show')
    },
    setNewMap () {
        let $this = this
        let map;
        let marker;

        function getLocation () {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(setCurren);
            } else { 
                alert("Geolocation is not supported by this browser.")
            }
        }



        function setCurren (position) {
            let long = position.coords.longitude
            let lat = position.coords.latitude
            let center
            if(long && lat){
                center = [long, lat]
            }else{
                center = [-100.017, 45.457]
            }

            

            map = new mapboxgl.Map({
                container: 'locationMap',
                style: 'mapbox://styles/mapbox/streets-v9',
                center: center,
                zoom: 15
            });
            map.addControl(new mapboxgl.NavigationControl(), "bottom-left");
            
            $this.$addLocationModal.find('[name="coordinates"]').val(center)


            marker = new mapboxgl.Marker({
                draggable: true,
                color: '#ff5e1c',
            })
            .setLngLat(center)
            .addTo(map);

            marker.on('dragend', function(){
                var lngLat = marker.getLngLat();
                $this.$addLocationModal.find('[name="coordinates"]').val([lngLat.lng, lngLat.lat])
            });



            $this.map = map
            $this.marker = marker
        }


        getLocation()
    },
    detectLocation (e) {
        let $el = $(e.currentTarget)
        let location = this.$addLocationModal.find('[name="location"]').val() 
        
        let businessStAddress = this.$addLocationModal.find('[name="businessStAddress"]').val()
        if(businessStAddress != ""){
            businessStAddress = + ", "
        }
        
        let code = this.$addLocationModal.find('[name="code"]').val() 
        if(code != ""){
            code = " " + code
        }
        
        let query = businessStAddress + location  + code

        

        let $this = this
        let map = this.map
        let marker = this.marker

        var mapboxClient = mapboxSdk({ accessToken: mapboxgl.accessToken });
        mapboxClient.geocoding.forwardGeocode({
            query: query,
            autocomplete: false,
            limit: 1
        })
            .send()
            .then(function (response) {
                if (response && response.body && response.body.features && response.body.features.length) {
                    var feature = response.body.features[0];

                    $this.$addLocationModal.find('[name="coordinates"]').val(feature.center)

                    $this.marker.remove()
                    marker = new mapboxgl.Marker({
                        draggable: true,
                        color: '#ff5e1c',
                    })
                    .setLngLat(feature.center)
                    .addTo(map);

                    marker.on('dragend', function(){
                        var lngLat = marker.getLngLat();
                        $this.$addLocationModal.find('[name="coordinates"]').val([lngLat.lng, lngLat.lat])
                    });

                    map.flyTo({
                        center: feature.center,
                    });


                    $this.map = map
                    $this.marker = marker

                }
            });

    },

    changeInput (e){
        let $el = $(e.currentTarget)
        let name = $el.attr('name');
        let $form = $el.parents('form')

        if(name == "hasEmail"){

            if($el.is(':checked')){
                $form.find('[name="email"]').removeAttr('disabled')
                $form.find('[name="email"]').prev('.field-name').append('<span>*</span>')
                   
            }else{
                $form.find('[name="email"]').attr('disabled', 'true').removeClass('error').val('');
                $form.find('[name="email"]').parents('.field-case').find('label.error').remove()
                $form.find('[name="email"]').prev('.field-name').find('span').remove()


            }

            $form.trigger('change')
            $el.val($el.prop('checked'))

            return false
        }

        if(name == "hasPhone"){

            if($el.is(':checked')){
                $form.find('[name="phone"]').removeAttr('disabled')
                $form.find('[name="phone"]').prev('.field-name').append('<span>*</span>')
                
            }else{
                $form.find('[name="phone"]').attr('disabled', 'true').removeClass('error').val('');
                $form.find('[name="phone"]').parents('.field-case').find('label.error').remove();
                $form.find('[name="phone"]').prev('.field-name').find('span').remove()

                
            }

            $form.trigger('change')
            $el.val($el.prop('checked'))

            return false
        }

        if(name == "hasWebsite"){

            if($el.is(':checked')){
                $form.find('[name="website"]').removeAttr('disabled')
                $form.find('[name="website"]').prev('.field-name').append('<span>*</span>')
            }else{
                $form.find('[name="website"]').attr('disabled', 'true').removeClass('error').val('');
                $form.find('[name="website"]').parents('.field-case').find('label.error').remove();
                $form.find('[name="website"]').prev('.field-name').find('span').remove()

            }
            
            $form.trigger('change')
            $el.val($el.prop('checked'))

            return false
        }
    },

    addLocation (e){
        e.preventDefault()
        let $el = $(e.currentTarget)
        let status = $el.attr('data-status');
        
        if($el.valid()){
            
            let item =  {}
            

            item.coordinates = $el.find('[name="coordinates"]').val()
            item.title = $el.find('[name="title"]').val()
            item.location = $el.find('[name="location"]').val()
            item.code = $el.find('[name="code"]').val()
            item.businessStAddress = $el.find('[name="businessStAddress"]').val()


            

            
            
            item.hasEmail = $el.find('[name="hasEmail"]').val()

            
            if($el.find('[name="email"]').val() != ""){
                item.email = $el.find('[name="email"]').val()    
            }else{
                item.email = "admin@admin.ru"
            }

            
            
            item.hasPhone = $el.find('[name="hasPhone"]').val()
            if($el.find('[name="phone"]').val() != ""){
                item.phone = $el.find('[name="phone"]').val()
            }else{
                item.phone = "+1 (564) 654-6546"
            }

            item.hasWebsite = $el.find('[name="hasWebsite"]').val()
            if($el.find('[name="website"]').val() != ""){
                item.website = $el.find('[name="website"]').val()
            }else{
                item.website = 'https://www.google.ru/'
            }
            

            item.ind = this.locations.length + 'location'

            this.locations.push(item) 
            this.getLocations()


            notific.setNote('icn-success',
                'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'reviewInfo-success')
            this.locationSubmit = true
            this.detectLocationConfirm()
            this.$addLocationModal.modal('hide')
        }
    },

   

    resetLocationForm (e) {
        this.$addLocationForm.find('.resetButton').trigger('click')
        this.$addLocationForm.find('.btns[type="submit"]').val('Add Location').attr('disabled', 'disabled');
        this.$addLocationForm.attr('data-status', 'new');
        this.$addLocationModal.find('.modals__title').text('Add New Locatin')

        this.$addLocationForm.find('#geocoder').html("")
        this.$addLocationForm.find('#locationMap').html("").removeClass("mapboxgl-map")

        this.$addLocationForm.find('[name="email"]').attr('disabled', true);
        this.$addLocationForm.find('[name="phone"]').attr('disabled', true);
        this.$addLocationForm.find('[name="website"]').attr('disabled', true);

        this.$addLocationForm.find('[name="email"]').prev('.field-name').find('span').remove()
        this.$addLocationForm.find('[name="phone"]').prev('.field-name').find('span').remove()
        this.$addLocationForm.find('[name="website"]').prev('.field-name').find('span').remove()

        setTimeout(() => {
            this.$addLocationForm.find('.field').removeClass('error')
            this.$addLocationForm.find('label.error').hide()

            this.map = false;
            this.marker = false;
        }, 300)
    },

    detectLocationConfirm () {
        if(this.locationSubmit == true){
            setTimeout(() => {
                this.resetLocationForm()
                this.locationSubmit = false
            }, 1000)
        }else{
            if(!(this.$addLocationForm.find('input[type="submit"]').is('[disabled]'))){
                this.$confirmLocationModal.modal('show')
            }else{
                setTimeout(() => {
                    this.resetLocationForm()
                    this.locationSubmit = false
                }, 1000)
            }
        }
    },

    doneLocation () {
        this.$confirmLocationModal.modal('hide')
        this.resetLocationForm()
    },
    cancelLocation () {
        this.$confirmLocationModal.one('hidden.bs.modal', () => {
            this.$addLocationModal.modal('show')
        });
        this.$confirmLocationModal.modal('hide')
    }, 


    // Add Location

    addPhoto (e) {
        let $el = $(e.currentTarget)
        let $parent = $el.parents('.photoAdd')
        let done = 0

        for(let img of e.target.files){
            let type = img.name.split(".")
            let ext = type[type.length - 1]
            
            if(ext == 'jpg' || ext == 'png' || ext == 'jpeg'){
                let url = URL.createObjectURL(img)
                $parent.before(`
                    <div class="photo">
                        <img alt="" src="${url}">
                        <i class="icn-close"></i>
                        <a class="main" tabindex="">Main Photo</a>
                    </div>
                `); 
                done++
            }

        }

        if(done > 0){
            notific.setNote('icn-success',
                `You uploaded ${done} photos`,
                'reviewInfo-success')    
        }else{
            notific.setNote('icn-wrong',
                'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'reviewInfo-wrong')
        }
        
        

        
        
    },

    removePhoto(e) {
        let $el = $(e.currentTarget)
        $el.parents('.photo').remove()
    },
    setMainPhoto(e) {

        let $el = $(e.currentTarget)
        $('.addEvent__photos .photo').each(function(index, el) {
            if($(this).find('a.main').length == 0){
                $(this).append('<a class="main" tabindex="">Main Photo</a>')
            }
        });
        $el.remove() 
       
    },
    getLocations(e){
        let $cnt = $('.addEvent__locations')
        $cnt.append('<div class="loader-circle"></div>')


        setTimeout(function(){
            $cnt.parents('form').find('.btns[type="submit"]').attr('disabled', 'disabled'); 
            let html = ` <label class="field-name">
                        Select Location
                        <span>*</span>
                        <i class="icn-info-solid" data-container="element" data-placement="left" data-toggle="tooltip" title="hint for you"></i>
                    </label>
                    <select class="field js-select" data-msg-required="Please enter select Location." name="addressed" required="true">
                        <option value="">Select One...</option>
                        <option value="1">
                            5200 35th Ave SW, Seattle, WA 98121
                        </option>
                        <option value="2">
                            5200 35th Ave SW, Seattle, WA 98122
                        </option>
                        <option value="3">
                            5200 35th Ave SW, Seattle, WA 98123
                        </option>
                        <option value="4">
                            5200 35th Ave SW, Seattle, WA 98124
                        </option>
                        <option value="5">
                            5200 35th Ave SW, Seattle, WA 98125
                        </option>
                        <option value="6">
                            5200 35th Ave SW, Seattle, WA 98126
                        </option>
                        <option value="7">
                            5200 35th Ave SW, Seattle, WA 98127
                        </option>
                        <option value="8">
                            5200 35th Ave SW, Seattle, WA 98128
                        </option>
                        <option value="9">
                            5200 35th Ave SW, Seattle, WA 98129
                        </option>
                        <option value="10">
                            5200 35th Ave SW, Seattle, WA 981210
                        </option>
                    </select> `
            $cnt.html(html)


            $cnt.find('select').select2({ 
                dropdownParent: $cnt,
                minimumResultsForSearch: -1
            }).on("change", function (e) {
                $(this).valid(); //jquery validation script validate on change
            })
        }, 1000)

        
    },
    events () {
        this.$form.on('submit', this.sendForm.bind(this));
        
        this.$addLinkForm.on('submit', this.addLink.bind(this));
        this.$body.on('click', '.js-linkEdit', this.linkEdit.bind(this));
        this.$body.on('click', '.js-linkRemove', this.linkRemove.bind(this));
        this.$body.on('click', '.js-linkDel', this.linkDel.bind(this));
        this.$addLinkModal.on('hidden.bs.modal', this.detectLinkConfirm.bind(this));
        this.$body.on('click', '.js-doneLink   ', this.doneLink.bind(this));
        this.$body.on('click', '.js-cancelLink', this.cancelLink.bind(this));
        
        

        

        this.$addLocationForm.on('submit', this.addLocation.bind(this));
        this.$addLocationModal.on('hidden.bs.modal', this.detectLocationConfirm.bind(this));
        this.$body.on('click', '.js-doneLocation', this.doneLocation.bind(this));
        this.$body.on('click', '.js-cancelLocation', this.cancelLocation.bind(this));
        this.$body.on('change', '.js-location', this.detectLocation.bind(this));

        
        this.$body.on('click', '.js-addNewLocation', this.createNewMap.bind(this));
        this.$body.on('change', '#addLocationModal [name="hasEmail"]', this.changeInput.bind(this));
        this.$body.on('change', '#addLocationModal [name="hasPhone"]', this.changeInput.bind(this));
        this.$body.on('change', '#addLocationModal [name="hasWebsite"]', this.changeInput.bind(this));


        this.$body.on("input change", ".js-counterStr", this.counterString.bind(this))
        this.$body.on("change", ".photoAdd input", this.addPhoto.bind(this))
        this.$body.on("click", ".addEvent__photos .photo .icn-close", this.removePhoto.bind(this))
        this.$body.on("click", ".addEvent__photos .photo .main", this.setMainPhoto.bind(this))
        this.$body.on("change", ".js-getLocations", this.getLocations.bind(this))

    },
    init () {
        this.cacheDom()
        this.renderLink()
        this.events()

    }
}
if($('.addEvent').length > 0){
    moduleAddEvent.init()
}



