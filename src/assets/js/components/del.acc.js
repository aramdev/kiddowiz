var moduleDelAcc = {
    cacheDom() {
        this.radio = $('[name="dellAcc"]')
    },
    otherArea(e){
        let $el = $(e.currentTarget)
        console.log($el.val())
        if($el.val() == "other"){
            $('#dellAcc').find('textarea').removeAttr('disabled')
        }else{
            $('#dellAcc').find('textarea').attr('disabled', 'disabled')
        }
    },
    events () {
        this.radio.on("change", this.otherArea.bind(this))
    },
    init () {
        this.cacheDom()
        this.events()
    }
}
if($('#dellAcc').length > 0){
    moduleDelAcc.init()
}