if($('#searchBy').length > 0){
	var feedbackModule = new Vue({
		el: '#searchBy',
		data: {
			state: '',
			category: '',
			option: 'Events',

			states: [
				{ 
					"name": "Ajo",
					"abbreviation": "AZ"
				},
				{ 
					"name": "Avondale",
					"abbreviation": "AZ"
				},
				{ 
					"name": "Bisbee",
					"abbreviation": "AZ"
				},
				{ 
					"name": "Casa Grande",
					"abbreviation": "AZ"
				},
				{ 
					"name": "Chandler",
					"abbreviation": "AZ"
				},
				{ 
					"name": "Clifton",
					"abbreviation": "AZ"
				},
				{ 
					"name": "Douglas",
					"abbreviation": "AZ"
				},
				{ 
					"name": "Flagstaff",
					"abbreviation": "AZ"
				}
			],
			categories: [
			    {
			        "name": "Air Activities",
			        "icon": "air-activities"
			    },
			    {
			        "name": "Cafe",
			        "icon": "cafe"
			    },
			    {
			        "name": "Cinema & Teater",
			        "icon": "cinema-theater"
			    },
			    {
			        "name": "Cirques",
			        "icon": "cirques"
			    },
			    {
			        "name": "Computers",
			        "icon": "computers"
			    }
			],

			select: [
				{name: "Events"},
				{name: "Companies"}
			],
			
			shwoStates: false,
			shwoCategories: false,
			showSelect: false,

			currentStates: [],
			currentCategories: [],

			latitude: '',
			longitude: '',



		},
		methods: {
			getCategories(){
				this.currentCategories = this.categories
				this.shwoCategories = true
				this.shwoStates = false
				this.showSelect = false
			},
			
			filterCategories(){
				this.currentCategories = []
				let app = this;
				let categories = app.categories;
			    let result = categories.filter(function(categories) {
			        let regex = new RegExp('(' + app.category + ')', 'i');
			        return categories.name.match(regex);
			    })

			    if(result.length != 0){
			    	this.currentCategories = result
			    }else{
			    	this.currentCategories.push({
			    		name: this.category,
			    		icon: ''
			    	})
			    }
			}, 
			setCategory(category){
				this.category = category;
				this.shwoCategories = false;
				this.currentCategories = [];
			},

			getStates(){
				this.currentStates = this.states
				this.shwoStates = true
				this.shwoCategories = false
				this.showSelect = false
			},
			filterStates(){
				this.currentStates = []
				let app = this;
				let states = app.states;
			    let result = states.filter(function(states) {
			        let regex = new RegExp('(' + app.state + ')', 'i');
			        return states.name.match(regex);
			    })

			    if(result.length != 0){
			    	this.currentStates = result
			    }else{
			    	this.currentStates.push({
			    		name: this.state,
			    		abbreviation: ''
			    	})
			    }
			    
			},
			setState(name, abbreviation){
				this.state = `${name}, ${abbreviation}`;
				this.shwoStates = false;
				this.currentStates = [];
			},
			hideList() {
				this.shwoStates = false;
				this.currentStates = [];
				this.shwoCategories = false;
				this.currentCategories = [];
				this.showSelect = false;

			},
			stopPrep(){},
			getLocation() {
				if (navigator.geolocation) {
					navigator.geolocation.getCurrentPosition(this.setCurren);
				} else { 
					alert("Geolocation is not supported by this browser.")
				}
			},
			setCurren(position){
				this.state = 'Seattle, WA'
				this.hideList()
				this.latitude = position.coords.latitude
				this.longitude = position.coords.longitude
				console.log(this.latitude)
				console.log(this.longitude)
			},
			getSelect(){
				this.showSelect = !this.showSelect;
				this.shwoCategories = false
				this.shwoStates = false
			},
			setSelect(name){
				this.option = name
				this.showSelect = false;
			}
		},
		created() {
			window.addEventListener('click', this.hideList)
		},
	})
}