var moduleEventSchedules = {
    cacheDom() {
        this.$check = $('.js-scheldules')
        this.$block = $('.eventItem__block')
        
    },
	toggleScheldules (e) {
        if($(e.currentTarget).prop("checked") == true){
            this.$block.addClass('active')
        }else{
            this.$block.removeClass('active')
        }
    },
    events () {
        this.$check.on('change', this.toggleScheldules.bind(this));
    },
    init () {
        this.cacheDom()
        this.events()
    }
}
if($('.js-scheldules').length > 0){
    moduleEventSchedules.init()
}