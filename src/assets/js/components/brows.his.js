var moduleBrowsHis = {
    cacheDom() {
        this.$body = $('body')
        this.$del = $('.js-deleteReview')
        this.$modalDel = $('#deleteReview')
        this.delItem = ""



        this.$win = $(window);
        this.$load = $('.loading');
        this.loadProcess = false;
    },
	remove (e) {
        let $el = $(e.currentTarget)
        this.$modalDel.modal('show')
        let $parent = $el.parents('.browsHis__list');
        var count = $parent.find('.browsHisItem').length
        if(count <= 1){
            this.delItem = $parent
        }else{
            this.delItem = $el.parents(".browsHisItem")[0];
        }
    },
    deleteRev () {
        this.delItem.remove()
        this.$modalDel.modal('hide')
    },
    loading () {
        let scrollTop = this.$win.scrollTop()
        let elTop = this.$load.offset().top
        let h = this.$win.height()
        if(scrollTop > elTop - h){
            this.$load.addClass('active')
            if(this.loadProcess == false){
                this.loadProcess = true
                setTimeout(() => {
                    $('.browsHis__container').append(` 
                        <div class="browsHis__list">
                            <div class="h4">May 5, 2018</div>
                            <div class="browsHisItem listItem">
                                <a class="browsHisItem__img" href="">
                                    <img alt="" src="http://loremflickr.com/90/65" srcset="http://loremflickr.com/90/65 1x, http://loremflickr.com/180/130 2x">
                                </a>
                                <div class="browsHisItem__content">
                                    <h4 class="browsHisItem__title">
                                        <a href="">Rock Climbing for Kids and Teenagers 1</a>
                                    </h4>
                                    <a class="link link-primary" href="">
                                        Adventure Out
                                    </a>
                                </div>
                                <i class="icn-close js-delBrowsHisItem"></i>
                            </div>
                            <div class="browsHisItem listItem">
                                <a class="browsHisItem__img" href="">
                                    <img alt="" src="http://loremflickr.com/90/65" srcset="http://loremflickr.com/90/65 1x, http://loremflickr.com/180/130 2x">
                                </a>
                                <div class="browsHisItem__content">
                                    <h4 class="browsHisItem__title">
                                        <a href="">Rock Climbing for Kids and Teenagers 2</a>
                                    </h4>
                                    <a class="link link-primary" href="">
                                        Adventure Out
                                    </a>
                                </div>
                                <i class="icn-close js-delBrowsHisItem"></i>
                            </div>
                            <div class="browsHisItem listItem">
                                <a class="browsHisItem__img" href="">
                                    <img alt="" src="http://loremflickr.com/90/65" srcset="http://loremflickr.com/90/65 1x, http://loremflickr.com/180/130 2x">
                                </a>
                                <div class="browsHisItem__content">
                                    <h4 class="browsHisItem__title">
                                        <a href="">Rock Climbing for Kids and Teenagers 3</a>
                                    </h4>
                                    <a class="link link-primary" href="">
                                        Adventure Out
                                    </a>
                                </div>
                                <i class="icn-close js-delBrowsHisItem"></i>
                            </div>
                        </div>
                    `)
                    this.$load.removeClass('active')
                    this.loadProcess = false
                }, 1000)
            }
        }
    },
    events () {
        this.$win.on('scroll', this.loading.bind(this));
    },
    init () {
        this.cacheDom()
        this.events()
    }
}
if($('.js-delBrowsHisItem').length > 0){
   moduleBrowsHis.init()
}
