var moduleHeader = {
    cacheDom() {
        this.$btn = $('.js-mobMenuOpen')
        this.$close = $('.js-mobMenuCloce')
        this.$searchBtn = $('.js-searchBy')
        this.$search = $('.header__searchBy')
        this.$html = $('html')
        this.$menu = $('.mobileMenu')
        this.$sign = $('.js-mobSign')
        
        this.$userBtn = $('.js-userBtn')
        this.$userMenu = $('.js-userMenu')
        
        this.$win = $(window)
        this.$head = $('.header')
        this.$place = $('.header-place')

    },
    openMenu (e) {
        this.$menu.addClass('open');
        this.$html.toggleClass('js-html');
    }, 
    closeMenu (e) {
        this.$menu.removeClass('open');
        this.$html.removeClass('js-html');
    },
    toggleSearch (e) {
        this.$search.toggleClass('open');
        this.$searchBtn.toggleClass('open');
    },
    signModals (e) {
        let modal = $(e.currentTarget).attr('data-modal')
        this.closeMenu()
        $(modal).modal('show')
    },
    toggleUserMenu (e){
        this.$userMenu.toggleClass('open');
        e.stopPropagation()
    },
    closeUserMenu (){
        this.$userMenu.removeClass('open');
    }, 
    headerPlace () {
        let height = this.$head.height()
        this.$place.css({height: height+'px'})
    },
   
    events () {
        this.$btn.on('click', this.openMenu.bind(this));
        this.$close.on('click', this.closeMenu.bind(this));
        this.$searchBtn.on('click', this.toggleSearch.bind(this));
        this.$sign.on('click', this.signModals.bind(this));
        
        this.$userBtn.on('click', this.toggleUserMenu.bind(this));
        this.$win.on('click', this.closeUserMenu.bind(this));
        

        this.$userMenu.on('click', function(e){e.stopPropagation() });
        this.$win.on('resize orientationchange', this.headerPlace.bind(this));
    },
    init () {
        this.cacheDom()
        this.headerPlace()
        this.events()
    }
}
if($('.header').length > 0){
    moduleHeader.init()
}


