var modulePayments = {
    cacheDom() {
        this.$daterange = $('.js-daterange')
        this.$change = $('.js-change')

        this.$showMore = $('.js-showMore');
        this.loadProcess = false;
        this.loadCounter = 0;
    },
    runDateRange(){

        let place = this.$daterange.parents('.field-case')
        let today = new Date();

        this.$daterange.daterangepicker({
            opens: 'right',
            buttonClasses: 'btns',
            applyButtonClasses: 'btns-primary',
            cancelButtonClasses: 'btns-default',
            autoUpdateInput: false,
            parentEl: place,
            changeMonth : true,  
            maxDate: today,
            locale: {
                cancelLabel: 'Clear',
                format: 'DD MMMM',
                monthNames: [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                daysOfWeek: [
                    'sun',
                    'mon',
                    'tue',
                    'wed', 
                    'thu',
                    'fri',
                    'sat'
                ]
            },
        }).on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD MMMM') + ' - ' + picker.endDate.format('DD MMMM'));
        }).on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        })
        
    },
    changePage (e) {
        let $el = $(e.currentTarget)
        let val = $el.val()
        window.location.href = val
    },
    loading (e) {
        let $el = $(e.currentTarget)
        if(this.loadProcess == false){
            this.loadProcess = true
            $el.prepend('<span class="connecting"><span>')
            setTimeout(() => {
                $('.payment__historyTable tbody').append(` 
                    <tr>
                        <td>
                            15 August, 2018 13:22
                        </td>
                        <td>
                            <strong>$190</strong>
                        </td>
                        <td>
                            Direct Deposit/ACH
                        </td>
                        <td>
                            <span class="badges2 badges2-danger">Cancelled</span>
                        </td>
                        <td>
                            <span class="date">
                                Estimated date of commision:
                                <br>
                                19 August, 2018
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            15 August, 2018 13:22
                        </td>
                        <td>
                            <strong>$190</strong>
                        </td>
                        <td>
                            Direct Deposit/ACH
                        </td>
                        <td>
                            <span class="badges2 badges2-warninig">Pending</span>
                        </td>
                        <td>
                            <span class="date">
                                Estimated date of commision:
                                <br>
                                19 August, 2018
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            15 August, 2018 13:22
                        </td>
                        <td>
                            <strong>$190</strong>
                        </td>
                        <td>
                            Direct Deposit/ACH
                        </td>
                        <td>
                            <span class="badges2 badges2-success">Paid</span>
                        </td>
                        <td>
                            <span class="date">
                                Estimated date of commision:
                                <br>
                                19 August, 2018
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            15 August, 2018 13:22
                        </td>
                        <td>
                            <strong>$190</strong>
                        </td>
                        <td>
                            Direct Deposit/ACH
                        </td>
                        <td>
                            <span class="badges2 badges2-success">Paid</span>
                        </td>
                        <td>
                            <span class="date">
                                Estimated date of commision:
                                <br>
                                19 August, 2018
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            15 August, 2018 13:22
                        </td>
                        <td>
                            <strong>$190</strong>
                        </td>
                        <td>
                            Direct Deposit/ACH
                        </td>
                        <td>
                            <span class="badges2 badges2-danger">Cancelled</span>
                        </td>
                        <td>
                            <span class="date">
                                Estimated date of commision:
                                <br>
                                19 August, 2018
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            15 August, 2018 13:22
                        </td>
                        <td>
                            <strong>$190</strong>
                        </td>
                        <td>
                            Direct Deposit/ACH
                        </td>
                        <td>
                            <span class="badges2 badges2-warninig">Pending</span>
                        </td>
                        <td>
                            <span class="date">
                                Estimated date of commision:
                                <br>
                                19 August, 2018
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            15 August, 2018 13:22
                        </td>
                        <td>
                            <strong>$190</strong>
                        </td>
                        <td>
                            Direct Deposit/ACH
                        </td>
                        <td>
                            <span class="badges2 badges2-success">Paid</span>
                        </td>
                        <td>
                            <span class="date">
                                Estimated date of commision:
                                <br>
                                19 August, 2018
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            15 August, 2018 13:22
                        </td>
                        <td>
                            <strong>$190</strong>
                        </td>
                        <td>
                            Direct Deposit/ACH
                        </td>
                        <td>
                            <span class="badges2 badges2-success">Paid</span>
                        </td>
                        <td>
                            <span class="date">
                                Estimated date of commision:
                                <br>
                                19 August, 2018
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            15 August, 2018 13:22
                        </td>
                        <td>
                            <strong>$190</strong>
                        </td>
                        <td>
                            Direct Deposit/ACH
                        </td>
                        <td>
                            <span class="badges2 badges2-success">Paid</span>
                        </td>
                        <td>
                            <span class="date">
                                Estimated date of commision:
                                <br>
                                19 August, 2018
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            15 August, 2018 13:22
                        </td>
                        <td>
                            <strong>$190</strong>
                        </td>
                        <td>
                            Direct Deposit/ACH
                        </td>
                        <td>
                            <span class="badges2 badges2-success">Paid</span>
                        </td>
                        <td>
                            <span class="date">
                                Estimated date of commision:
                                <br>
                                19 August, 2018
                            </span>
                        </td>
                    </tr>
                `)
                this.loadProcess = false
                this.loadCounter++
                $el.find('.connecting').remove()
                if(this.loadCounter > 2){
                    $el.hide()
                }
            }, 1000)
           
        }
        
    },
	events () {
        this.$change.on('change', this.changePage.bind(this));
        this.$showMore.on('click', this.loading.bind(this));
    },
    init () {
        this.cacheDom()
        this.runDateRange()
        this.events()
    }
}
if($('.payment').length > 0){
    modulePayments.init()
}