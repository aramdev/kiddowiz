var moduleFilterSidebar = {
    cacheDom() {
        this.$btn = $('.js-toggleFilter');
        this.$cnt = $('.filterSidebar__showHide');

        this.$start = $('.js-priceStart')
        this.$end = $('.js-priceEnd')
        this.$range = $('.js-range')

        this.$daterange = $('.js-daterange')
        this.$showCalendar = $('.js-showCalendar')
        this.$calendar = $('.filterSidebar__dateRange')
    },
	toggleFilter () {
        var text = this.$btn.find('span').text()
        if (text == 'more'){
            text = "less"
        }else{
            text = 'more'
        }
        this.$cnt.toggleClass('active');
        this.$btn.find('span').text(text)
    },
    runSlider () {
        let tath = this 
        this.$range.slider({
            range: true,
            min: 0,
            max: 5000,
            values: [ 0, 5000 ],
            slide: function( event, ui ) {
                tath.$start.val(ui.values[ 0 ])
                tath.$end.val(ui.values[ 1 ])
            }
        });
    },
    changeStart (e) {
        var val = $(e.currentTarget).val()
        this.$range.slider('values', 0, val);
    },
    changeEnd (e) {
        var val = $(e.currentTarget).val()
        this.$range.slider('values', 1, val);
    },
    runDateRange(){

        let place = this.$calendar
        let today = new Date();

        this.$daterange.daterangepicker({
            opens: 'left',
            buttonClasses: 'btns',
            applyButtonClasses: 'btns-primary',
            cancelButtonClasses: 'btns-default',
            autoUpdateInput: false,
            parentEl: place,
            changeMonth : true,  
            minDate: today,

            locale: {
                cancelLabel: 'Clear',
                format: 'DD MMMM',
                monthNames: [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                daysOfWeek: [
                    'sun',
                    'mon',
                    'tue',
                    'wed', 
                    'thu',
                    'fri',
                    'sat'
                ]
            },
        }).on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD MMMM') + ' - ' + picker.endDate.format('DD MMMM'));
        }).on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        })
        
    },
    calendarShow (e) {
        let val =  $(e.currentTarget).val()
        if(val == 'PickDate'){
            this.$calendar.addClass('active');
        }else{
            this.$calendar.removeClass('active');
        }
    },
    events () {
        this.$btn.on('click', this.toggleFilter.bind(this));
        this.$showCalendar.on('change', this.calendarShow.bind(this));
        this.$start.on('input', this.changeStart.bind(this));
        this.$end.on('input', this.changeEnd.bind(this));
    },
    init () {
        this.cacheDom()
        this.runSlider()
        this.runDateRange()
        this.events()
    }
}
if($('.js-toggleFilter').length > 0){
    moduleFilterSidebar.init()
}




