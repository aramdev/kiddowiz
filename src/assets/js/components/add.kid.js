var moduleAddKid = {
    cacheDom() {
        this.$body = $("body")
        this.$modal = $("#kidModal")
        this.$kidForm = $("#kidForm")
        
        this.$modalEdit = $("#kidEditModal")
        this.$kidEditForm = $("#kidEditForm")
        this.$delete = this.$modalEdit.find('.js-delete')

        this.children = [
            {
                ind: "BranStarkXXX",
                old: 16,
                birthday: '03 Feb, 2003',
                firstname: 'Bran',
                lastname: 'Stark',
                interests: [
                    "OUTDOOR ACTIVITIES",
                    "SPORT"
                ],
            },
            {
                ind: "GagoStarkYYY",
                old: 16,
                birthday: '03 Feb, 2003',
                firstname: 'Gago',
                lastname: 'Stark',
                interests: [
                    "SPORT",
                    "OUTDOOR ACTIVITIES",
                ],
            }
        ];
        this.kids = new Kids(this.children);
        this.kids.birthDay('input[name="birthday"]')
    },
	addChild(e){
        e.preventDefault()
        let $el = $(e.currentTarget)
        // add ajax (autosave)
        this.kids.add('#kidModal', '#kidForm')
        this.outputKidList()
    },
    editChild (e){
        e.preventDefault()
        let $el = $(e.currentTarget)
        let childId = $el.find('[name="id"]').val()
        // add ajax (autosave)
        this.kids.edit('#kidEditModal', '#kidEditForm', childId)
        this.outputKidList()
    },
    setChild(e) {
        let $el = $(e.currentTarget)
        let childId =  $el.attr('data-id');
        this.kids.set('#kidEditModal', '#kidEditForm', childId)
    },
    deteleChild(e){
        let $el = $(e.currentTarget)
        let childId = $el.parents('#kidEditModal').find('[name="id"]').val()
        this.kids.delete('#kidEditModal', '#kidEditForm', childId)
        this.outputKidList()

        if(this.children.length == 0){
            this.$paymentCheck.val('').trigger('change') 
            this.$seats.val(0).trigger('change')
            $(".step3 .btnsBox .btns").addClass('disabled')
        }else{
            let flag = 0
            this.children.forEach(function(element, i) {
                if(element.checked == true){
                   flag++
                }
            });
            if(flag <= 0){
               this.$paymentCheck.val('').trigger('change') 
               this.$seats.val(0).trigger('change')
               $(".step3 .btnsBox .btns").addClass('disabled')
            }
        }
    },
    outputKidList(){
        let container = $('.addKids__list')
        container.html('')
        let child = ""
        let count = 0;
        for(let item of this.children){
            
            let tags = ""
            item.interests.forEach( function(element, index) {
                tags += `
                    <a class="tag" href="">
                        ${element}
                    </a>
                `
            });
            
            var value = JSON.stringify(item);
            child += `
                <div class="addKids__listItem">
                    <input type="hidden" value="${value}">
                    <span class="name">
                        ${item.firstname}
                        ${item.lastname}
                    </span>
                    <span class="old">${item.old} years old</span>
                    <div class="interests">
                        <span>Interests</span>
                        ${tags}
                    </div>
                    <a class="edit js-editChild" tabindex="" data-id="${item.ind}">
                        <i class="icn-edit"></i>
                        Edit
                    </a>
                </div>
            `
        }
        container.append(child)
    },
    resetForm(e){
        let $el = $(e.currentTarget)
        $el.find('[type="reset"]').trigger('click')
        $el.find('.select2-selection__choice__remove').trigger('click')
        $el.find('.btns').attr('disabled', 'disabled');
        setTimeout(function(){
            $el.find('.field').removeClass('error')
            $el.find('label.error').hide()
        }, 300)
    },    
    events () {
        this.$modal.on('hide.bs.modal', this.resetForm.bind(this));
        this.$modalEdit.on('hide.bs.modal', this.resetForm.bind(this));
        this.$kidForm.on('submit', this.addChild.bind(this));
        this.$body.on('click', '.js-editChild', this.setChild.bind(this));
        this.$kidEditForm.on('submit', this.editChild.bind(this));
        this.$delete.on('click', this.deteleChild.bind(this));
    },
    init () {
        this.cacheDom()
        this.outputKidList()
        this.events()
    }
}
if($('.addKids__list').length > 0){
    moduleAddKid.init()
}