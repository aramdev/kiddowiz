var moduleSidebar = {
    cacheDom() {
        this.$btn = $('.js-toggleSidebar')
        this.$close = $('.js-closeSidebar')
        this.$html = $('html')
    },
	toggleSidebar (e) {
       $(e.currentTarget).toggleClass('open');
	   $(e.currentTarget).parents('.js-sidebar').toggleClass('open');
       this.$html.toggleClass('js-html');
    }, 
    closeSidebar (e) {
       this.$btn.removeClass('open');
       $(e.currentTarget).parents('.js-sidebar').removeClass('open');
       this.$html.removeClass('js-html');
    },    
    events () {
        this.$btn.on('click', this.toggleSidebar.bind(this));
        this.$close.on('click', this.closeSidebar.bind(this));
    },
    init () {
        this.cacheDom()
        this.events()
    }
}
if($('.js-toggleSidebar').length > 0){
    moduleSidebar.init()
}