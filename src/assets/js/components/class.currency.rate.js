class CurrencyRate {
    constructor (price, output) {
        this.price = price;
        this.output = $(output)
    }
    counting(usd, cad){
        let result = this.price * usd / cad
        result = result.toFixed(2)
        this.output.text(result)
    }
    init(){
        $.ajax({
            url: `http://data.fixer.io/api/latest?access_key=3eb0b1fd280f0889dc29866c2122c6fd&symbols=CAD,USD`, 
            success: (result) => {
                if(result.success){
                    let usd = result.rates.USD
                    let cad = result.rates.CAD 
                    this.counting(usd, cad)   
                }
            }
        });
    }
}
