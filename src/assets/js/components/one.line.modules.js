$('[type="tel"]').inputmask("+1 (999) 999-9999");

$('[name="tax"]').inputmask("99-9999999");

$('.ageInput').inputmask("9{1,2}");



/*$('.timeInput').inputmask({ 
	alias: "datetime",
	inputFormat: "hh:MM tt",
});*/

$('[data-fancybox]').fancybox();

autosize($('textarea.resize'));

$('[data-toggle="tooltip"]').each(function(index, el) {
	let $this = $(this)
	$this.tooltip({
		container: $this.parent()
	})
});	

$('.perfect-date').each(function(index, el) {
	let $this = $(this);
	let date = new Date($this.text())
	let time = moment(date).format('MMMM DD, YYYY');
	$this.text(time)
});
