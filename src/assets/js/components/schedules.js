var calendarDates = [  
    {  
        "year":"2019",
        "month":"4",
        "day":"5",
        "type":"other2",
        "style": "background-color: #ac8dea",
        "time": ["9:00am - 11:00am"]
    },

    
    {  
        "year":"2019",
        "month":"7",
        "day":"14",
        "type":"am",
        "style": "background-color: #f1edf9",
        "time": ["9:00am - 11:00am"]
    },
    {  
        "year":"2019",
        "month":"3",
        "day":"11",
        "type":"am",
        "style": "background-color: #f1edf9",
        "time": ["9:00am - 11:00am"]
    },
    {  
        "year":"2019",
        "month":"4",
        "day":"11",
        "type":"am",
        "style": "background-color: #f1edf9",
        "time": ["9:00am - 11:00am"]
    },
    {  
        "year":"2019",
        "month":"5",
        "day":"11",
        "type":"am",
        "style": "background-color: #f1edf9",
        "time": ["9:00am - 11:00am"]
    },
    {  
        "year":"2019",
        "month":"6",
        "day":"11",
        "type":"am",
        "style": "background-color: #f1edf9",
        "time": ["9:00am - 11:00am"]
    },
    {  
        "year":"2019",
        "month":"3",
        "day":"10",
        "type":"am",
        "style": "background-color: #f1edf9",
        "time": ["9:00am - 11:00am"]
    },
    {  
        "year":"2019",
        "month":"4",
        "day":"10",
        "type":"am",
        "style": "background-color: #f1edf9",
        "time": ["9:00am - 11:00am"]
    },
    {  
        "year":"2019",
        "month":"5",
        "day":"10",
        "type":"am",
        "style": "background-color: #f1edf9",
        "time": ["9:00am - 11:00am"]
    },
    {  
        "year":"2019",
        "month":"6",
        "day":"10",
        "type":"am",
        "style": "background-color: #f1edf9",
        "time": ["9:00am - 11:00am"]
    },
    {  
        "year":"2019",
        "month":"3",
        "day":"18",
        "type":"other",
        "style": "background: #f1edf9; background: -moz-linear-gradient(45deg, #f1edf9 50%, #ccc4de 50%); background: -webkit-linear-gradient(45deg, #f1edf9 50%,#ccc4de 50%); background: linear-gradient(45deg, #f1edf9 50%,#ccc4de 50%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f1edf9', endColorstr='#ccc4de',GradientType=1 );",
        "time": [
            "9:00am - 11:00am",
            "9:00pm - 11:00pm"
        ]
    },
    {  
        "year":"2019",
        "month":"4",
        "day":"18",
        "type":"other",
        "style": "background: #f1edf9; background: -moz-linear-gradient(45deg, #f1edf9 50%, #ccc4de 50%); background: -webkit-linear-gradient(45deg, #f1edf9 50%,#ccc4de 50%); background: linear-gradient(45deg, #f1edf9 50%,#ccc4de 50%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f1edf9', endColorstr='#ccc4de',GradientType=1 );",
        "time": [
            "9:00am - 11:00am",
            "9:00pm - 11:00pm"
        ]
    },
    {  
        "year":"2019",
        "month":"5",
        "day":"18",
        "type":"other",
        "style": "background: #f1edf9; background: -moz-linear-gradient(45deg, #f1edf9 50%, #ccc4de 50%); background: -webkit-linear-gradient(45deg, #f1edf9 50%,#ccc4de 50%); background: linear-gradient(45deg, #f1edf9 50%,#ccc4de 50%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f1edf9', endColorstr='#ccc4de',GradientType=1 );",
        "time": [
            "9:00am - 11:00am",
            "9:00pm - 11:00pm"
        ]
    },
    {  
        "year":"2019",
        "month":"6",
        "day":"18",
        "type":"other",
        "style": "background: #f1edf9; background: -moz-linear-gradient(45deg, #f1edf9 50%, #ccc4de 50%); background: -webkit-linear-gradient(45deg, #f1edf9 50%,#ccc4de 50%); background: linear-gradient(45deg, #f1edf9 50%,#ccc4de 50%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f1edf9', endColorstr='#ccc4de',GradientType=1 );",
        "time": [
            "9:00am - 11:00am",
            "9:00pm - 11:00pm"
        ]
    },

    {  
        "year":"2019",
        "month":"7",
        "day":"20",
        "type":"pm",
        "style": "background-color: #ccc4de",
        "time": ["9:00pm - 11:00pm"]
    },
    {  
        "year":"2019",
        "month":"3",
        "day":"20",
        "type":"pm",
        "style": "background-color: #ccc4de",
        "time": ["9:00pm - 11:00pm"]
    },
    {  
        "year":"2019",
        "month":"4",
        "day":"20",
        "type":"pm",
        "style": "background-color: #ccc4de",
        "time": ["9:00pm - 11:00pm"]
    },
    {  
        "year":"2019",
        "month":"5",
        "day":"20",
        "type":"pm",
        "style": "background-color: #ccc4de",
        "time": ["9:00pm - 11:00pm"]
    },
    {  
        "year":"2019",
        "month":"6",
        "day":"20",
        "type":"pm",
        "style": "background-color: #ccc4de",
        "time": ["9:00pm - 11:00pm"]
    },
    {  
        "year":"2019",
        "month":"4",
        "day":"13",
        "type":"other2",
        "style": "background-color: #ac8dea",
        "time": ["9:00am - 11:00am"]
    },
]




var moduleSchedules = {
    cacheDom() {
        this.$btn = $('.js-showSchedulesDates');
        this.$cnt = $('.schedules__calendar');
        this.calendar = $( ".datepicker__calendar" )
        this.timePick = $( ".schedules__time .item" )
        this.showScheBtn = $( ".js-moreScheldules" )
        this.$win = $(window)
    },

    runCalendar () {
        let that = this
        this.calendar.each(function() {

            let str = $(this).attr('data-minDate');
            let dataDates = $(this).attr('data-dates');
            let minDate = str ? Date(str) : new Date()
            let calendar = $(this)
            $(this).datepicker({
                numberOfMonths: 2,
                dayNamesMin: ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"],
                minDate: minDate,
                onChangeMonthYear () {
                    setTimeout(function(){
                        that.setDay(calendar)
                    }, 20)
                },
                onSelect(dateText, inst){
                    $(this).data('datepicker').inline = false;  
                    
                }
            });

            that.setDay($(this))
        });
    },
    setDay (calendar) {
        let data = calendarDates
        //data = JSON.parse(data)
        for(let item of data){
            let $tds = calendar.find(`td[data-month="${item.month}"][data-year="${item.year}"]`)
            $tds.each(function(index, el) {
                let $td = $(this)
                let $a = $td.find('a')
                if($a.text() == item.day){
                    $td.attr('style', item.style);
                    $td.attr('data-type', item.type);
                    let inner = "";
                    
                    for(let i = 0; i < item.time.length; i++){
                        inner += `<span>${item.time[i]}</span>`
                    }

                    $td.append(`
                        <span class="time">
                            ${inner}
                        </span>
                    `)
                }
            });
        }
    },
    toggleCalendar (e) {
        let $el = $(e.currentTarget)
        let dataText = $el.attr('data-text') || 'yes'; 
        if(dataText == 'yes'){
            let $span = $el.find('span') 
            var text = $span.text()
            
            if (text == 'Show'){
                text = "Hide"
            }else{
                text = 'Show'
            }
            $el.parents('.schedules').find('.schedules__calendar').toggleClass('active');
            $span.text(text)
        }else{
            $('.schedules__calendar.ext').removeClass('active')
            e.stopPropagation()
            $el.next('.schedules__calendar').toggleClass('active');
        }
    },
    closeCalendar () {
        $('.schedules__calendar.ext').removeClass('active')
    },
    dateChack(e){
        let $el = $(e.currentTarget)
        let type = $el.attr('data-type');
        let $parent = $el.parents('.schedules__calendar')
        let $td = $parent.find(`td[data-type="${type}"]`)
        $td.find('a').css({"box-shadow": "inset 0 0 1px 1px #5642ab"})
    },
    dateUnChack(e){
        let $el = $(e.currentTarget)
        let type = $el.attr('data-type');
        let $parent = $el.parents('.schedules__calendar')
        let $td = $parent.find(`td[data-type="${type}"]`)
        $td.find('a').removeAttr('style')
    },
    showScheldules(e){
      let $el = $(e.currentTarget)
      $el.parent('.eventsPage__more').remove()
      $('.schedules').removeClass('hidden')
    },
    events () {
        this.$btn.on('click', this.toggleCalendar.bind(this));
        this.$win.on('click', this.closeCalendar.bind(this));
        this.$cnt.on('click', function(e){e.stopPropagation()});
        this.timePick.on('mouseover', this.dateChack.bind(this));
        this.timePick.on('mouseleave', this.dateUnChack.bind(this));
        this.showScheBtn.on('click', this.showScheldules.bind(this));
    },
    init () {
        this.cacheDom()
        this.runCalendar()
        this.events()
    }
}
if($('.js-showSchedulesDates').length > 0){
    moduleSchedules.init()
}

