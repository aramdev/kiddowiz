var moduleCopmanyDel = {
    cacheDom() {
        this.$btn = $('.js-companyDel')
        this.$del = $('.js-deleteReview')
        this.$modalDel = $('#deleteReview')
        this.delItem = ""
    },
	remove (e) {
        let $el = $(e.currentTarget)
        this.$modalDel.modal('show')
        this.delItem = $el.parents(".companyProf")[0];

             
    },
    deleteRev () {
        this.delItem.remove()
        if($(".companyProf").length == 0){
            this.$modalDel.one('hidden.bs.modal', function(){
                $(".companyProfList").append(`
                    <p>
                        You don't have company defined. Please consider to add one by clicking to the
                        <strong>'Add Company'</strong>
                        button. It will allow you to add events.
                    </p>
                `)
            })
        }
        this.$modalDel.modal('hide')
        
    },
    events () {
        this.$btn.on('click', this.remove.bind(this));
        this.$del.on('click', this.deleteRev.bind(this));
        
    },
    init () {
        this.cacheDom()
        this.events()
    }
}
if($('.js-companyDel').length > 0){
    moduleCopmanyDel.init()
}