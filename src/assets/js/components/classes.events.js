var moduleClassesEvents = {
    cacheDom() {
        this.$win = $(window);
        this.$load = $('.loading');
        this.loadProcess = false;
    },
	loading () {
        let scrollTop = this.$win.scrollTop()
        let elTop = this.$load.offset().top
        let h = this.$win.height()
        if(scrollTop > elTop - h){
            this.$load.addClass('active')
            if(this.loadProcess == false){
                this.loadProcess = true
                setTimeout(() => {
                    $('.classesEvents__list').append(` 
                        <div class="classesEventsItem listItem">
                            <a class="classesEventsItem__img" href="">
                                <img alt="" src="http://loremflickr.com/90/65" srcset="http://loremflickr.com/90/65 1x, http://loremflickr.com/180/130 2x">
                            </a>
                            <div class="classesEventsItem__content">
                                <div class="classesEventsItem__rightBar">
                                    <a class="edit" href="">
                                        <i class="icn-edit"></i>
                                        Edit
                                    </a>
                                    <br>
                                    <div class="badges2 badges2-danger">
                                        No available seats
                                    </div>
                                    <br>
                                    <div class="rate">
                                        <i class="icn-star active"></i>
                                        <i class="icn-star active"></i>
                                        <i class="icn-star active"></i>
                                        <i class="icn-star"></i>
                                        <i class="icn-star"></i>
                                    </div>
                                    <a class="commentsCount" href="">
                                        <i class="icn-comments"></i>
                                        3
                                    </a>
                                </div>
                                <h4 class="classesEventsItem__title">
                                    <a href="">Music Lessons for Kids and Teenagers</a>
                                </h4>
                                <span class="classesEventsItem__address">
                                    900 Poplar Pl S, Seattke, WA 98144
                                </span>
                                <a class="link link-primary" href="">Adventure Out</a>
                                <div class="classesEventsItem__props">
                                    <div class="item">
                                        PRICE
                                        <span>
                                            <strong>$160-$320</strong>
                                        </span>
                                    </div>
                                    <div class="item">
                                        Date
                                        <span>Today</span>
                                    </div>
                                    <div class="item">
                                        age
                                        <span>8-14</span>
                                    </div>
                                    <div class="item">
                                        ORDERS
                                        <a class="link link-primary" href="">3</a>
                                    </div>
                                </div>
                                <a class="btns btns-icon" href="">
                                    <i class="icn-edit"></i>
                                </a>
                            </div>
                        </div>
                        <div class="classesEventsItem listItem">
                            <a class="classesEventsItem__img" href="">
                                <img alt="" src="http://loremflickr.com/90/65" srcset="http://loremflickr.com/90/65 1x, http://loremflickr.com/180/130 2x">
                            </a>
                            <div class="classesEventsItem__content">
                                <div class="classesEventsItem__rightBar">
                                    <a class="edit" href="">
                                        <i class="icn-edit"></i>
                                        Edit
                                    </a>
                                    <br>
                                    <div class="badges2 badges2-danger">
                                        No available seats
                                    </div>
                                    <br>
                                    <div class="rate">
                                        <i class="icn-star active"></i>
                                        <i class="icn-star active"></i>
                                        <i class="icn-star active"></i>
                                        <i class="icn-star"></i>
                                        <i class="icn-star"></i>
                                    </div>
                                    <a class="commentsCount" href="">
                                        <i class="icn-comments"></i>
                                        3
                                    </a>
                                </div>
                                <h4 class="classesEventsItem__title">
                                    <a href="">Music Lessons for Kids and Teenagers</a>
                                </h4>
                                <span class="classesEventsItem__address">
                                    900 Poplar Pl S, Seattke, WA 98144
                                </span>
                                <a class="link link-primary" href="">Adventure Out</a>
                                <div class="classesEventsItem__props">
                                    <div class="item">
                                        PRICE
                                        <span>
                                            <strong>$160-$320</strong>
                                        </span>
                                    </div>
                                    <div class="item">
                                        Date
                                        <span>Today</span>
                                    </div>
                                    <div class="item">
                                        age
                                        <span>8-14</span>
                                    </div>
                                    <div class="item">
                                        ORDERS
                                        <a class="link link-primary" href="">3</a>
                                    </div>
                                </div>
                                <a class="btns btns-icon" href="">
                                    <i class="icn-edit"></i>
                                </a>
                            </div>
                        </div>
                        <div class="classesEventsItem listItem">
                            <a class="classesEventsItem__img" href="">
                                <img alt="" src="http://loremflickr.com/90/65" srcset="http://loremflickr.com/90/65 1x, http://loremflickr.com/180/130 2x">
                            </a>
                            <div class="classesEventsItem__content">
                                <div class="classesEventsItem__rightBar">
                                    <a class="edit" href="">
                                        <i class="icn-edit"></i>
                                        Edit
                                    </a>
                                    <br>
                                    <div class="badges2 badges2-danger">
                                        No available seats
                                    </div>
                                    <br>
                                    <div class="rate">
                                        <i class="icn-star active"></i>
                                        <i class="icn-star active"></i>
                                        <i class="icn-star active"></i>
                                        <i class="icn-star"></i>
                                        <i class="icn-star"></i>
                                    </div>
                                    <a class="commentsCount" href="">
                                        <i class="icn-comments"></i>
                                        3
                                    </a>
                                </div>
                                <h4 class="classesEventsItem__title">
                                    <a href="">Music Lessons for Kids and Teenagers</a>
                                </h4>
                                <span class="classesEventsItem__address">
                                    900 Poplar Pl S, Seattke, WA 98144
                                </span>
                                <a class="link link-primary" href="">Adventure Out</a>
                                <div class="classesEventsItem__props">
                                    <div class="item">
                                        PRICE
                                        <span>
                                            <strong>$160-$320</strong>
                                        </span>
                                    </div>
                                    <div class="item">
                                        Date
                                        <span>Today</span>
                                    </div>
                                    <div class="item">
                                        age
                                        <span>8-14</span>
                                    </div>
                                    <div class="item">
                                        ORDERS
                                        <a class="link link-primary" href="">3</a>
                                    </div>
                                </div>
                                <a class="btns btns-icon" href="">
                                    <i class="icn-edit"></i>
                                </a>
                            </div>
                        </div>
                        <div class="classesEventsItem listItem">
                            <a class="classesEventsItem__img" href="">
                                <img alt="" src="http://loremflickr.com/90/65" srcset="http://loremflickr.com/90/65 1x, http://loremflickr.com/180/130 2x">
                            </a>
                            <div class="classesEventsItem__content">
                                <div class="classesEventsItem__rightBar">
                                    <a class="edit" href="">
                                        <i class="icn-edit"></i>
                                        Edit
                                    </a>
                                    <br>
                                    <div class="badges2 badges2-danger">
                                        No available seats
                                    </div>
                                    <br>
                                    <div class="rate">
                                        <i class="icn-star active"></i>
                                        <i class="icn-star active"></i>
                                        <i class="icn-star active"></i>
                                        <i class="icn-star"></i>
                                        <i class="icn-star"></i>
                                    </div>
                                    <a class="commentsCount" href="">
                                        <i class="icn-comments"></i>
                                        3
                                    </a>
                                </div>
                                <h4 class="classesEventsItem__title">
                                    <a href="">Music Lessons for Kids and Teenagers</a>
                                </h4>
                                <span class="classesEventsItem__address">
                                    900 Poplar Pl S, Seattke, WA 98144
                                </span>
                                <a class="link link-primary" href="">Adventure Out</a>
                                <div class="classesEventsItem__props">
                                    <div class="item">
                                        PRICE
                                        <span>
                                            <strong>$160-$320</strong>
                                        </span>
                                    </div>
                                    <div class="item">
                                        Date
                                        <span>Today</span>
                                    </div>
                                    <div class="item">
                                        age
                                        <span>8-14</span>
                                    </div>
                                    <div class="item">
                                        ORDERS
                                        <a class="link link-primary" href="">3</a>
                                    </div>
                                </div>
                                <a class="btns btns-icon" href="">
                                    <i class="icn-edit"></i>
                                </a>
                            </div>
                        </div>
                        <div class="classesEventsItem listItem">
                            <a class="classesEventsItem__img" href="">
                                <img alt="" src="http://loremflickr.com/90/65" srcset="http://loremflickr.com/90/65 1x, http://loremflickr.com/180/130 2x">
                            </a>
                            <div class="classesEventsItem__content">
                                <div class="classesEventsItem__rightBar">
                                    <a class="edit" href="">
                                        <i class="icn-edit"></i>
                                        Edit
                                    </a>
                                    <br>
                                    <div class="badges2 badges2-success">
                                        Available 5 seats
                                    </div>
                                    <br>
                                    <div class="rate">
                                        <i class="icn-star active"></i>
                                        <i class="icn-star active"></i>
                                        <i class="icn-star active"></i>
                                        <i class="icn-star"></i>
                                        <i class="icn-star"></i>
                                    </div>
                                    <a class="commentsCount" href="">
                                        <i class="icn-comments"></i>
                                        3
                                    </a>
                                </div>
                                <h4 class="classesEventsItem__title">
                                    <a href="">Music Lessons for Kids and Teenagers</a>
                                </h4>
                                <span class="classesEventsItem__address">
                                    900 Poplar Pl S, Seattke, WA 98144
                                </span>
                                <a class="link link-primary" href="">Adventure Out</a>
                                <div class="classesEventsItem__props">
                                    <div class="item">
                                        PRICE
                                        <span>
                                            <strong>$160-$320</strong>
                                        </span>
                                    </div>
                                    <div class="item">
                                        Date
                                        <span>Today</span>
                                    </div>
                                    <div class="item">
                                        age
                                        <span>8-14</span>
                                    </div>
                                    <div class="item">
                                        ORDERS
                                        <a class="link link-primary" href="">3</a>
                                    </div>
                                </div>
                                <a class="btns btns-icon" href="">
                                    <i class="icn-edit"></i>
                                </a>
                            </div>
                        </div>
                        <div class="classesEventsItem listItem">
                            <a class="classesEventsItem__img" href="">
                                <img alt="" src="http://loremflickr.com/90/65" srcset="http://loremflickr.com/90/65 1x, http://loremflickr.com/180/130 2x">
                            </a>
                            <div class="classesEventsItem__content">
                                <div class="classesEventsItem__rightBar">
                                    <a class="edit" href="">
                                        <i class="icn-edit"></i>
                                        Edit
                                    </a>
                                    <br>
                                    <div class="badges2 badges2-danger">
                                        No available seats
                                    </div>
                                    <br>
                                    <div class="rate">
                                        <i class="icn-star active"></i>
                                        <i class="icn-star active"></i>
                                        <i class="icn-star active"></i>
                                        <i class="icn-star"></i>
                                        <i class="icn-star"></i>
                                    </div>
                                    <a class="commentsCount" href="">
                                        <i class="icn-comments"></i>
                                        3
                                    </a>
                                </div>
                                <h4 class="classesEventsItem__title">
                                    <a href="">Music Lessons for Kids and Teenagers</a>
                                </h4>
                                <span class="classesEventsItem__address">
                                    900 Poplar Pl S, Seattke, WA 98144
                                </span>
                                <a class="link link-primary" href="">Adventure Out</a>
                                <div class="classesEventsItem__props">
                                    <div class="item">
                                        PRICE
                                        <span>
                                            <strong>$160-$320</strong>
                                        </span>
                                    </div>
                                    <div class="item">
                                        Date
                                        <span>Today</span>
                                    </div>
                                    <div class="item">
                                        age
                                        <span>8-14</span>
                                    </div>
                                    <div class="item">
                                        ORDERS
                                        <a class="link link-primary" href="">3</a>
                                    </div>
                                </div>
                                <a class="btns btns-icon" href="">
                                    <i class="icn-edit"></i>
                                </a>
                            </div>
                        </div>
                        <div class="classesEventsItem listItem">
                            <a class="classesEventsItem__img" href="">
                                <img alt="" src="http://loremflickr.com/90/65" srcset="http://loremflickr.com/90/65 1x, http://loremflickr.com/180/130 2x">
                            </a>
                            <div class="classesEventsItem__content">
                                <div class="classesEventsItem__rightBar">
                                    <a class="edit" href="">
                                        <i class="icn-edit"></i>
                                        Edit
                                    </a>
                                    <br>
                                    <div class="badges2 badges2-danger">
                                        No available seats
                                    </div>
                                    <br>
                                    <div class="rate">
                                        <i class="icn-star active"></i>
                                        <i class="icn-star active"></i>
                                        <i class="icn-star active"></i>
                                        <i class="icn-star"></i>
                                        <i class="icn-star"></i>
                                    </div>
                                    <a class="commentsCount" href="">
                                        <i class="icn-comments"></i>
                                        3
                                    </a>
                                </div>
                                <h4 class="classesEventsItem__title">
                                    <a href="">Music Lessons for Kids and Teenagers</a>
                                </h4>
                                <span class="classesEventsItem__address">
                                    900 Poplar Pl S, Seattke, WA 98144
                                </span>
                                <a class="link link-primary" href="">Adventure Out</a>
                                <div class="classesEventsItem__props">
                                    <div class="item">
                                        PRICE
                                        <span>
                                            <strong>$160-$320</strong>
                                        </span>
                                    </div>
                                    <div class="item">
                                        Date
                                        <span>Today</span>
                                    </div>
                                    <div class="item">
                                        age
                                        <span>8-14</span>
                                    </div>
                                    <div class="item">
                                        ORDERS
                                        <a class="link link-primary" href="">3</a>
                                    </div>
                                </div>
                                <a class="btns btns-icon" href="">
                                    <i class="icn-edit"></i>
                                </a>
                            </div>
                        </div>
                        <div class="classesEventsItem listItem">
                            <a class="classesEventsItem__img" href="">
                                <img alt="" src="http://loremflickr.com/90/65" srcset="http://loremflickr.com/90/65 1x, http://loremflickr.com/180/130 2x">
                            </a>
                            <div class="classesEventsItem__content">
                                <div class="classesEventsItem__rightBar">
                                    <a class="edit" href="">
                                        <i class="icn-edit"></i>
                                        Edit
                                    </a>
                                    <br>
                                    <div class="badges2 badges2-danger">
                                        No available seats
                                    </div>
                                    <br>
                                    <div class="rate">
                                        <i class="icn-star active"></i>
                                        <i class="icn-star active"></i>
                                        <i class="icn-star active"></i>
                                        <i class="icn-star"></i>
                                        <i class="icn-star"></i>
                                    </div>
                                    <a class="commentsCount" href="">
                                        <i class="icn-comments"></i>
                                        3
                                    </a>
                                </div>
                                <h4 class="classesEventsItem__title">
                                    <a href="">Music Lessons for Kids and Teenagers</a>
                                </h4>
                                <span class="classesEventsItem__address">
                                    900 Poplar Pl S, Seattke, WA 98144
                                </span>
                                <a class="link link-primary" href="">Adventure Out</a>
                                <div class="classesEventsItem__props">
                                    <div class="item">
                                        PRICE
                                        <span>
                                            <strong>$160-$320</strong>
                                        </span>
                                    </div>
                                    <div class="item">
                                        Date
                                        <span>Today</span>
                                    </div>
                                    <div class="item">
                                        age
                                        <span>8-14</span>
                                    </div>
                                    <div class="item">
                                        ORDERS
                                        <a class="link link-primary" href="">3</a>
                                    </div>
                                </div>
                                <a class="btns btns-icon" href="">
                                    <i class="icn-edit"></i>
                                </a>
                            </div>
                        </div>
                        <div class="classesEventsItem listItem">
                            <a class="classesEventsItem__img" href="">
                                <img alt="" src="http://loremflickr.com/90/65" srcset="http://loremflickr.com/90/65 1x, http://loremflickr.com/180/130 2x">
                            </a>
                            <div class="classesEventsItem__content">
                                <div class="classesEventsItem__rightBar">
                                    <a class="edit" href="">
                                        <i class="icn-edit"></i>
                                        Edit
                                    </a>
                                    <br>
                                    <div class="badges2 badges2-danger">
                                        No available seats
                                    </div>
                                    <br>
                                    <div class="rate">
                                        <i class="icn-star active"></i>
                                        <i class="icn-star active"></i>
                                        <i class="icn-star active"></i>
                                        <i class="icn-star"></i>
                                        <i class="icn-star"></i>
                                    </div>
                                    <a class="commentsCount" href="">
                                        <i class="icn-comments"></i>
                                        3
                                    </a>
                                </div>
                                <h4 class="classesEventsItem__title">
                                    <a href="">Music Lessons for Kids and Teenagers</a>
                                </h4>
                                <span class="classesEventsItem__address">
                                    900 Poplar Pl S, Seattke, WA 98144
                                </span>
                                <a class="link link-primary" href="">Adventure Out</a>
                                <div class="classesEventsItem__props">
                                    <div class="item">
                                        PRICE
                                        <span>
                                            <strong>$160-$320</strong>
                                        </span>
                                    </div>
                                    <div class="item">
                                        Date
                                        <span>Today</span>
                                    </div>
                                    <div class="item">
                                        age
                                        <span>8-14</span>
                                    </div>
                                    <div class="item">
                                        ORDERS
                                        <a class="link link-primary" href="">3</a>
                                    </div>
                                </div>
                                <a class="btns btns-icon" href="">
                                    <i class="icn-edit"></i>
                                </a>
                            </div>
                        </div>
                        <div class="classesEventsItem listItem">
                            <a class="classesEventsItem__img" href="">
                                <img alt="" src="http://loremflickr.com/90/65" srcset="http://loremflickr.com/90/65 1x, http://loremflickr.com/180/130 2x">
                            </a>
                            <div class="classesEventsItem__content">
                                <div class="classesEventsItem__rightBar">
                                    <a class="edit" href="">
                                        <i class="icn-edit"></i>
                                        Edit
                                    </a>
                                    <br>
                                    <div class="badges2 badges2-danger">
                                        No available seats
                                    </div>
                                    <br>
                                    <div class="rate">
                                        <i class="icn-star active"></i>
                                        <i class="icn-star active"></i>
                                        <i class="icn-star active"></i>
                                        <i class="icn-star"></i>
                                        <i class="icn-star"></i>
                                    </div>
                                    <a class="commentsCount" href="">
                                        <i class="icn-comments"></i>
                                        3
                                    </a>
                                </div>
                                <h4 class="classesEventsItem__title">
                                    <a href="">Music Lessons for Kids and Teenagers</a>
                                </h4>
                                <span class="classesEventsItem__address">
                                    900 Poplar Pl S, Seattke, WA 98144
                                </span>
                                <a class="link link-primary" href="">Adventure Out</a>
                                <div class="classesEventsItem__props">
                                    <div class="item">
                                        PRICE
                                        <span>
                                            <strong>$160-$320</strong>
                                        </span>
                                    </div>
                                    <div class="item">
                                        Date
                                        <span>Today</span>
                                    </div>
                                    <div class="item">
                                        age
                                        <span>8-14</span>
                                    </div>
                                    <div class="item">
                                        ORDERS
                                        <a class="link link-primary" href="">3</a>
                                    </div>
                                </div>
                                <a class="btns btns-icon" href="">
                                    <i class="icn-edit"></i>
                                </a>
                            </div>
                        </div>
                    `)
                    this.$load.removeClass('active')
                    this.loadProcess = false
                }, 1000)
            }
        }
    },
    events () {
        this.$win.on('scroll', this.loading.bind(this));
    },
    init () {
        this.cacheDom()
        this.events()
    }
}
if($('.classesEvents').length > 0){
   moduleClassesEvents.init()
}