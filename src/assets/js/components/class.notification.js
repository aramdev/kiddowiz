class Notification {
    constructor () {
        this.count = 0;
    }

    detectPlace () {
        let notes = $('body').find('.reviewInfo')
        let bottom = 30
        if(notes.length > 0){
            let pos = notes.last().position().top;
            let win = window.innerHeight;
            bottom = win - pos + 15;
        }

        return bottom
    }

    resetPositions(){
        let notes = $('body').find('.reviewInfo')
        let bottom = 30

        if(notes.length > 0){
            for(let note of notes){
                $(note).css('bottom', bottom+'px')
                bottom += $(note).outerHeight() + 15
            }
        }
    }

    setNote(icon, text, type) {
        let bottom = this.detectPlace()
        this.count++
        $('body').append(`<div class="reviewInfo ${type}" 
                                data-note="noteId${this.count}"
                                style="bottom: ${bottom}px"
                                >
                            <i class="${icon} reviewInfo__icon"></i>
                            <div class="reviewInfo__text">
                                ${text}
                            </div>
                             <i class="icn-close"></i>
                        </div>`)

        let $note = $('body').find(`[data-note="noteId${this.count}"]`)
        this.noteDeactivate(this.count)
        
        let time = setTimeout(() => {
            $note.addClass('active')
        }, 100)
    }

    noteDeactivate(id){
        let $note = $('body').find(`[data-note="noteId${id}"]`)
        let time = ""
        let $close = $note.find('.icn-close')
        let self = this


        time = setTimeout( () => {
            $note.one('webkitTransitionEnd transitionend', function(){
                $(this).remove()
                self.resetPositions()
            })
            $note.removeClass('active')
        }, 7000)

        $close.one('click', function(){
            clearTimeout(time)
            $note.one('webkitTransitionEnd transitionend', function(){
                $(this).remove()
                self.resetPositions()
            })
            $note.removeClass('active')
        })
    }

}

let notific = new Notification;

if($('.notification').length > 0){
    $('.notification').each(function(index, el) {
        let $this = $(this)
        setTimeout(function() {
            let icon = $this.attr("data-icon")
            let text = $this.text()
            let type = $this.attr("data-type")
            notific.setNote(icon, text, type)
        }, 500 * index)
    });
}



