$.validator.addMethod('comparedate', function (value, element, param) {
    return this.optional(element) || parseInt(new Date(value).getTime()) >= parseInt(new Date($(param).val()).getTime());
}, 'Invalid value');

$.validator.addMethod('schedule', function (value, element, param) {
    let count = 0
    console.log(param)
    $(param).each(function(index, el) {
        if($(param).val() == ""){
            count++
        }
    });
    console.log(count)
    return count > 0 ? false : true;
}, 'Invalid value');

$.validator.addMethod("latin", function(value, element) {
    return this.optional(element) || /^[a-z0-9\-!@#$%^&*()_+=.\/|?]+$/i.test(value);
}, "Password must contain only letters, numbers, or sybols.");

$.validator.addMethod("emailEqually", function(value, element) {
    let $email = $(element).parents('form').find('[type="email"]').val()
    return ($email == value) ? false : true
}, "Password should not match the email address.");


$.validator.addMethod("balance", function(value, element) {
    let balance = $(element).attr('data-balance')
    balance = parseInt(balance)
    value = parseInt(value)
    return (balance < value) ? false : true
}, "You have exceeded your balance");


$.validator.addMethod("lettersNumbers", function(value, element) {
    return this.optional(element) || /^[a-z0-9]+$/i.test(value);
}, "This field must contain only letters, numbers.");


$.validator.addMethod("phonev", function(value, element) {
    return this.optional(element) || /^[0-9() +-]+$/i.test(value);
}, "Please enter a valid phone number");

$.validator.addMethod("tax", function(value, element) {
    return this.optional(element) || /^[0-9-]+$/i.test(value);
}, "Please enter a valid tax");

$.validator.addMethod("space", function(value, element){
    let letter = value.charAt(0)
    return (letter == " ") ? false : true
}, "Letters and spaces only please"); 

$.validator.addMethod("address", function(value, element){
    let coordinates = $(element).parents('form').find('[name="coordinates"]').val()
    return (coordinates == "") ? false : true
}, "Please select Address dopdown."); 


$.validator.addMethod("time", function(value, element) {
    return this.optional(element) || /^[0-9:APM ]+$/i.test(value);
}, "Please enter a valid time");







$.validator.addMethod("checkUser", function(value, element) {
    let emailList = [
        {
            "email": "test@test.com"
        },
        {
            "email": "user@user.com"
        },
        {
            "email": "admin@admin.com"
        },
        {
            "email": "aram@aram.com"
        },
        {
            "email": "art@art.com"
        }
    ]

    let result = emailList.filter(item => item.email == value);
    if(result.length == 0){
        return false
    }else{
        return true
    }
    
}, "User is not found with provided email.");


jQuery.validator.addMethod("myEmail", function(value, element) {
    return this.optional( element ) || ( /^[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,4}$/.test( value ) && /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test( value ) );
}, 'Please enter valid email address.');


class Validate {
	constructor (form) {
		this.form = $(form)
	}
	validation(){
        let form = this.form.validate({
            ignore: ".ignore" 
        })
        this.validate = form
    }
    events(){
        this.form.on('change keyup', () => {
            let isValid = this.validate.checkForm();
            if(isValid){
                this.form.find('[type="submit"]').removeAttr('disabled')
            }else{
                this.form.find('[type="submit"]').attr('disabled', 'disabled')
            }
        });
    }
    init(){
        this.validation()
        this.events() 
    }
    activeButton(){
        this.form.validate()
    }
}

if($("#fullSearch").length > 0){
    var form = new Validate('#fullSearch')
    form.init()
}

if($("#footerForm").length > 0){
    var form = new Validate('#footerForm')
    form.init()
}

if($("#question").length > 0){
    var form = new Validate('#question')
    form.init()
    $("#question").submit(function(event) {
        event.preventDefault()
        if($(this).valid()){

            let url = $(this).attr('data-url');
            let formData = $(this).serialize();
            let resultHideDelay = $("#resultModal").attr('data-hide-delay') || 4000;

            axios({
                method: 'post',
                url: url,
                data: formData,
            })
            .then(function (response) {
                let icon = "";
                let msg = response.data.message;
                if(response.data.status == "success"){
                    icon = "icn-success"
                }else{
                    icon = "icn-wrong"
                }
                $("#resultModal i").addClass(icon)
                $("#resultModal p").html(msg)

                $("#questionModal").modal('hide')
                $("#questionModal").one('hidden.bs.modal', function () {
                    $("#question")[0].reset()
                    $("#resultModal").modal('show')
                })

                setTimeout(function(){
                    $("#resultModal").modal('hide')
                }, resultHideDelay)

            })
            .catch(function (response) {
                let msg = `Error, the server has sent status ${response.stautus}. <br> Please, try again...`
                
                $("#resultModal i").addClass('icn-wrong')
                $("#resultModal p").html(msg)

                $("#questionModal").modal('hide')
                $("#questionModal").one('hidden.bs.modal', function () {
                    $("#resultModal").modal('show')
                })

                setTimeout(function(){
                    $("#resultModal").modal('hide')
                }, 4000)
            })
        }
    });
}

if($("#singIn").length > 0){
    var form = new Validate('#singIn')
    form.init()
}

if($("#singUp").length > 0){
    var form = new Validate('#singUp')
    form.init()
}

if($("#forgot").length > 0){
    var form = new Validate('#forgot')
    form.init()
    $("#forgot").submit(function(event) {
        event.preventDefault()
        if($(this).valid()){
            let url = $(this).attr('data-action')
            let email = $(this).find('[name="email"]').val() 
            $('#checkEmailModal .modals__subtitle strong').text(email)
        
            $('#forgotModal').one('hidden.bs.modal', function () {
              $('#checkEmailModal').modal('show')
            })
            $('#forgotModal').modal('hide')
        }
    });
}

if($("#resetPassword").length > 0){
    var form = new Validate('#resetPassword')
    form.init()
}

if($("#resetPasswordCheck").length > 0){
    $('#resetPasswordModal').modal('show')
}

if($("#foodAfternoon").length > 0){
    var form = new Validate('#foodAfternoon')
    form.init()
}

if($("#pleaseSingIn").length > 0){
    var form = new Validate('#pleaseSingIn')
    form.init()
}

if($("#contact").length > 0){
    var form = new Validate('#contact')
    form.init()
}

if($("#promoForm").length > 0){
    var form = new Validate('#promoForm')
    form.init()
}

if($("#promoForm2").length > 0){
    var form = new Validate('#promoForm2')
    form.init()
}

if($("#thankYou").length > 0){
    var form = new Validate('#thankYou')
    form.init()
}

if($("#cancel").length > 0){
    var form = new Validate('#cancel')
    form.init()
}

if($("#checkoutForm").length > 0){
    var form = new Validate('#checkoutForm')
    form.init()
}

if($("#kidForm").length > 0){
    var form = new Validate('#kidForm')
    form.init()
}

if($("#kidEditForm").length > 0){
    var form = new Validate('#kidEditForm')
    form.init()
}
if($("#settingsForm").length > 0){
    var form = new Validate('#settingsForm')
    form.init()
}

if($("#dellAcc").length > 0){
    var form = new Validate('#dellAcc')
    form.init()
}

if($("#changePassword").length > 0){
    var form = new Validate('#changePassword')
    form.init()
    $("#changePassword").submit(function(event) {
        event.preventDefault()
        if($(this).valid()){
            $('#changePasswordModal').modal('hide')
            notific.setNote('icn-success',
                        'Your password has been successfully changed.',
                        'reviewInfo-success')
        }
    });
}


if($("#getMoney").length > 0){
    var form = new Validate('#getMoney')
    form.init()
}


if($("#directDeposit").length > 0){
    var form = new Validate('#directDeposit')
    form.init()
}

if($("#payPalForm").length > 0){
    var form = new Validate('#payPalForm')
    form.init()
}

if($("#wireTransfer").length > 0){
    var form = new Validate('#wireTransfer')
    form.init()
}

if($("#addCompanyForm").length > 0){
    var form = new Validate('#addCompanyForm')
    form.init()
}

if($("#addSocialLink").length > 0){
    var form = new Validate('#addSocialLink')
    form.init()
}

if($("#addDoc").length > 0){
    var form = new Validate('#addDoc')  
    form.init()
}

if($("#addLocation").length > 0){
    var form = new Validate('#addLocation')  
    form.init()
}

if($("#addSchedule").length > 0){
    let addSchedule = new Validate('#addSchedule')  
    addSchedule.activeButton()

    //$('#addSchedule').validate()
}








