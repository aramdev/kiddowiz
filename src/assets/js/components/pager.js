var modulePager = {
    cacheDom() {
        this.$doc = $(document)
        this.$prev = $('.sitePager__item:first-child')
        this.$next = $('.sitePager__item:last-child')
    },
	clickTo (e) {
        var isCtrl = false;
        if(e.ctrlKey) isCtrl = true;
        if(e.keyCode == 37 && isCtrl) {
            let href = this.$prev.attr('href')
            if(href && !$("body").hasClass('modal-open')){
                window.location.href = href
            }
        }
        if(e.keyCode == 39 && isCtrl) {
            let href = this.$next.attr('href')
            if(href && !$("body").hasClass('modal-open')){
                window.location.href = href
            }
        }
    },
    resetClick (e) {
        if(e.ctrlKey) isCtrl = false;
    },
    events () {
        
        this.$doc.keydown(this.clickTo.bind(this))
    },
    init () {
        this.cacheDom()
        this.events()
    }
}
if($('.sitePager').length > 0){
    modulePager.init()
}


