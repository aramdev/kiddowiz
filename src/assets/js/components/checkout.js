if($('#checkout').length > 0){
    
var moduleCheckoutCalendar = {
    cacheDom() {
        this.calendar = $( ".checkout__calendar" )
        this.$win = $(window)
    },
    runCalendar () {
        let that = this
        this.calendar.each(function() {

            let str = $(this).attr('data-minDate');
            let dataDates = $(this).attr('data-dates');
            let minDate = str ? Date(str) : new Date()
            let calendar = $(this)
            $(this).datepicker({
                numberOfMonths: 2,
                dayNamesMin: ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"],
                minDate: minDate,
                dateFormat: 'mm/dd/yy',
                onChangeMonthYear () {
                    setTimeout(function(){
                        that.setDay($('body'), calendar)
                    }, 20)
                },
                beforeShow(){
                    setTimeout(function(){
                        that.setDay($('body'))
                    }, 20)
                },
                onSelect: function(date, el) {
                    var day  = el.selectedDay,
                    mon  = el.selectedMonth,
                    year = el.selectedYear;

                    var el = $(el.dpDiv).find('[data-year="'+year+'"][data-month="'+mon+'"]').filter(function() {
                        return $(this).find('a').text().trim() == day;
                    });




                    let $container = $('.checkout__leftBar')
                    
                    $container.find('.date').html(date)
                    $container.find('.time').html(el.find('.time span:first-child').html())
                    
                    
                    

                    
                    let $spans = el.find('.time span')
                    let timesLength = $spans.length

                    if(timesLength > 1){
                        $(this).parents('.selectContainer').next('.timeContainer').remove()
                        let html = ""
                        $spans.each(function(index, el) {
                            html += `<option value="${$(this).text()}">${$(this).text()}</option>`
                        });
                        $(this).parents('.selectContainer').after(`
                            <div class="col-md-4 col-xl-3 timeContainer">
                                <div class="field-case">
                                    <label class="field-name">
                                        Nearest Time
                                        <i class="icn-info-solid" data-container="element" data-placement="left" data-toggle="tooltip" title="hint for you"></i>
                                    </label>
                                    <select class="field" name="nearestTime" required="true" data-msg-required="Please select time">
                                        ${html}
                                    </select>
                                </div>
                            </div>
                        `)
                        $(this).parents('.selectContainer')
                                .next('.timeContainer')
                                .find('select')
                                .select2({
                                    dropdownParent: $(this).parents(".field-case"),
                                    minimumResultsForSearch: -1
                                })
                    }else{
                       $(this).parents('.selectContainer').next('.timeContainer').remove()
                    }

                    $(this).trigger('change')

                }

            });
            that.setDay($('body'))
        });
    },
    setDay (calendar) {
        let data = calendarDates
        //data = JSON.parse(data)
        for(let item of data){
            let $tds = calendar.find(`td[data-month="${item.month}"][data-year="${item.year}"]`)
            $tds.each(function(index, el) {
                let $td = $(this)
                let $a = $td.find('a')
                if($a.text() == item.day){
                    $td.attr('style', item.style);
                    $td.attr('data-type', item.type);
                    let inner = "";
                    
                    for(let i = 0; i < item.time.length; i++){
                        inner += `<span>${item.time[i]}</span>`
                    }

                    $td.append(`
                        <span class="time">
                            ${inner}
                        </span>
                    `)
                }
            });
        }
    },
    events () {},
    init () {
        this.cacheDom()
        this.runCalendar()
    }
}

moduleCheckoutCalendar.init()



var userCheck = true



var moduleCheckout = {
    cacheDom() {
        this.$body = $("body")

        this.$html = $("body, html")
        
        this.$modal = $("#kidModal")
        this.$kidForm = $("#kidForm")
        
        this.$modalEdit = $("#kidEditModal")
        this.$kidEditForm = $("#kidEditForm")
        this.$delete = this.$modalEdit.find('.js-delete')

        this.$checkoutForm = $('#checkoutForm')

        this.$calendar = this.$checkoutForm.find(".checkout__calendar")
        this.$nextBtn = this.$checkoutForm.find(".js-next")


        this.flagEmail = true
        this.$email = this.$checkoutForm.find(".js-email")
        
        

        this.$password = this.$checkoutForm.find(".js-password")
        this.$signIn = this.$checkoutForm.find(".js-singIn")


        this.$newpassword = this.$checkoutForm.find(".js-newpassword")
        this.$passwordconf = this.$checkoutForm.find(".js-passwordconf")
        this.$inputs = this.$checkoutForm.find("[data-case='userInfo'] input")

        this.$paymant = this.$checkoutForm.find('[name="paymant"]')
        this.$paymentCheck = this.$checkoutForm.find('[name="paymentCheck"]')
    
        this.$title = this.$checkoutForm.find('.checkout__stepTitle')

        
        this.openSchedule = this.$checkoutForm.find(".js-openSchedule")
        this.schedule = this.$checkoutForm.find(".js-schedule")


        this.$seats = this.$checkoutForm.find('[name="seats"]')
        


        this.children = [];
        this.kids = new Kids(this.children);
        this.kids.birthDay('input[name="birthday"]')

        /*this.location = new Location('#checkoutForm [name="location"]')
        this.location.init()*/

        this.timeoutPass = ""



    },
    activatedNextButton(e){
        let $el = $(e.currentTarget)
        let $parent = $('.step1')
        let $conatainer = $('.selectContainer')
        $parent.find('.js-next').removeClass('disabled')
    },
    nextStepActions(el){
        el.parent('.btnsBox').hide()
        let $parent = el.parents('.checkout__step')
        $parent.removeClass('show')
        $parent.next().addClass('active show') 
    },
    nextStep(e){
        let $el = $(e.currentTarget)
        let action = $el.attr('data-action') || false;
        

        if(action == "email"){
            
            this.nextStepActions($el)
            this.$email.removeAttr('disabled')
            this.$email.trigger('focus')
            let head = $('.header').outerHeight();
            let pos = $('.step2').offset().top  - head - 10
            this.$html.animate({scrollTop:pos+'px'}, 300);

        }else if(action == 'registration'){
            let $parent = $el.parents('.checkout__step')
            $parent.prepend('<div class="loader-circle"></div>')
            setTimeout(() => {
                $('[data-case="newpassword"]').addClass('hidden')
                this.nextStepActions($el)
                $parent.find('.loader-circle').remove()
            }, 500)
        }else if(action == "payment"){
            this.$paymentCheck.val(10).trigger('change') 
            this.nextStepActions($el)
        }else{
            this.nextStepActions($el)
        }
    },
    toggleStep(e){
        let $el = $(e.currentTarget)
        $el.parent('.checkout__step').toggleClass('show');
    },
    checkEmail (e) {
        setTimeout(() => {
            let $el = $(e.currentTarget)
            let $parent = $el.parents('.step2')
            if(!$el.hasClass('error')){
                if(this.flagEmail == true){
                    $parent.prepend('<div class="loader-circle"></div>') 
                    $('[data-note="email"]').hide()
                    setTimeout(() => {
                        if(userCheck){
                            $('[data-case="password"]').removeClass('hidden')
                            this.$password.removeAttr('disabled')
                            this.$password.trigger('focus')
                        }else{
                            $('[data-case="newpassword"]').removeClass('hidden')
                            this.$newpassword.removeAttr('disabled')
                            this.$passwordconf.removeAttr('disabled')
                            this.$newpassword.trigger('focus')
                        }
                        $('[data-case="userInfo"]').removeClass('hidden')
                        $parent.find('.loader-circle').remove()
                        this.flagEmail = false
                    }, 500)
                }
            }               
        }, 30)
    },
    activateSignIn(e){
        //let $el = $(e.currentTarget)
        //clearTimeout(this.timeoutPass)
    },
    checkPass (e){
        let $el = $(e.currentTarget)
        let $parent = $el.parents('.step2')

        if(this.$password.val() == ""){
            this.$password.addClass('error').after('<label id="password-error" class="error" for="password">Please enter your password.</label>')
        }else{
            setTimeout(() => {
                if(!this.$password.hasClass('error')){
                    $parent.prepend('<div class="loader-circle"></div>') 
                    $('[data-note="password"]').hide()
                    setTimeout(() => {
                        var userData = {
                            fname: "Jhone",
                            lname: "Smith",
                            phone: "+1 (465) 465-4654",
                            location: 'USA California San Diego',
                        }

                        $parent.find('[name="firstname"]')
                            .removeAttr('disabled')
                            .val(userData.fname)
                            
                        $parent.find('[name="lastname"]')
                            .removeAttr('disabled')
                            .val(userData.lname)
                            
                        $parent.find('[name="phone"]')
                            .removeAttr('disabled')
                            .val(userData.phone)
                            
                        $parent.find('[name="location"]')
                            .removeAttr('disabled')
                            .val(userData.location)
                            
                        $parent.find('.js-next')
                            .removeClass('disabled')
                            .removeAttr('data-action');
                        $('[data-case="password"]').addClass('hidden')
                        $parent.find('.loader-circle').remove()
                    }, 500)
                }               
            }, 30)
        }
    },
    checkNewPass(e){
        setTimeout(() => {
            let $el = $(e.currentTarget)
            let $parent = $el.parents('.step2')
            if(!$el.hasClass('error')){
                $('[data-case="userInfo"] input').removeAttr('disabled')
                $('[data-case="userInfo"] [name="firstname"]').trigger('focus')
            }               
        }, 30)
    },
    toKids(e){
        let $el = $(e.currentTarget)
        let $parent = $el.parents('.step2')
        
        
        let inputs = $('[data-case="userInfo"] input').not('[name="location"]')
        
        let flagText = 0;
        let flagError = 0;

        setTimeout(function(){
            
            inputs.each(function(index, el) {
                let text = $(el).val()
                let error = $(el).hasClass('error')

                
                if(text == ""){
                    flagText++
                }
                if(error){
                    flagError++
                }
            });

            if(flagText == 0 && flagError == 0){
                $parent.find('.js-next').removeClass('disabled')
            }else{
                $parent.find('.js-next').addClass('disabled')
            }
        }, 30)
    },
    addChild(e){
        e.preventDefault()
        let $el = $(e.currentTarget)
        this.kids.add('#kidModal', '#kidForm')
        this.outputKidList()
    },
    editChild (e){
        e.preventDefault()
        let $el = $(e.currentTarget)
        let childId = $el.find('[name="id"]').val()
        this.kids.edit('#kidEditModal', '#kidEditForm', childId)
        this.outputKidList()
    },
    setChild(e) {
        let $el = $(e.currentTarget)
        let childId =  $el.attr('data-id');
        this.kids.set('#kidEditModal', '#kidEditForm', childId)
    },
    deteleChild(e){
        let $el = $(e.currentTarget)
        let childId = $el.parents('#kidEditModal').find('[name="id"]').val()
        this.kids.delete('#kidEditModal', '#kidEditForm', childId)
        this.outputKidList()

        if(this.children.length == 0){
            this.$paymentCheck.val('').trigger('change') 
            this.$seats.val(0).trigger('change')
            $(".step3 .btnsBox .btns").addClass('disabled')
        }else{
            let flag = 0
            this.children.forEach(function(element, i) {
                if(element.checked == true){
                   flag++
                }
            });
            if(flag <= 0){
               this.$paymentCheck.val('').trigger('change') 
               this.$seats.val(0).trigger('change')
               $(".step3 .btnsBox .btns").addClass('disabled')
            }
        }
    },
    outputKidList(){
        let container = $('.checkout__kids .items')
        container.html('')
        let child = ""
        let count = 0;
        for(let item of this.children){
            let checked
            if(item.checked){
                checked = "checked"
            }else{
                checked = ""
            }
            var value = JSON.stringify(item);
            child += `
                <div class="item">
                    <label class="check-case">
                        <input name="${item.ind}" type="checkbox" ${checked} value='${value}'>
                        <span class="field-checkbox">
                            <i class="icn-checkbox"></i>
                        </span>
                        <div class="check-name">
                            ${item.firstname}
                            ${item.lastname}
                        </div>
                    </label>
                    <div class="old">${item.old} years old</div>
                    <a class="link link-default js-editChild" data-id="${item.ind}">
                        <i class="icn-edit"></i>
                        Edit
                    </a>
                </div>
            `
            
        }
        container.append(child)
    },
    resetForm(e){
        let $el = $(e.currentTarget)
        $el.find('[type="reset"]').trigger('click')
        $el.find('.select2-selection__choice__remove').trigger('click')
        $el.find('.btns').attr('disabled', 'disabled');
        setTimeout(function(){
            $el.find('.field').removeClass('error')
            $el.find('label.error').hide()
        }, 300)
    },
    changeStatus(e){
        let $el = $(e.currentTarget)
        let name =  $el.attr('name');
        let seatCount = parseInt($('.seatCount').text())

        let num = "";
        let count = 0;

        this.children.forEach(function(element, i) {
            if(element.ind == name){
               num = i 
            }
            if(element.checked == true){
               count++
            }
        });

        if($el.is(":checked")){
            if(count >= seatCount){
                $el.parents('.step3').find('.alert').slideDown("slow", function() {
                    setTimeout(() => {
                        $(this).slideUp("slow")
                    }, 8000)
                });
                $el.prop('checked', false)
            }else{
                this.children[num].checked = true
                this.$seats.val(count + 1).trigger('change')
            }    
        }else{
            this.children[num].checked = false
            this.$seats.val(count - 1).trigger('change') 
        }
        this.toPayment()
    },
    toPayment(){
        let num = 0
        this.children.forEach(function(element, i) {
            if(element.checked == true){
               num++ 
            }
        });
        if(num > 0){
            $('.step3').find('.js-next').removeClass('disabled')
        }else{
            $('.step3').find('.js-next').addClass('disabled')
        }

        if($('.step4').hasClass('active')){
            if(num > 0){
                this.$paymentCheck.val(10).trigger('change') 
            }else{
                this.$paymentCheck.val('').trigger('change') 
            }
        }
    },
    countingPrice(e){
        let $el = $(e.currentTarget)
        let val = $el.val()
        let price = $('[name="price"]').val()
        if(val > 0){
            let sum = val * price
            $('.checkout__leftBar').find('.text-success').addClass('hide')
            $('.checkout__leftBar').find('.total').removeClass('hide')
            $('.checkout__leftBar .total .value strong .val').text(sum)
            let dollar = new CurrencyRate(sum, '.checkout__leftBar .total .value strong .conv')
            dollar.init()
            let inbutton = new CurrencyRate(sum, '.checkout__botBar .btns span')
            inbutton.init()
        }else{
           $('.checkout__leftBar').find('.text-success').removeClass('hide')
           $('.checkout__leftBar').find('.total').addClass('hide') 
           $('.checkout__botBar .btns span').text(0)
        }
    },
    changePayment (e){
        let $el = $(e.currentTarget)
        if($el.val() == 'paypal'){
            $('.paypal').show()
            $('.stripe').hide()
        }else{
            $('.paypal').hide()
            $('.stripe').show()
        }
    },
    changeTime(e){
        let $el = $(e.currentTarget)
        let $container = $('.checkout__leftBar')
        $container.find('.time').html($el.val())
    },
    showSchedule (e){
        e.stopPropagation()
        let $parent = $(e.currentTarget).parents(".checkout__info")
        $parent.toggleClass('active');
    },
    hideSchedule(){

        $(".checkout__info").removeClass('active')
    },
    changeSchedule (e){
        let $el = $(e.currentTarget)
        let val = $el.val()
        let html = $(`.js-schedule[value="${val}"]`).next('.content').html()
        
        let $parent = $el.parents('.checkout__info')
        $parent.find('.cnt').html(html)
        $parent.removeClass('active')
    },
    events () {
        this.$calendar.on( "change", this.activatedNextButton.bind(this));
        this.$nextBtn.on('click', this.nextStep.bind(this));
        
        this.$email.on('change', this.checkEmail.bind(this));
        
        //this.$password.on('change', this.activateSignIn.bind(this));
        this.$signIn.on('click', this.checkPass.bind(this));
        this.$passwordconf.on('change', this.checkNewPass.bind(this));
        

        this.$inputs.on('change', this.toKids.bind(this));
        
        this.$modal.on('hide.bs.modal', this.resetForm.bind(this));
        this.$modalEdit.on('hide.bs.modal', this.resetForm.bind(this));
         
        this.$seats.on('change', this.countingPrice.bind(this));
        

        this.$kidForm.on('submit', this.addChild.bind(this));
        this.$body.on('click', '.js-editChild', this.setChild.bind(this));
        this.$kidEditForm.on('submit', this.editChild.bind(this));
        this.$delete.on('click', this.deteleChild.bind(this));

        
        this.$body.on('change', ".checkout__kids .item .check-case input", this.changeStatus.bind(this));

        this.$paymant.on('change', this.changePayment.bind(this));

        this.$title.on('click', this.toggleStep.bind(this));

        


        this.openSchedule.on('click', this.showSchedule.bind(this));
        this.schedule.on('click', this.changeSchedule.bind(this));

        this.$body.on('click', this.hideSchedule.bind(this));
        
        this.$body.on('change', "[name='nearestTime']", this.changeTime.bind(this));


        this.$body.on('focus', ".js-comment", function(e){$(e.currentTarget).addClass('active')});
    },
    init () {
        this.cacheDom()
        //this.countingPrice()
        this.events()
    }
}

moduleCheckout.init()


}

