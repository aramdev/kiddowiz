if($('#addScheduleModal').length > 0){
var moduleAddSchedule = {
    cacheDom() {
        this.$body = $("body")
        
        this.$addScheduleModal = $('#addScheduleModal')
        this.$confirmScheduleModal = $('#confirmScheduleModal')
        this.$addScheduleForm = $('#addSchedule')
        this.scheduleSubmit = false;
        this.schedules = []
        this.schCount = 0


        this.$start = $('.js-kidStart')
        this.$end = $('.js-kidEnd')
        this.$range = $('.js-range')

        this.$daterange = $(".js-daterange")
        this.period = ""
    },
    runDateRange(){
        let today = new Date();
        //let tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
        this.$daterange.each(function(index, el) {
            let place = $(this).parents(".field-case")
                    
            $(this).daterangepicker({
                opens: 'right',
                singleDatePicker: true,
                parentEl: place,
                autoApply: false,
                changeMonth : true,  
                minDate: today,
                locale: {
                    //cancelLabel: 'Clear',
                    format: 'DD MMMM YYYY',
                    monthNames: [
                        "January",
                        "February",
                        "March",
                        "April",
                        "May",
                        "June",
                        "July",
                        "August",
                        "September",
                        "October",
                        "November",
                        "December"
                    ],
                    daysOfWeek: [
                        'sun',
                        'mon',
                        'tue',
                        'wed', 
                        'thu',
                        'fri',
                        'sat'
                    ]
                },
            }).on('apply.daterangepicker', function(ev, picker) {
                //$(this).val(picker.startDate.format('DD MMMM YYYY'));
                $(this).removeClass('error')
                $(this).parents('.field-case').find('label.error').remove() 
                /*let $this = $(this)
                setTimeout(function(){
                    $this.valid()
                }, 500)*/
            })
        });
    },

    getPeriod(e) {
        let from = $('#addSchedule [name="daterangeForm"]').val()
        let to = $('#addSchedule [name="daterangeTo"]').val()

        let fromTime = (new Date(from)).getTime()
        let toTime = (new Date(to)).getTime()
        
        if(fromTime <= toTime){
            let count = 0
            let dates = this.getDates(new Date(from), new Date(to));

            for(let date in dates){
                if(date == 0){
                    count++ 
                }else{
                    if(dates[date-1].getMonth() != dates[date].getMonth()){
                        count++
                    }   
                }
            }
            this.period = from+"-"+to
            this.runCalendar(from, to, count)

            if($('.timesBlock .item').length == 0){
                $('[name="timeCount"]').val("")
                $('[name="timeCount"]').valid()
            }else{
                $('.timesBlock .item').first().trigger('click')
                $('[name="timeCount"]').val($('.timesBlock .item').length).valid()
                $('[name="timeCount"]').valid()
            }
        }else{
            $('.calendarBox').html(
                `<div class="alert alert-danger text-center" role="alert">
                    The start date is less than the end date.
                </div>`
                
            )
            $('[name="timeCount"]').val("")
            $('[name="timeCount"]').valid()
        }

       

    },
    getDates (startDate, endDate) {
      var dates = [],
          currentDate = startDate,
          addDays = function(days) {
            var date = new Date(this.valueOf());
            date.setDate(date.getDate() + days);
            return date;
          };
      while (currentDate <= endDate) {
        dates.push(currentDate);
        currentDate = addDays.call(currentDate, 1);
      }
      return dates;
    },
    addDates(e) {
        let $el = $(e.currentTarget)
        let day = $el.attr('data-day');
        let period = this.period.split("-")
        
        var dates = this.getDates(new Date(period[0]), new Date(period[1]));
        var currentDates = pickmeup('.calendar').get_date()
        
        if(day == "weekdays"){
            dates.forEach(function(date, index) {
                if(
                    date.getDay() == 1 || 
                    date.getDay() == 2 || 
                    date.getDay() == 3 || 
                    date.getDay() == 4 || 
                    date.getDay() == 5
                ){
                    currentDates.push(date) 
                }
            });
        }else if(day == "weekends"){
            dates.forEach(function(date, index) {
                if(date.getDay() == 6 || date.getDay() == 0){
                    currentDates.push(date) 
                }
            });
        }else if(day == "all"){
            currentDates = dates
        }else{
            dates.forEach(function(date, index) {
                if(date.getDay() == day){
                    currentDates.push(date) 
                }
            });
        }
        
        $(".addEvent .timesBlock .item.active input[type='hidden']").val(currentDates)
        $(".addEvent .timesBlock .item.active").trigger('click')
        $('[name="timeCount"]').valid()
    },
    removeDates(e) {
        let $el = $(e.currentTarget)
        let day = $el.attr('data-day');
        let period = this.period.split("-")
        
        
        var currentDates = pickmeup('.calendar').get_date()
        

        if(day == "weekdays"){
            for(let i = 0; i < currentDates.length; i++){ 
                if ( 
                    (currentDates[i].getDay() == 1) || 
                    (currentDates[i].getDay() == 2) || 
                    (currentDates[i].getDay() == 3) || 
                    (currentDates[i].getDay() == 4) || 
                    (currentDates[i].getDay() == 5) 
                ){
                    currentDates.splice(i, 1); 
                    i--
                }
            }
        }else if(day == "weekends"){
            for(let i = 0; i < currentDates.length; i++){ 
                if ( (currentDates[i].getDay() == 6) || (currentDates[i].getDay() == 0)) {
                    currentDates.splice(i, 1); 
                    i--
                }
            }
        }else if(day == "all"){
            currentDates = []; 
        }else{
            for(let i = 0; i < currentDates.length; i++){ 
                if ( currentDates[i].getDay() == day) {
                    currentDates.splice(i, 1); 
                    i--
                }
            }
        }

       
        $(".addEvent .timesBlock .item.active input[type='hidden']").val(currentDates)
        $(".addEvent .timesBlock .item.active").trigger('click')
        $('[name="timeCount"]').valid()
    },
    runCalendar(min, max, count, dates){
        let $el = $('.calendarBox')
        $el.html("")
        pickmeup.defaults.locales['en'] = {
            daysMin: [
                '<div data-day-week="0" class="week"></div> Sun', 
                '<div data-day-week="1" class="week"></div> Mon', 
                '<div data-day-week="2" class="week"></div> Tue', 
                '<div data-day-week="3" class="week"></div> Wed', 
                '<div data-day-week="4" class="week"></div> Thu', 
                '<div data-day-week="5" class="week"></div> Fri', 
                '<div data-day-week="6" class="week"></div> Sat'
            ],
            months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        };

        let minDate = min ? new Date(min) : new Date()
        let maxDate = max ? new Date(max) : new Date() 
        let calendars = count || 1
        let date = dates || []
        $el.append('<div class="calendar"></div>')
        
        pickmeup('.calendar', {
            flat : true,
            mode : 'multiple',
            calendars : calendars,
            min: minDate,
            max: maxDate,
            date: date,
            prev: "<i class='icn-angle-left'></i>",
            next: "<i class='icn-angle-right'></i>",
            first_day: 0,
            render : function (date) {
                return { 
                    class_name : `day-${date.getDay()}`
                }
            }
            
        })

        $('.calendar').on('pickmeup-change', function (e) {
            $(".addEvent .timesBlock .item.active input[type='hidden']").val(e.detail.date)
            $('[name="timeCount"]').valid()
        });
    },

    addWeek(e) {
        let $el = $(e.currentTarget)
        let weekDay = $el.attr('data-day-week');


        //var currentDates = pickmeup('.calendar').get_date()
        

        /*let month = $el.parents(".pmu-instance").find(`.pmu-month`).text()
        let dates = getDaysInMonth(month)

        console.log(dates)*/
        
        //$(".addEvent .timesBlock .item.active input[type='hidden']").val(currentDates)
        //$(".addEvent .timesBlock .item.active").trigger('click')
        
        /*days.each(function(index, el) {
            $(this).trigger('click')
        });*/
    },

    startTime (e) {
        let $el = $(e.currentTarget)

        let str = $el.val() + " - " + $(".js-endTime").val()
        $('.timesBlock .item.active input[type="text"]').val(str)
        //setTimeout(function(){
        //    if(!$el.hasClass('error')){
        //    }
        //}, 300)
    },
    endTime (e) {
        let $el = $(e.currentTarget)
        let str = $(".js-startTime").val() + " - " + $el.val()
        $('.timesBlock .item.active  input[type="text"]').val(str)
        //setTimeout(function(){
        //    if(!$el.hasClass('error')){
        //    }
        //}, 300)
    },
    setTime (e) {
        let $el = $(e.currentTarget) 
        $('#addSchedule .timesBlock .item').removeClass('active')
        $el.addClass('active')


        let currentTimes = $el.find('input[type="text"]').val()
        currentTimes = currentTimes.split(" - ")
        
                
        $("#addSchedule .js-startTime").val(currentTimes[0])
        $("#addSchedule .js-endTime").val(currentTimes[1])

        let currentDates = $el.find('input[type="hidden"]').val()
        currentDates = currentDates.split(",")
        let dates = []


        for(let i = 0; i < currentDates.length; i++){
            let date = new Date(currentDates[i])
            dates.push(date)
        }

        if(dates.length > 1){
            pickmeup('.calendar').set_date(dates);
        }else{
            pickmeup('.calendar').clear(); 
        }
    },
    addTime (e) {
        let $el = $(e.currentTarget)
        if($('.timesBlock .item').length == 0){
            this.getPeriod()
        }
        let $item = `<div class="item">
                        <input name="itemTime[]" readonly="" type="text" value="12:00 AM - 01:00 PM">
                        <input name="itemDates[]" type="hidden" value="">
                        <i class="icn-close"></i>
                    </div>`

        //$("#addSchedule .js-startTime").val('12:00 AM')
        //$("#addSchedule .js-endTime").val('01:00 PM')
        $el.before($item)
        $el.prev('.item').trigger('click')



        $('[name="timeCount"]').val($('.timesBlock .item').length)
        $('[name="timeCount"]').valid()

        
    },
    removeTime (e) {
        e.stopPropagation()
        let $el = $(e.currentTarget) 
        let $parent = $el.parents('.item') 
        $parent.remove()
        
        if($('.timesBlock .item').length == 0){
            $('[name="timeCount"]').val("")
            $('.calendarBox').html(
                `<div class="alert alert-danger text-center" role="alert">
                    Please add times schedule.
                </div>`
                
            )
        }else{
            $('.timesBlock .item').first().trigger('click')
            $('[name="timeCount"]').val($('.timesBlock .item').length)
        }

        $('[name="timeCount"]').valid()
    },

    showTotal(){
        let currency = $('[name="currency"]').val()
        let priceType = $('[name="priceType"]').val()
        let price = $('.js-price').val()
        let $total = $('#addSchedule .total')

        if(price != "" && currency != "" && priceType != "" ){
            $total.addClass('active')
        }else{
            $total.removeClass('active')
        }
    },
    countingPrice () {
        let price = $('.js-price').val()
        let discount = $('.js-discount').val()
        let $discountType = $('#addSchedule [name="discountType"]')
        let $total = $('#addSchedule .total')

        
        if(price != ""){ 
			if(price != 0){
				$('.js-discount').removeAttr('disabled')
				if(discount != ""){
					$discountType.removeAttr('disabled')
					let sum = 0
					
					if($('#addSchedule [name="discountType"]:checked').val() == "percentage"){
						sum = price / 100 * (100 - discount)
					}else{
						sum = price - discount
					}

					sum = parseFloat(sum.toFixed(1))
					if(sum < 0){
						sum = 0
					}
					$total.find('.sum').text(sum)
				}else{
					$discountType.attr('disabled', true)
					$total.find('.sum').text(parseInt(price))
				}
			}else{
				$('.js-discount').val("").attr('disabled', true)
                $discountType.attr('disabled', true)
                $total.find('.sum').text(0)
			}
            
        }else{
            $total.find('.sum').text(0)
        }


        this.showTotal() 
    },

    setCurrency (e) {
        let $el = $(e.currentTarget) 
        let val = $el.val()
        let $total = $('#addSchedule .total')
        $total.find('.currency').text(val)

        this.showTotal() 
    }, 
    setPriceType (e) {
        let $el = $(e.currentTarget) 
        let val = $el.val()
        let $total = $('#addSchedule .total')
        $total.find('.type').text(val)

        this.showTotal() 
    }, 

    
    runSlider () {
        let tath = this 
        this.$range.slider({
            range: true,
            min: 0,
            max: 18,
            values: [ 0, 18 ],
            slide: function( event, ui ) {
                tath.$start.val(ui.values[ 0 ]).trigger('change')
                tath.$end.val(ui.values[ 1 ]).trigger('change')
            }
        });
    },
    changeStart (e) {
        let $el = $(e.currentTarget) 
        let val = parseInt($el.val())
        let endVal = parseInt(this.$end.val())


        if(val > this.$end.val() && this.$end.val() != ""){
            $el.val(endVal)
            val = endVal
        }

        this.$range.slider('values', 0, val);
        $el.valid()

    },
    changeEnd (e) {
        let $el = $(e.currentTarget) 
        let val = parseInt($el.val())
        let startVal = parseInt(this.$start.val())

        if(val < this.$start.val() && this.$start.val() != ""){
            $el.val(startVal)
            val = startVal
        }
        
        this.$range.slider('values', 1, val);
        $el.valid()


    },

    
    /* 
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
        Accusamus illum facere voluptatem, porro deserunt omnis 
        quibusdam qui, nulla iusto natus ea accusantium repudiandae ab, modi 
        perferendis voluptate distinctio non maiores.
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
        Accusamus illum facere voluptatem, porro deserunt omnis 
        quibusdam qui, nulla iusto natus ea accusantium repudiandae ab, modi 
        perferendis voluptate distinctio non maiores.
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
        Accusamus illum facere voluptatem, porro deserunt omnis 
        quibusdam qui, nulla iusto natus ea accusantium repudiandae ab, modi 
        perferendis voluptate distinctio non maiores.
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
        Accusamus illum facere voluptatem, porro deserunt omnis 
        quibusdam qui, nulla iusto natus ea accusantium repudiandae ab, modi 
        perferendis voluptate distinctio non maiores.
    */
    
    // Add links
    addSchedule (e){
        let $el = $(e.currentTarget)
        if($el.valid()){
            e.preventDefault()
            let status = $el.attr('data-status');
            if(status == 'new'){
                this.schCount++
                
                let item = {}
                item.title = $el.find('[name="title"]').val() 
                item.ind = item.title + this.schCount.toString()
                item.shortDescription = $el.find('[name="shortDescription"]').val() 
                item.groupSize = $el.find('[name="groupSize"]').val() 
                item.seatsAvailable = $el.find('[name="seatsAvailable"]').val() 
                item.kidsAgeFrom = $el.find('[name="kidsAgeFrom"]').val() 
                item.kidsAgeTo = $el.find('[name="kidsAgeTo"]').val() 
                item.daterangeForm = $el.find('[name="daterangeForm"]').val() 
                item.daterangeTo = $el.find('[name="daterangeTo"]').val() 
                item.timeCount = $el.find('[name="timeCount"]').val() 
                item.datesaleFrom = $el.find('[name="datesaleFrom"]').val() 
                item.datesaleTo = $el.find('[name="datesaleTo"]').val() 
                item.price = $el.find('[name="price"]').val() 
                item.priceCount = $el.find('.total .sum').text() 
                item.currency = $el.find('[name="currency"]').val() 
                item.priceType = $el.find('[name="priceType"]').val() 
                item.discount = $el.find('[name="discount"]').val() 
                item.discountType = $el.find('[name="discountType"]:checked').val() 
                item.enabled = $el.find('[name="enabled"]').val() 

                item.itemTime = $el.find('[name="itemTime[]"]').map(function(){return $(this).val();}).get();
                item.itemDates = $el.find('[name="itemDates[]"]').map(function(){return $(this).val();}).get();

                
                this.schedules.push(item) 
                console.log(this.schedules)
                
            
            }else{
                let id = $el.find('[name="ind"]').val()
                let num = ""

                this.schedules.forEach(function(element, i) {
                    if(element.ind == id){
                       num = i 
                    }
                });

                this.schedules[num].title = $el.find('[name="title"]').val() 
                this.schedules[num].shortDescription = $el.find('[name="shortDescription"]').val() 
                this.schedules[num].groupSize = $el.find('[name="groupSize"]').val() 
                this.schedules[num].seatsAvailable = $el.find('[name="seatsAvailable"]').val() 
                this.schedules[num].kidsAgeFrom = $el.find('[name="kidsAgeFrom"]').val() 
                this.schedules[num].kidsAgeTo = $el.find('[name="kidsAgeTo"]').val() 
                this.schedules[num].daterangeForm = $el.find('[name="daterangeForm"]').val() 
                this.schedules[num].daterangeTo = $el.find('[name="daterangeTo"]').val() 
                this.schedules[num].timeCount = $el.find('[name="timeCount"]').val() 
                this.schedules[num].datesaleFrom = $el.find('[name="datesaleFrom"]').val() 
                this.schedules[num].datesaleTo = $el.find('[name="datesaleTo"]').val() 
                this.schedules[num].price = $el.find('[name="price"]').val() 
                this.schedules[num].priceCount = $el.find('.total .sum').text()  
                this.schedules[num].currency = $el.find('[name="currency"]').val() 
                this.schedules[num].priceType = $el.find('[name="priceType"]').val() 
                this.schedules[num].discount = $el.find('[name="discount"]').val() 
                this.schedules[num].discountType = $el.find('[name="discountType"]').val() 
                this.schedules[num].enabled = $el.find('[name="enabled"]').val() 
                this.schedules[num].itemTime = $el.find('[name="itemTime[]"]').map(function(){return $(this).val();}).get();
                this.schedules[num].itemDates = $el.find('[name="itemDates[]"]').map(function(){return $(this).val();}).get();

                
                
            }
            notific.setNote('icn-success',
               'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
               'reviewInfo-success')
            this.renderSchedules()
            this.scheduleSubmit = true
            this.detectScheduleConfirm()
            this.$addScheduleModal.modal('hide')
        }
        
    },
    renderSchedules(){
        let schedule = ""
        
        





        for(let item of this.schedules){

            //console.log(item.discount)
            //console.log(item.discountType)

            let badges = ""
            let percentage = ""
            let price = ""
            let showong = ""
            

            if(item.discount != "" || item.discount != 0){
                if(item.discountType == "percentage"){
                    percentage = '%'
                }
                badges = `<span class="badges badges-discount">-${item.discount}${percentage}</span>`

                price = `<del>${item.currency}${item.price}</del>`
            }

            

            if(item.enabled == "true"){
                showong = `
                    <span class="text-success">Is showing in the catalog</span>
                    <a class="link link-primary js-showing show" tabindex="" data-item="${item.ind}">Stop Showing</a>
                `  
            }else{
                showong = `
                    <span class="text-danger">Is hidden from catalog</span>
                    <a class="link link-primary js-showing" tabindex="" data-item="${item.ind}">Start Showing</a>
                `  
            }

            schedule += `
                <div class="addEvent__schedule">
                    <div class="head">
                        <h4>
                            ${item.title}
                        </h4> 
                        ${badges}
                    </div>
                    <div class="statusPre">
                        <div class="status">
                            ${showong}
                        </div>
                        <div class="per">
                            ${price}
                            <strong>${item.currency}${item.priceCount}</strong>
                            <span>${item.priceType}</span>
                        </div>
                    </div>
                    <div class="props">
                        <div class="prop">
                            <div class="name">Date</div>
                            <div class="val">${item.daterangeForm} - ${item.daterangeTo}</div>
                        </div>
                        <div class="prop">
                            <div class="name">age</div>
                            <div class="val">${item.kidsAgeFrom}-${item.kidsAgeTo}</div>
                        </div>
                        <div class="prop">
                            <div class="name">SEATS AVAILABLE</div>
                            <div class="val">${item.seatsAvailable}</div>
                        </div>
                    </div>
                    <div class="bottBar">
                        <p>
                            ${item.shortDescription}
                        </p>
                        <div class="prop">
                            <div class="name">the price is valid from</div>
                            <div class="val">${item.datesaleFrom} - ${item.datesaleTo}</div>
                        </div>
                    </div>
                    <div class="btnsBlock">
                        <a class="addEvent__edit ext js-scheduleClone" tabindex="" data-item="${item.ind}">
                            <i class="icn-clone"></i>
                            Clone
                        </a>
                        <a class="addEvent__edit js-scheduleEdit" tabindex=""  data-item="${item.ind}">
                            <i class="icn-edit"></i>
                            Edit
                        </a>
                        <a class="addEvent__remove js-scheduleRemove" tabindex="" data-item="${item.ind}">
                            <i class="icn-close"></i>
                            Remove
                        </a>
                    </div>
                </div>
            `
        }
        $('.addEvent__schedules').html('')
        $('.addEvent__schedules').html(schedule)
    },

    
    
    scheduleEdit (e) {
        let $el  = $(e.currentTarget)
        let id = $el.attr('data-item')

        let found
        this.schedules.forEach(function(element) {
            if(element.ind == id){
                found = element
            }
        }); 




        this.$addScheduleForm.find('[name="title"]').val(found.title) 
        this.$addScheduleForm.find('[name="ind"]').val(found.ind) 
        this.$addScheduleForm.find('[name="shortDescription"]').val(found.shortDescription) 
        this.$addScheduleForm.find('[name="groupSize"]').val(found.groupSize).trigger('change') 
        this.$addScheduleForm.find('[name="seatsAvailable"]').val(found.seatsAvailable) 

        this.$addScheduleForm.find('[name="kidsAgeFrom"]').val(found.kidsAgeFrom) 
        this.$addScheduleForm.find('[name="kidsAgeTo"]').val(found.kidsAgeTo) 

        this.$range.slider('values', 0, found.kidsAgeFrom);
        this.$range.slider('values', 1, found.kidsAgeTo);


        this.$addScheduleForm.find('[name="daterangeForm"]').val(found.daterangeForm).trigger('apply.daterangepicker') 
        this.$addScheduleForm.find('[name="daterangeTo"]').val(found.daterangeTo).trigger('apply.daterangepicker') 


        this.$addScheduleForm.find('[name="timeCount"]').val(found.timeCount) 
        

        this.$addScheduleForm.find('[name="datesaleFrom"]').val(found.datesaleFrom).trigger('apply.daterangepicker') 
        this.$addScheduleForm.find('[name="datesaleTo"]').val(found.datesaleTo).trigger('apply.daterangepicker')

        this.$addScheduleForm.find('[name="price"]').val(found.price) 
        this.$addScheduleForm.find('.total .sum').text(found.priceCount) 

        this.$addScheduleForm.find('[name="currency"]').val(found.currency).trigger('change') 
        this.$addScheduleForm.find('[name="priceType"]').val(found.priceType).trigger('change') 

        this.$addScheduleForm.find('[name="discount"]').val(found.discount)

        if(found.discount != ""){
            this.$addScheduleForm.find('[name="discountType"]').removeAttr('disabled')
            this.$addScheduleForm.find(`[name="discountType"][value="${found.discountType}"]`).trigger('click') 
        } 
        if(found.enabled == 'true'){
            $el.find('[name="enabled"]').val(true).attr('checked',true) 
        }
         

        //this.runCalendar()

        let times = ""
        found.itemTime.forEach(function(element, i) {
            times += `
                <div class="item">
                    <input name="itemTime[]" readonly="" type="text" value="${element}">
                    <input name="itemDates[]" type="hidden" value="${found.itemDates[i]}">
                    <i class="icn-close"></i>
                </div>
            ` 
        });


        $('.addEvent .timesBlock').html(`
            ${times}
            <div class="buttonAddTime js-addTime">
                + Add Time
            </div>
        `)

        $('.addEvent .timesBlock').find('.item').first().trigger('click')

        this.$addScheduleForm.attr('data-status', 'edit');
        this.$addScheduleForm.find('.btns').val('Edit')
        this.$addScheduleModal.find('.modals__smallSpace').text('Edit Schedule and Price')

        



        //this.$addLinkForm.find('[name="title"]').val(found.title)
        //this.$addLinkForm.find('[name="url"]').val(found.url)
        //this.$addLinkForm.find('[name="ind"]').val(found.ind)

        this.$addScheduleModal.modal('show')

        this.renderSchedules()
    },
    scheduleRemove (e) {
        let $el  = $(e.currentTarget)
        let item = $el.attr('data-item')

        $('#removeScheduleModal').find('.js-scheduleDel').attr('data-item', item);
        $('#removeScheduleModal').modal('show');
    },
    scheduleDel (e) {
        let $el  = $(e.currentTarget)
        let item = $el.attr('data-item')
        
        
        let num = "";
        this.schedules.forEach(function(element, i) {
            if(element.ind == item){
               num = i 
            }
        });
        this.schedules.splice(num, 1) 
        this.renderSchedules()

        $('#removeScheduleModal').one('hidden.bs.modal', function(){
            notific.setNote('icn-success',
                'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'reviewInfo-success')
        });
        $('#removeScheduleModal').modal('hide');
    },
    scheduleClone (e){
        let $el  = $(e.currentTarget)
        let item = $el.attr('data-item')

        this.schedules.forEach(function(element, i) {
            if(element.ind == item){
               num = i 
            }
        });

        let oldObj  = this.schedules[num]
        let newObj = {}; 
        let key;

        for (key in oldObj) {
            newObj[key] = oldObj[key]; 
        }
        
        this.schedules.push(newObj)
        this.renderSchedules()
    },
    resetScheduleForm (e) {
        this.$addScheduleForm.find('.resetButton').trigger('click')
        this.$addScheduleForm.find('.btns').val('Add')
        this.$addScheduleForm.attr('data-status', 'new');
        this.$addScheduleModal.find('.modals__smallSpace').text('Add Schedule and Price')


        this.$addScheduleForm.find('[name="groupSize"]').val("").trigger('change')
        this.$addScheduleForm.find('[name="currency"]').val("").trigger('change')
        this.$addScheduleForm.find('[name="priceType"]').val("").trigger('change')

        const monthNames = ["January", "February", "March", "April", "May", "June",
          "July", "August", "September", "October", "November", "December"
        ];

        let date = new Date()
        let day = date.getDate()
        let month = date.getMonth()
        let year = date.getFullYear()
        let dateVal = `${day} ${monthNames[month]} ${year}`
        this.$addScheduleForm.find('[name="daterangeForm"]').val(dateVal).trigger('apply.daterangepicker') 
        this.$addScheduleForm.find('[name="daterangeTo"]').val(dateVal).trigger('apply.daterangepicker') 
        this.$addScheduleForm.find('[name="datesaleFrom"]').val(dateVal).trigger('apply.daterangepicker') 
        this.$addScheduleForm.find('[name="datesaleTo"]').val(dateVal).trigger('apply.daterangepicker') 
        
        this.$addScheduleForm.find('[name="discountType"]').attr('disabled', true);
        this.$addScheduleForm.find('[name="discountType"][value="percentage"]').prop('checked', true);
        this.$addScheduleForm.find('[name="discountType"][value="absolute"]').removeAttr('checked');
        this.$addScheduleForm.find('[name="enabled"]').val(false) 
        this.$addScheduleForm.find('[name="discount"]').val("");

        $('.addEvent .timesBlock').html(`
            <div class="item active">
                <input name="itemTime[]" readonly="" type="text" value="12:00 AM - 01:00 PM">
                <input name="itemDates[]" type="hidden" value="">
                <i class="icn-close"></i>
            </div>
            <div class="buttonAddTime js-addTime">
                + Add Time
            </div>
        `)
        this.$range.slider('values', 0, 0);
        this.$range.slider('values', 1, 18);

        
        setTimeout(() => {
            this.$addScheduleModal.find('.field').removeClass('error')
            this.$addScheduleModal.find('label.error').hide()
        }, 300)
    },
    detectScheduleConfirm () {
        if(this.scheduleSubmit == true){
            setTimeout(() => {
                this.resetScheduleForm()
                this.scheduleSubmit = false
            }, 1000)
        }else{
            if(!(this.$addScheduleModal.find('input[type="submit"]').is('[disabled]'))){
                this.$confirmScheduleModal.modal('show')
            }else{
                setTimeout(() => {
                    this.resetScheduleForm()
                    this.scheduleSubmit = false
                }, 1000)
            }
        }
    },
    doneSchedule () {
        this.$confirmScheduleModal.modal('hide')
        this.resetScheduleForm()
    },
    cancelSchedule () {
        this.$confirmScheduleModal.one('hidden.bs.modal', () => {
            this.$addScheduleModal.modal('show')
        });
        this.$confirmScheduleModal.modal('hide')
    },
    // Add links

    showing(e){ 
        let $el = $(e.currentTarget)
        let item = $el.attr('data-item')
        let num = "";
        
        this.schedules.forEach(function(element, i) {
            if(element.ind == item){
               num = i 
            }
        });
        if($el.hasClass('show')){
            this.schedules[num].enabled = false
            $el.removeClass('show')
            $el.text('Start Showing')
            $el.prev('span')
                .text('Is hidden from catalog')
                .removeClass('text-success')
                .addClass('text-danger')
        }else{
            this.schedules[num].enabled = true
            $el.addClass('show')
            $el.text('Stop Showing')
            $el.prev('span')
                .text('Is showing in the catalog')
                .removeClass('text-danger')
                .addClass('text-success')
        }


    },
    showingChange(e) {
        let $el = $(e.currentTarget)
        if($el.is(':checked')){
            $el.val(true)
        }else{
            $el.val(false)

        }
    },
    
    events () {
        
        this.$body.on('click', '#addSchedule .on', this.addDates.bind(this));
        this.$body.on('click', '#addSchedule .off', this.removeDates.bind(this));
        this.$body.on('click', '#addSchedule .pmu-instance .pmu-day-of-week .week', this.addWeek.bind(this));
        this.$body.on('apply.daterangepicker', '#addSchedule [name="daterangeForm"]', this.getPeriod.bind(this));
        this.$body.on('apply.daterangepicker', '#addSchedule [name="daterangeTo"]', this.getPeriod.bind(this));
        
        this.$body.on('change.bfhtimepicker', '#addSchedule .js-startTime', this.startTime.bind(this));
        this.$body.on('change.bfhtimepicker', '#addSchedule .js-endTime', this.endTime.bind(this));
        this.$body.on('click', '#addSchedule .timesBlock .item', this.setTime.bind(this));
        this.$body.on('click', '#addSchedule .timesBlock .item i', this.removeTime.bind(this));
        this.$body.on('click', '#addSchedule .js-addTime', this.addTime.bind(this)); 
        

        this.$body.on('input', '#addSchedule .js-price', this.countingPrice.bind(this)); 
        this.$body.on('input', '#addSchedule .js-discount', this.countingPrice.bind(this)); 
        this.$body.on('change', '#addSchedule [name="discountType"]', this.countingPrice.bind(this)); 
        this.$body.on('change', '#addSchedule .js-currency', this.setCurrency.bind(this)); 
        this.$body.on('change', '#addSchedule .js-priceType', this.setPriceType.bind(this)); 
        this.$body.on('apply.daterangepicker', '#addSchedule [name="datesaleFrom"]', function(){
            $("#addSchedule [name='datesaleTo']").valid()
        }); 

        this.$start.on('input change', this.changeStart.bind(this));
        this.$end.on('input change', this.changeEnd.bind(this));


        
        
        



        
        
        this.$body.on('click', '.addEvent__schedule .js-showing', this.showing.bind(this)); 
        this.$body.on('change', '#addSchedule  [name="enabled"]', this.showingChange.bind(this)); 
        this.$addScheduleForm.on('submit', this.addSchedule.bind(this));
        this.$body.on('click', '.js-scheduleEdit', this.scheduleEdit.bind(this));
        this.$addScheduleModal.on('hidden.bs.modal', this.detectScheduleConfirm.bind(this));
        this.$body.on('click', '.js-scheduleRemove', this.scheduleRemove.bind(this));
        this.$body.on('click', '.js-scheduleDel', this.scheduleDel.bind(this));
        this.$body.on('click', '.js-scheduleClone', this.scheduleClone.bind(this));
        this.$body.on('click', '.js-doneSchedule   ', this.doneSchedule.bind(this));
        this.$body.on('click', '.js-cancelSchedule', this.cancelSchedule.bind(this));
    },
    init () {
        this.cacheDom()
        //this.renderLink()
        this.runDateRange()
        this.runSlider()
        this.runCalendar()
        this.events()

    }
}

    moduleAddSchedule.init()
}

