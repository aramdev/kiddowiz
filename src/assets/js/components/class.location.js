class Location {
    constructor (input, data) {
        this.input = $(input);
        this.$parent = this.input.parent('.field-case');
        this.$win = $(window);
        this.data = [
            { 
                "name": "Ajo",
                "abbreviation": "AZ"
            },
            { 
                "name": "Avondale",
                "abbreviation": "AZ"
            },
            { 
                "name": "Bisbee",
                "abbreviation": "AZ"
            },
            { 
                "name": "Casa Grande",
                "abbreviation": "AZ"
            },
            { 
                "name": "Chandler",
                "abbreviation": "AZ"
            },
            { 
                "name": "Clifton",
                "abbreviation": "AZ"
            },
            { 
                "name": "Douglas",
                "abbreviation": "AZ"
            },
            { 
                "name": "Flagstaff",
                "abbreviation": "AZ"
            }
        ];
        this.curerntData = [];
        this.flag = false;
    }
    setData(e){
        e.stopPropagation()

        this.curerntData = this.data
        this.genData(this.curerntData)
    }
    filterDate(e){
        e.stopPropagation()

        let app = this;
        app.curerntData = []
        let states = app.data;
        let result = states.filter(function(states) {
            let regex = new RegExp('(' + app.input.val() + ')', 'i');
            return states.name.match(regex);
        })

        if(result.length != 0){
            this.genData(result)
        }else{
            this.curerntData.push({
                name: app.input.val(),
                abbreviation: ''
            })
            this.genData(this.curerntData)
        }
    }
    clearData(){
        this.$parent.find('.locationList').remove()
    }
    genData(list){
        this.clearData()
        let items = "";
        list.forEach(function(el, i) {
            items += `<li>${el.name} ${el.abbreviation}</li>`
        });
        
        let html = `<div class="locationList">
                        <span class="current">
                            <i class="icn-location"></i>
                            Current Location
                        </span>
                        <div class="listTitle">RECENTLY VIEWED</div>
                        <ul class="list">
                            ${items}
                        </ul>
                    </div>`
        this.$parent.append(html)
    }
    setValue(e){
        e.stopPropagation()
        let $el = $(e.currentTarget) 
        let text = $el.text()
        this.input.val(text)
        this.clearData()
        this.input.valid()
        this.input.trigger('change')

        
    }
    setCurrent(){
        this.input.val('Seattle WA')
        this.clearData()
        this.input.valid()
        this.input.trigger('change')
        
    }
    events(){
        this.input.on('focus', this.setData.bind(this));
        this.input.on('input', this.filterDate.bind(this));
        this.$parent.on('click', '.locationList .current', this.setCurrent.bind(this));
        this.$parent.on('click', '.locationList li', this.setValue.bind(this));
        this.$parent.on('click', function(e){e.stopPropagation()});
        this.$win.on('click', this.clearData.bind(this));
    }
    init(){
        this.events() 
    }
}


let location = new Location('[name="location"]')
location.init()
