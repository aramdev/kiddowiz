class Kids {
    constructor (kids) {
        this.kids = kids;
        this.increment = 0;
    }
    add(modal, form){
        this.increment++

        let ind = $(form).find('[name="firstname"]').val() + this.increment
        let old = $(form).find('[name="old"]').val()
        let firstname = $(form).find('[name="firstname"]').val();
        let lastname = $(form).find('[name="lastname"]').val();
        let birthday = $(form).find('[name="birthday"]').val();
        let interests = $(form).find('[name="interests"]').val();

        this.kids.push({
            checked: false,
            ind: ind,
            old: old,
            firstname: firstname,
            lastname: lastname,
            birthday: birthday,
            interests: interests,
        })
        
        $(modal).modal('hide')
    }
    edit(modal, form, id){
        let num = ""

        this.kids.forEach(function(element, i) {
            if(element.ind == id){
               num = i 
            }
        });
        
        this.kids[num].old = $(form).find('[name="old"]').val()
        this.kids[num].firstname = $(form).find('[name="firstname"]').val()
        this.kids[num].lastname = $(form).find('[name="lastname"]').val()
        this.kids[num].birthday = $(form).find('[name="birthday"]').val()
        this.kids[num].interests = $(form).find('[name="interests"]').val()

        $(modal).modal('hide')
    }
    set(modal, form, id){
        let found
        this.kids.forEach(function(element) {
            if(element.ind == id){
                found = element
            }
        }); 

        $(form).find('[name="id"]').val(found.ind)
        $(form).find('[name="old"]').val(found.old)
        $(form).find('[name="firstname"]').val(found.firstname)
        $(form).find('[name="lastname"]').val(found.lastname)
        $(form).find('[name="birthday"]').val(found.birthday)
        $(form).find('[name="interests"]').val(found.interests).trigger('change')
        $(modal).modal('show')
    }
    delete(modal, form, id){
        let num = "";
        this.kids.forEach(function(element, i) {
            if(element.ind == id){
               num = i 
            }
        });
        this.kids.splice(num, 1) 
        $(modal).modal('hide')
    }
    birthDay(input){
        $(input).each(function(index, el) {
            let id = $(this).attr('id');
            let calendar = new BirthDay(`#${id}`)
            calendar.setCalendar()
        });
    }
}