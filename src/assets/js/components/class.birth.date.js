class BirthDay {
    constructor (input) {
        this.input = $(input);
    }
    setCalendar(){
        let now = new Date();
        let mindate = new Date('2.11.2001')

        let $this = this.input
        let $parent = $this.parent(".field-case")
        let $form = $this.parents('form')
        this.input.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            parentEl: $parent,
            maxDate: moment(),
            minDate: mindate,
            locale: {
                format: 'DD MMM, YYYY',
                monthNames: [
                    "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
                ],
                daysOfWeek: [
                    'sun','mon','tue','wed', 'thu','fri','sat'
                ]
            }
        }, function(start, end, label) {
            var years = moment().diff(start, 'years');
            $form.find('[name="old"]').val(years)
        }).on('hide.daterangepicker',function(){
            setTimeout(function(){
                let val = $this.val()
                if(val != ""){
                    $this.removeClass('error')    
                    $this.next('label.error').hide()    
                }
            }, 10)            
        }).on('showCalendar.daterangepicker',function(){
            //$.datepicker._clearDate($this); 
            //$this.data('daterangepicker').setStartDate(now); 
            
        });

        $this.val("")
    }
}