var moduleOrder = {
    cacheDom() {
        this.$body = $('body')
        this.$modal = $('#changeOrderStatus')

        this.loadOrdersBtn  = $(".js-loadOrders")
       	this.loadProcessOrders = false;
       	this.iteratorOrders = 0
        
    },
	move (e) {
		let $el = $(e.currentTarget)
		let data = $el.attr('data-process') 
		this.$modal.find('.js-addText').text(data)
        this.$modal.modal('show')
    },
    load(e) {
		let $el = $(e.currentTarget)
		if(this.loadProcessOrders == false){
            this.loadProcessOrders = true
            $el.prepend('<span class="connecting"></span>')
            setTimeout(() => {
                $el.find('.connecting').remove()
                $('.orderItems').append(` 
                    <div class="orderItem">
							<div class="box">
								<p>
									<span class="num">#3876</span>
									May 5, 2018, 6:42PM
								</p>
								<p>
									Class "Rock Climbing for Kids"
								</p>
							</div>
							<div class="box">
								<p>Adam Toporek</p>
								<p>wants to begin on May 7. 2018</p>
								<p>
									<i class="icn-phone"></i>
									<a class="link link-default" href="">
										+1 676 787878
									</a>
								</p>
								<p>
									<i class="icn-email"></i>
									<a class="link link-primary" href="">
										a.toporek@gmail.com
									</a>
								</p>
							</div>
							<div class="box">
								<p class="total">
									Total:
									&nbsp;
									<strong>$500</strong>
								</p>
								<p>
									<a class="link link-primary process js-orderProcess" data-process="Are you sure to move order to Current status ?" tabindex="">
										Move the order to current
										<i class="icn-arr-up"></i>
									</a>
								</p>
								<a class="btns-sm btns btns-primary" data-target="#orderInfoModal" data-toggle="modal" tabindex="">Show full info</a>
							</div>
						</div>
						<div class="orderItem">
							<div class="box">
								<p>
									<span class="num">#3876</span>
									May 5, 2018, 6:42PM
								</p>
								<p>
									Class "Rock Climbing for Kids"
								</p>
							</div>
							<div class="box">
								<p>Adam Toporek</p>
								<p>wants to begin on May 7. 2018</p>
								<p>
									<i class="icn-phone"></i>
									<a class="link link-default" href="">
										+1 676 787878
									</a>
								</p>
								<p>
									<i class="icn-email"></i>
									<a class="link link-primary" href="">
										a.toporek@gmail.com
									</a>
								</p>
							</div>
							<div class="box">
								<p class="total">
									Total:
									&nbsp;
									<strong>$500</strong>
								</p>
								<p>
									<a class="link link-primary process js-orderProcess" data-process="Are you sure to move order to Current status ?" tabindex="">
										Move the order to current
										<i class="icn-arr-up"></i>
									</a>
								</p>
								<a class="btns-sm btns btns-primary" data-target="#orderInfoModal" data-toggle="modal" tabindex="">Show full info</a>
							</div>
						</div>
						<div class="orderItem">
							<div class="box">
								<p>
									<span class="num">#3876</span>
									May 5, 2018, 6:42PM
								</p>
								<p>
									Class "Rock Climbing for Kids"
								</p>
							</div>
							<div class="box">
								<p>Adam Toporek</p>
								<p>wants to begin on May 7. 2018</p>
								<p>
									<i class="icn-phone"></i>
									<a class="link link-default" href="">
										+1 676 787878
									</a>
								</p>
								<p>
									<i class="icn-email"></i>
									<a class="link link-primary" href="">
										a.toporek@gmail.com
									</a>
								</p>
							</div>
							<div class="box">
								<p class="total">
									Total:
									&nbsp;
									<strong>$500</strong>
								</p>
								<p>
									<a class="link link-primary process js-orderProcess" data-process="Are you sure to move order to Current status ?" tabindex="">
										Move the order to current
										<i class="icn-arr-up"></i>
									</a>
								</p>
								<a class="btns-sm btns btns-primary" data-target="#orderInfoModal" data-toggle="modal" tabindex="">Show full info</a>
							</div>
						</div>
						<div class="orderItem">
							<div class="box">
								<p>
									<span class="num">#3876</span>
									May 5, 2018, 6:42PM
								</p>
								<p>
									Class "Rock Climbing for Kids"
								</p>
							</div>
							<div class="box">
								<p>Adam Toporek</p>
								<p>wants to begin on May 7. 2018</p>
								<p>
									<i class="icn-phone"></i>
									<a class="link link-default" href="">
										+1 676 787878
									</a>
								</p>
								<p>
									<i class="icn-email"></i>
									<a class="link link-primary" href="">
										a.toporek@gmail.com
									</a>
								</p>
							</div>
							<div class="box">
								<p class="total">
									Total:
									&nbsp;
									<strong>$500</strong>
								</p>
								<p>
									<a class="link link-primary process js-orderProcess" data-process="Are you sure to move order to Current status ?" tabindex="">
										Move the order to current
										<i class="icn-arr-up"></i>
									</a>
								</p>
								<a class="btns-sm btns btns-primary" data-target="#orderInfoModal" data-toggle="modal" tabindex="">Show full info</a>
							</div>
						</div>
						<div class="orderItem">
							<div class="box">
								<p>
									<span class="num">#3876</span>
									May 5, 2018, 6:42PM
								</p>
								<p>
									Class "Rock Climbing for Kids"
								</p>
							</div>
							<div class="box">
								<p>Adam Toporek</p>
								<p>wants to begin on May 7. 2018</p>
								<p>
									<i class="icn-phone"></i>
									<a class="link link-default" href="">
										+1 676 787878
									</a>
								</p>
								<p>
									<i class="icn-email"></i>
									<a class="link link-primary" href="">
										a.toporek@gmail.com
									</a>
								</p>
							</div>
							<div class="box">
								<p class="total">
									Total:
									&nbsp;
									<strong>$500</strong>
								</p>
								<p>
									<a class="link link-primary process js-orderProcess" data-process="Are you sure to move order to Current status ?" tabindex="">
										Move the order to current
										<i class="icn-arr-up"></i>
									</a>
								</p>
								<a class="btns-sm btns btns-primary" data-target="#orderInfoModal" data-toggle="modal" tabindex="">Show full info</a>
							</div>
						</div>
                `)
                this.loadProcessOrders = false;
                this.iteratorOrders ++
                if(this.iteratorOrders > 2){
                	$el.remove()
                }
            }, 1000)
        }
	},
    events () {
        this.$body.on('click', '.js-orderProcess', this.move.bind(this))

        this.loadOrdersBtn.on('click', this.load.bind(this))
    },
    init () {
        this.cacheDom()
        this.events()
    }
}
if($('.js-orderProcess').length > 0){
    moduleOrder.init()
}