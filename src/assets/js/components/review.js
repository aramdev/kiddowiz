if($('#reviews').length > 0){
    let reviews = new Vue({
        el: '#reviews',
        data: {},
    })
}


var moduleReview = {
    cacheDom() {
        this.$review = $('.review')
        this.$answers = this.$review.find('.js-answers')
        this.$iterator = this.$review.find('.review__iterator .js-iterator')
        
        this.$win = $(window)
        
        this.$reply = this.$review.find('.js-reply')
        this.$writeReply = this.$review.find('.js-writeReply')
        
        
        this.$modal = $('#checkAbuse')

        this.$edit = this.$review.find('.js-edit')
        this.$remove = this.$review.find('.js-remove')

        this.$deleteReview = $('.js-deleteReview')
        this.$modalDel = $('#deleteReview')
        this.delItem = ""
        this.delUrl = ""
        
    },
	shwoAnswers (e) {
        let $el = $(e.currentTarget)
        let show = $(e.currentTarget).attr('data-show');
        let $prevEl = $el.prevAll('.review') 

        if($el.hasClass('open')){
            $prevEl.toArray().reverse();
            $prevEl.each(function(index, el) {
                if(index < show){
                    $(this).addClass('hidden')
                }
            });
            $el.text(`${show} replies`)
            $el.removeClass('open')
        }else{
            $prevEl.removeClass('hidden')
            $el.text(`Hide replies`)
            $el.addClass('open')
        }
    },
    topggleReply (e) {
        $('.replyBody').removeClass('open');
        let $el = $(e.currentTarget)
        $el.parent('span').next('.replyBody').toggleClass('open');
        e.stopPropagation()
    },
    hideReply () {
        $('.replyBody').removeClass('open');
    },
    edit (e) {
        let $el = $(e.currentTarget)
        let $inputBox = $el.parents(".review__reply").prev('.review__inputBoxEdit')
        let $content = $el.parents(".review__reply").prevAll('.review__text')
        
        $el.parents(".review__reply").nextAll('.review__inputBox').removeClass('open')
        
        $content.addClass('hidden')
        $inputBox.addClass('open');
    },
    add (e) {
        let $el = $(e.currentTarget)
        let $inputBox = $el.parents(".review__reply").next('.review__inputBox')
        $el.parents(".review__reply").prevAll('.review__text').removeClass('hidden')
        $el.parents(".review__reply").prevAll('.review__inputBoxEdit').removeClass('open')
        $inputBox.toggleClass('open'); 
    },
    remove (e) {
        let $el = $(e.currentTarget)
        this.$modalDel.modal('show')
        this.delItem = $el.parents(".review")[0];
    },
    deleteRev () {
        this.delItem.remove()
        this.$modalDel.modal('hide')
    },
    increment (e) {
        let $el = $(e.currentTarget)
        let $parent = $(e.currentTarget).parent(".review__iterator")
        let $span = $parent.find('span')

        let direction = $el.attr('data-iterator');
        let num = $span.text()

        num = parseInt(num)
        if(direction == "up"){
            num ++ 
        }else{
            num--
        }

        if(num > 0){
            $span.addClass('green').removeClass('red')
        }else if(num < 0){
            $span.addClass('red').removeClass('green')
        }else{
            $span.removeAttr('class')
        }

        $span.text(num)
        $parent.find('[data-iterator]').off();
    },
    abuseSend(e) {
        let tath = this
        let $el = $(e.currentTarget)
        $el.parents('.modal').modal("hide")
        notific.setNote('icn-success',
                        'Your complaint has been sent to the moderator',
                        'reviewInfo-success')
    },
    abuseSet(e) {
        let $el = $(e.currentTarget)
        $el.find('.js-abuse').one('click', this.abuseSend.bind(this));
    },
    events () {
        this.$answers.on('click', this.shwoAnswers.bind(this));
        this.$reply.on('click', this.topggleReply.bind(this));
        this.$win.on('click', this.hideReply.bind(this));
        this.$edit.on('click', this.edit.bind(this));
        this.$writeReply.on('click', this.add.bind(this));

        this.$remove.on('click', this.remove.bind(this));
        this.$deleteReview.on('click', this.deleteRev.bind(this));


        this.$iterator.on('click', this.increment.bind(this));
        
        this.$modal.on('shown.bs.modal', this.abuseSet.bind(this));

    },
    init () {
        this.cacheDom()
        this.events()
    }
}
if($('.review').length > 0){
    moduleReview.init()
}
