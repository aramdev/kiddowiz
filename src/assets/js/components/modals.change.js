var moduleChangeModals = {
    cacheDom() {
        this.$btn = $('.js-changeModals')
    },
    closeModal (e){
        let modal = $(e.currentTarget).attr('data-close')
        let showModal = $(e.currentTarget).attr('data-open')
        $(modal).one('hidden.bs.modal', function () {
          $(showModal).modal('show')
        })
        $(modal).modal('hide')
    },
    events () {
        this.$btn.on('click', this.closeModal.bind(this));
       
    },
    init () {
        this.cacheDom()
        this.events()
    }
}
if($('.js-changeModals').length > 0){
    moduleChangeModals.init()
}


var moduleResultModal = {
    cacheDom() {
        this.$modal = $('#resultModal')
    },
    resetModal (e) {
        this.$modal.find("i").removeAttr('class').addClass('noteModal__icon')
        this.$modal.find("p").html('')
    },    
    events () {
        this.$modal.on('hidden.bs.modal', this.resetModal.bind(this))
    },
    init () {
        this.cacheDom()
        this.events()
    }
}
if($('#resultModal').length > 0){
    moduleResultModal.init()
}