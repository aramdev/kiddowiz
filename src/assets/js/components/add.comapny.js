var moduleAddCompany = {
    cacheDom() {
        this.$body = $("body")
        this.$form = $('#addCompanyForm')


        this.$addLinkModal = $('#addSocialLinkModal')
        this.$confirmLinkModal = $('#confirmSocialLinkModal')
        this.$addLinkForm = $('#addSocialLink')
        this.linkSubmit = false;
        this.links = []

        this.$addDocModal = $('#addDocModal')
        this.$confirmDocModal = $('#confirmDocModal')
        this.$addDocForm = $('#addDoc')
        this.docSubmit = false;
        this.docs = []


        this.$addLocationModal = $('#addLocationModal')
        this.$confirmLocationModal = $("#confirmLocationModal")
        this.$removeLocationModal = $("#removeLocationModal")
        this.$addLocationForm = $('#addLocation')
             

        this.locationSubmit = false;
        this.locations = []



        this.$html = $('html, body');
        this.$head = $('.header');

        

        this.map = false;
        this.marker = false;
    },
    sendForm (e){
        let $el = $(e.currentTarget)
        let status = $el.attr('data-status');
        if(status == 'new'){
            $('.js.next').remove()
            $('[data-next]').show()
            $el.attr('data-status', "old")

            setTimeout(() => {
                let head = this.$head.outerHeight();
                let pos = $('#nextStep').offset().top - head - 30
                this.$html.animate({scrollTop:pos+'px'}, 700);
            }, 20)

            return false
        }
    },
    callTrigger(e){
        $(e.currentTarget).parents(".addCompany__img").find('input').trigger('click')
    },
    addImg(e){
        let $el = $(e.currentTarget)
        let val =  $el.val()
        val = val.split(".")
        let item = val.length - 1
        
        let $parent = $el.parents('.addCompany__img')
        
        if(val[item] == 'jpg' || val[item] == 'png' || val[item] == 'gif'){
            let url = URL.createObjectURL(e.target.files[0])
            if($parent.find('img').length <= 0){
                $parent.find('.img').prepend(`<img src="${url}"/>`)
            }else{
                $parent.find('img').attr('src', url);
            }
            //this.call.trigger('blur')
            $parent.find('.error').text('')
        }else{
           $parent.find('.error').text('You can add only .jpg, .png, .gif')
           $el.val("")
        }  
    },
    removeImg(e){
        let $el = $(e.currentTarget)
        let $parent = $el.parents('.addCompany__img')
        $parent.find('img').remove()
        $('.addCompany__img input').val("")
        $parent.find('.error').text('')
    },
    counterString (e) {
        let $el = $(e.currentTarget)
        let $cnt =  $('.addCompany__max')
        let max =  $cnt.attr('data-max');
        max = parseInt(max)
        let count = $el.val().length
        let sum = max - count

        if(sum >= 0){
            $cnt.addClass('text-success').removeClass('text-danger')
        }else{
            $cnt.addClass('text-danger').removeClass('text-success')
        }

        $cnt.find('span').text(sum) 
    },
    // Add links
    addLink (e){
        e.preventDefault()
        let $el = $(e.currentTarget)
        let status = $el.attr('data-status');
        
        if($el.valid()){
            if(status == 'new'){
                let item =  {}
                item.url = $el.find('[name="url"]').val() 
                item.title = $el.find('[name="title"]').val() 
                item.ind = this.links.length + item.title
                this.links.push(item) 


            }else{
                let id = $el.find('[name="ind"]').val()
                let num = ""

                this.links.forEach(function(element, i) {
                    if(element.ind == id){
                       num = i 
                    }
                });

                this.links[num].title = $el.find('[name="title"]').val()
                this.links[num].url = $el.find('[name="url"]').val()
            }
            notific.setNote('icn-success',
                'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'reviewInfo-success')
            this.renderLink()
            this.linkSubmit = true
            this.detectLinkConfirm()
            this.$addLinkModal.modal('hide')
        }
    },
    renderLink(){
        let link = ""
        for(let item of this.links){
            link += `
                <div class="addCompanySocLink">
                    <i class="icn-facebook"></i>
                    <div class="cnt">
                        <div class="title">${item.title}</div>
                        <a class="link link-primary" href="${item.url}">
                            ${item.url}
                        </a>
                    </div>
                    <div class="control">
                        <a class="addCompany__edit js-linkEdit"  data-item="${item.ind}" tabindex="">
                            <i class="icn-edit"></i>
                            Edit
                        </a>
                        <a class="addCompany__remove js-linkRemove"  data-item="${item.ind}" tabindex="">
                            <i class="icn-close"></i>
                            Remove
                        </a>
                    </div>
                </div>
            `
        }
        $('.addCompanySocLinks').html('')
        $('.addCompanySocLinks').html(link)
    },
    linkEdit (e) {
        let $el  = $(e.currentTarget)
        let id = $el.attr('data-item')

        let found
        this.links.forEach(function(element) {
            if(element.ind == id){
                found = element
            }
        }); 

        this.$addLinkForm.attr('data-status', 'edit');
        this.$addLinkForm.find('[name="title"]').val(found.title)
        this.$addLinkForm.find('[name="url"]').val(found.url)
        this.$addLinkForm.find('[name="ind"]').val(found.ind)
        this.$addLinkForm.find('.btns').val('Edit Link')
        this.$addLinkModal.find('.modals__title').text('Edit Link')

        

        this.$addLinkModal.modal('show')

        this.renderLink()
    },
    linkRemove (e) {
        let $el  = $(e.currentTarget)
        let item = $el.attr('data-item')
        
        $('#removeSocialLinkModal').find('.js-linkDel').attr('data-item', item);
        $('#removeSocialLinkModal').modal('show');
    },
    linkDel (e) {
        let $el  = $(e.currentTarget)
        let item = $el.attr('data-item')
        
        
        let num = "";
        this.links.forEach(function(element, i) {
            if(element.ind == item){
               num = i 
            }
        });
        this.links.splice(num, 1) 
        this.renderLink()

        $('#removeSocialLinkModal').one('hidden.bs.modal', function(){
            notific.setNote('icn-success',
                'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'reviewInfo-success')
        });
        $('#removeSocialLinkModal').modal('hide');
    },
    resetLinkForm (e) {
        this.$addLinkForm.find('.resetButton').trigger('click')
        this.$addLinkForm.find('.btns').val('Add Link').attr('disabled', 'disabled');
        this.$addLinkForm.attr('data-status', 'new');
        this.$addLinkModal.find('.modals__title').text('Add Link')
        setTimeout(() => {
            this.$addLinkForm.find('.field').removeClass('error')
            this.$addLinkForm.find('label.error').hide()
        }, 300)
    },
    detectLinkConfirm () {
        if(this.linkSubmit == true){
            setTimeout(() => {
                this.resetLinkForm()
                this.linkSubmit = false
            }, 1000)
        }else{
            if(!(this.$addLinkForm.find('input[type="submit"]').is('[disabled]'))){
                this.$confirmLinkModal.modal('show')
            }else{
                setTimeout(() => {
                    this.resetLinkForm()
                    this.linkSubmit = false
                }, 1000)
            }
        }
    },
    doneLink () {
        this.$confirmLinkModal.modal('hide')
        this.resetLinkForm()
    },
    cancelLink () {
        this.$confirmLinkModal.one('hidden.bs.modal', () => {
            this.$addLinkModal.modal('show')
        });
        this.$confirmLinkModal.modal('hide')
    },
    // Add links

    // Add docs
    addDoc (e){
        e.preventDefault()
        let $el = $(e.currentTarget)
        let status = $el.attr('data-status');
        

        if($el.valid()){
            if(status == 'new'){
                let item =  {}
                item.type = $el.find('[name="type"]').val() 
                item.title = $el.find('[name="title"]').val() 
                item.waiverFile = $el.find('[name="waiverFile"]').val() 
                item.filename = $el.find('.uploadFileBox .name').text()
                item.ind = this.docs.length + item.title
                this.docs.push(item) 

                

            }else{
                let id = $el.find('[name="ind"]').val()
                let num = ""

                this.docs.forEach(function(element, i) {
                    if(element.ind == id){
                       num = i 
                    }
                });

                this.docs[num].type = $el.find('[name="type"]').val()
                this.docs[num].title = $el.find('[name="title"]').val()
                this.docs[num].waiverFile = $el.find('[name="waiverFile"]').val()
                this.docs[num].filename = $el.find('.uploadFileBox .name').text()

            }
            this.$addDocModal.modal('hide')
            notific.setNote('icn-success',
                'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'reviewInfo-success')
            this.renderDoc()
            this.docSubmit = true
            this.detectLinkConfirm()
            
        }
    },
    renderDoc(){
        let doc = ""
        for(let item of this.docs){
            doc += `
                <div class="addCompanyDoc">
                    <div class="cnt">
                        <div class="title">Type: ${item.type}</div>
                        <a class="link link-primary" href="">
                            ${item.title}
                        </a>
                    </div>
                    <div class="control">
                        <a class="addCompany__edit js-docEdit" data-item="${item.ind}" tabindex="">
                            <i class="icn-edit"></i>
                            Edit
                        </a>
                        <a class="addCompany__remove js-docRemove" data-item="${item.ind}" tabindex="">
                            <i class="icn-close"></i>
                            Remove
                        </a>
                    </div>
                </div>
            `
        }
        $('.addCompanyDocs').html('')
        $('.addCompanyDocs').html(doc)
    },
    docEdit (e) {
        let $el  = $(e.currentTarget)
        let id = $el.attr('data-item')

        let found
        this.docs.forEach(function(element) {
            if(element.ind == id){
                found = element
            }
        }); 

        

        this.$addDocForm.attr('data-status', 'edit');
        this.$addDocForm.find('[name="type"]').val(found.type).trigger('change')
        this.$addDocForm.find('[name="title"]').val(found.title)
        this.$addDocForm.find('[name="ind"]').val(found.ind)
        this.$addDocForm.find('[name="waiverFile"]').removeAttr('required')
        this.$addDocForm.find('.uploadFileBox .name').text(found.filename)
        this.$addDocForm.find('.btns').val('Edit Document')
        this.$addDocModal.find('.modals__title').text('Edit Document')

        this.$addDocModal.modal('show')


        this.renderDoc()
    },
    docRemove (e) {
        let $el  = $(e.currentTarget)
        let item = $el.attr('data-item')
        
        $('#removeDocModal').find('.js-docDel').attr('data-item', item);
        $('#removeDocModal').modal('show');
    },
    docDel (e) {
        let $el  = $(e.currentTarget)
        let item = $el.attr('data-item')
        let num = "";
        this.docs.forEach(function(element, i) {
            if(element.ind == item){
               num = i 
            }
        });
        this.docs.splice(num, 1) 
        this.renderDoc()
        $('#removeDocModal').one('hidden.bs.modal', function(){
            notific.setNote('icn-success',
                'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'reviewInfo-success')
        });
        $('#removeDocModal').modal('hide');
    },
    resetDocForm (e) {
        this.$addDocForm.find('.resetButton').trigger('click')
        this.$addDocForm.find('.btns[type="submit"]').val('Add Document').attr('disabled', 'disabled');
        this.$addDocForm.attr('data-status', 'new');
        this.$addDocModal.find('.modals__title').text('Add Document')
        this.$addDocForm.find('[name="waiverFile"]').attr('required', true)
        this.$addDocModal.find('[name="type"]').trigger('change')
        this.$addDocModal.find('.uploadFileBox .name').text("File (PDF, JPG, PNG)")
        setTimeout(() => {
            this.$addDocForm.find('.field').removeClass('error')
            this.$addDocForm.find('label.error').hide()
        }, 300)
    },
    detectDocConfirm () {
        if(this.docSubmit == true){
            setTimeout(() => {
                this.resetDocForm()
                this.docSubmit = false
            }, 1000)
        }else{
            if(!(this.$addDocForm.find('input[type="submit"]').is('[disabled]'))){
                this.$confirmDocModal.modal('show')
            }else{
                setTimeout(() => {
                    this.resetDocForm()
                    this.docSubmit = false
                }, 1000)
            }
        }
    },
    doneDoc () {
        this.$confirmDocModal.modal('hide')
        this.resetDocForm()
    },
    cancelDoc () {
        this.$confirmDocModal.one('hidden.bs.modal', () => {
            this.$addDocModal.modal('show')
        });
        this.$confirmDocModal.modal('hide')
    },
    // Add docs

    // Add Location
    createNewMap () {
        this.$addLocationModal.one('shown.bs.modal', () => {
            this.setNewMap()
        });
        this.$addLocationModal.modal('show')
    },
    setNewMap () {
        let $this = this
        let map;
        let marker;

        function getLocation () {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(setCurren);
            } else { 
                alert("Geolocation is not supported by this browser.")
            }
        }



        function setCurren (position) {
            let long = position.coords.longitude
            let lat = position.coords.latitude
            let center
            if(long && lat){
                center = [long, lat]
            }else{
                center = [-100.017, 45.457]
            }

            

            map = new mapboxgl.Map({
                container: 'locationMap',
                style: 'mapbox://styles/mapbox/streets-v9',
                center: center,
                zoom: 15
            });
            map.addControl(new mapboxgl.NavigationControl(), "bottom-right");
            
            $this.$addLocationModal.find('[name="coordinates"]').val(center)


            marker = new mapboxgl.Marker({
                draggable: true,
                color: '#ff5e1c',
            })
            .setLngLat(center)
            .addTo(map);

            marker.on('dragend', function(){
                var lngLat = marker.getLngLat();
                $this.$addLocationModal.find('[name="coordinates"]').val([lngLat.lng, lngLat.lat])
                if($this.$addLocationForm.valid()){
                    $this.$addLocationForm.find('[type="submit"]').removeAttr('disabled')
                }
            });



            $this.map = map
            $this.marker = marker
        }


        getLocation()
    },
    detectLocation (e) {
        let $el = $(e.currentTarget)
        let location = this.$addLocationModal.find('[name="location"]').val() 
        
        let businessStAddress = this.$addLocationModal.find('[name="businessStAddress"]').val()
        console.log(businessStAddress)
        if(businessStAddress != ""){
            businessStAddress = businessStAddress + ", "
        }
        
        let code = this.$addLocationModal.find('[name="code"]').val() 
        if(code != ""){
            code = " " + code
        }
        
        let query = businessStAddress + location  + code


        console.log(query)

        

        let $this = this
        let map = this.map
        let marker = this.marker

        var mapboxClient = mapboxSdk({ accessToken: mapboxgl.accessToken });
        mapboxClient.geocoding.forwardGeocode({
            query: query,
            autocomplete: false,
            limit: 1
        })
            .send()
            .then(function (response) {
                if (response && response.body && response.body.features && response.body.features.length) {
                    var feature = response.body.features[0];

                    $this.$addLocationModal.find('[name="coordinates"]').val(feature.center)

                    $this.marker.remove()
                    marker = new mapboxgl.Marker({
                        draggable: true,
                        color: '#ff5e1c',
                    })
                    .setLngLat(feature.center)
                    .addTo(map);

                    marker.on('dragend', function(){
                        var lngLat = marker.getLngLat();
                        $this.$addLocationModal.find('[name="coordinates"]').val([lngLat.lng, lngLat.lat])
                        if($this.$addLocationForm.valid()){
                            $this.$addLocationForm.find('[type="submit"]').removeAttr('disabled')
                        }
                    });

                    map.flyTo({
                        center: feature.center,
                    });


                    $this.map = map
                    $this.marker = marker

                }else{
                    console.log('wrong')
                }
            });

    },

    changeInput (e){
        let $el = $(e.currentTarget)
        let name = $el.attr('name');
        let $form = $el.parents('form')

        if(name == "hasEmail"){

            if($el.is(':checked')){
                $form.find('[name="email"]').removeAttr('disabled')
                $form.find('[name="email"]').prev('.field-name').append('<span>*</span>')
                   
            }else{
                $form.find('[name="email"]').attr('disabled', 'true').removeClass('error').val('');
                $form.find('[name="email"]').parents('.field-case').find('label.error').remove()
                $form.find('[name="email"]').prev('.field-name').find('span').remove()


            }

            $form.trigger('change')
            $el.val($el.prop('checked'))

            return false
        }

        if(name == "hasPhone"){

            if($el.is(':checked')){
                $form.find('[name="phone"]').removeAttr('disabled')
                $form.find('[name="phone"]').prev('.field-name').append('<span>*</span>')
                
            }else{
                $form.find('[name="phone"]').attr('disabled', 'true').removeClass('error').val('');
                $form.find('[name="phone"]').parents('.field-case').find('label.error').remove();
                $form.find('[name="phone"]').prev('.field-name').find('span').remove()

                
            }

            $form.trigger('change')
            $el.val($el.prop('checked'))

            return false
        }

        if(name == "hasWebsite"){

            if($el.is(':checked')){
                $form.find('[name="website"]').removeAttr('disabled')
                $form.find('[name="website"]').prev('.field-name').append('<span>*</span>')
            }else{
                $form.find('[name="website"]').attr('disabled', 'true').removeClass('error').val('');
                $form.find('[name="website"]').parents('.field-case').find('label.error').remove();
                $form.find('[name="website"]').prev('.field-name').find('span').remove()

            }
            
            $form.trigger('change')
            $el.val($el.prop('checked'))

            return false
        }
    },

    setMap (coord) {
        let $this = this
        let coordinates = coord.split(",")
        let map;
        let marker;
        
        
        map = new mapboxgl.Map({
            container: 'locationMap',
            style: 'mapbox://styles/mapbox/streets-v9',
            center: coordinates,
            zoom: 15
        });
        map.addControl(new mapboxgl.NavigationControl(), "bottom-right");
        
        $this.$addLocationModal.find('[name="coordinates"]').val(coordinates)


        marker = new mapboxgl.Marker({
            draggable: true,
            color: '#ff5e1c',
        })
        .setLngLat(coordinates)
        .addTo(map);

        marker.on('dragend', function(){
            var lngLat = marker.getLngLat();
            $this.$addLocationModal.find('[name="coordinates"]').val([lngLat.lng, lngLat.lat])
            if($this.$addLocationForm.valid()){
                $this.$addLocationForm.find('[type="submit"]').removeAttr('disabled')
            }
        });



        $this.map = map
        $this.marker = marker
    },


    addLocation (e){
        e.preventDefault()
        let $el = $(e.currentTarget)
        let status = $el.attr('data-status');
        
        if($el.valid()){
            if(status == 'new'){
                let item =  {}
                

                item.coordinates = $el.find('[name="coordinates"]').val()
                item.title = $el.find('[name="title"]').val()
                item.location = $el.find('[name="location"]').val()
                item.code = $el.find('[name="code"]').val()
                item.businessStAddress = $el.find('[name="businessStAddress"]').val()


                

                
                
                item.hasEmail = $el.find('[name="hasEmail"]').val()

                
                if($el.find('[name="email"]').val() != ""){
                    item.email = $el.find('[name="email"]').val()    
                }else{
                    item.email = "admin@admin.ru"
                }

                
                
                item.hasPhone = $el.find('[name="hasPhone"]').val()
                if($el.find('[name="phone"]').val() != ""){
                    item.phone = $el.find('[name="phone"]').val()
                }else{
                    item.phone = "+1 (564) 654-6546"
                }

                item.hasWebsite = $el.find('[name="hasWebsite"]').val()
                if($el.find('[name="website"]').val() != ""){
                    item.website = $el.find('[name="website"]').val()
                }else{
                    item.website = 'https://www.google.ru/'
                }
                

                item.ind = this.locations.length + 'location'

                this.locations.push(item) 


            }else{
                let id = $el.find('[name="ind"]').val()
                let num = ""

                this.locations.forEach(function(element, i) {
                    if(element.ind == id){
                       num = i 
                    }
                });

                
                this.locations[num].coordinates = $el.find('[name="coordinates"]').val()
                this.locations[num].title = $el.find('[name="title"]').val()
                this.locations[num].location = $el.find('[name="location"]').val()
                this.locations[num].code = $el.find('[name="code"]').val()
                this.locations[num].businessStAddress = $el.find('[name="businessStAddress"]').val()




                
                
                this.locations[num].hasEmail = $el.find('[name="hasEmail"]').val()

                
                if($el.find('[name="email"]').val() != ""){
                    this.locations[num].email = $el.find('[name="email"]').val()    
                }else{
                    this.locations[num].email = "admin@admin.ru"
                }

                
                
                this.locations[num].hasPhone = $el.find('[name="hasPhone"]').val()
                if($el.find('[name="phone"]').val() != ""){
                    this.locations[num].phone = $el.find('[name="phone"]').val()
                }else{
                    this.locations[num].phone = "+1 (564) 654-6546"
                }

                this.locations[num].hasWebsite = $el.find('[name="hasWebsite"]').val()
                if($el.find('[name="website"]').val() != ""){
                    this.locations[num].website = $el.find('[name="website"]').val()
                }else{
                    this.locations[num].website = 'https://www.google.ru/'
                }
                

            }
            notific.setNote('icn-success',
                'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'reviewInfo-success')
            this.renderLocation()
            this.locationSubmit = true
            this.detectLocationConfirm()
            this.$addLocationModal.modal('hide')
        }
    },

    locationEdit (e) {
        let $el  = $(e.currentTarget)
        let id = $el.attr('data-item')

        let found
        this.locations.forEach(function(element) {
            if(element.ind == id){
                found = element
            }
        }); 

        
        

        this.$addLocationForm.attr('data-status', 'edit');
        this.$addLocationModal.find('.modals__title').text('Edit Location')
        this.$addLocationForm.find('.btns').val('Edit Location')

        

        this.$addLocationForm.find('[name="coordinates"]').val(found.coordinates)
        this.$addLocationForm.find('[name="location"]').val(found.location)
        this.$addLocationForm.find('[name="title"]').val(found.title)
        this.$addLocationForm.find('[name="ind"]').val(found.ind)
        this.$addLocationForm.find('[name="code"]').val(found.code)
        this.$addLocationForm.find('[name="businessStAddress"]').val(found.businessStAddress)

        this.$addLocationForm.find('[name="hasEmail"]').val(found.hasEmail)
        
        if(found.hasEmail == "true"){
            this.$addLocationForm.find('[name="hasEmail"]').prop('checked', true);
            this.$addLocationForm.find('[name="email"]').val(found.email).removeAttr('disabled')
            this.$addLocationForm.find('[name="email"]').prev('.field-name').append('<span>*</span>')
        }

        this.$addLocationForm.find('[name="hasPhone"]').val(found.hasPhone)
        
        if(found.hasPhone == "true"){
            this.$addLocationForm.find('[name="hasPhone"]').prop('checked', true);
            this.$addLocationForm.find('[name="phone"]').val(found.phone).removeAttr('disabled')
            this.$addLocationForm.find('[name="phone"]').prev('.field-name').append('<span>*</span>')
        }

        this.$addLocationForm.find('[name="hasWebsite"]').val(found.hasWebsite)
        
        if(found.hasWebsite == "true"){
            this.$addLocationForm.find('[name="hasWebsite"]').prop('checked', true);
            this.$addLocationForm.find('[name="website"]').val(found.website).removeAttr('disabled')
            this.$addLocationForm.find('[name="website"]').prev('.field-name').append('<span>*</span>')
        }


        
        this.$addLocationModal.one('shown.bs.modal', () => {
            this.setMap(found.coordinates)

        });

        //console.log('aaaaaaaaaaaa')
        
        this.$addLocationModal.modal('show')

    },

    renderLocation(){
        let doc = ""
        for(let item of this.locations){
            doc += `
                <div class="addCompanyLocation">
                    <div class="state">${item.title}</div>
                    <div class="address">${item.location}</div>
                    <div class="info">
                        Phone:
                        <a class="link link-default" href="">${item.phone}</a>
                        Email:
                        <a class="link link-default" href="">${item.email}</a>
                    </div>
                    <label class="check-case addCompany__check">
                        <input name="" type="radio">
                        <span class="field-checkbox">
                            <i class="icn-checkbox"></i>
                        </span>
                        <div class="check-name">
                            it is base company location
                            <i class="icn-info-solid" data-container="element" data-placement="left" data-toggle="tooltip" title="hint for you"></i>
                        </div>
                    </label>
                    <div class="control">
                        <a class="addCompany__edit js-locationEdit" data-item="${item.ind}" tabindex="">
                            <i class="icn-edit"></i>
                            Edit
                        </a>
                        <a class="addCompany__remove js-locationRemove" data-item="${item.ind}" tabindex="">
                            <i class="icn-close"></i>
                            Remove
                        </a>
                    </div>
                </div>
            `
        }
        $('.addCompanyLocations').html('')
        $('.addCompanyLocations').html(doc)
    },

    resetLocationForm (e) {
        this.$addLocationForm.find('.resetButton').trigger('click')
        this.$addLocationForm.find('.btns[type="submit"]').val('Add Location').attr('disabled', 'disabled');
        this.$addLocationForm.attr('data-status', 'new');
        this.$addLocationModal.find('.modals__title').text('Add New Locatin')

        this.$addLocationForm.find('#geocoder').html("")
        this.$addLocationForm.find('#locationMap').html("").removeClass("mapboxgl-map")

        this.$addLocationForm.find('[name="email"]').attr('disabled', true);
        this.$addLocationForm.find('[name="phone"]').attr('disabled', true);
        this.$addLocationForm.find('[name="website"]').attr('disabled', true);

        this.$addLocationForm.find('[name="email"]').prev('.field-name').find('span').remove()
        this.$addLocationForm.find('[name="phone"]').prev('.field-name').find('span').remove()
        this.$addLocationForm.find('[name="website"]').prev('.field-name').find('span').remove()

        setTimeout(() => {
            this.$addLocationForm.find('.field').removeClass('error')
            this.$addLocationForm.find('label.error').hide()

            this.map = false;
            this.marker = false;
        }, 300)
    },

    detectLocationConfirm () {
        if(this.locationSubmit == true){
            setTimeout(() => {
                this.resetLocationForm()
                this.locationSubmit = false
            }, 1000)
        }else{
            if(!(this.$addLocationForm.find('input[type="submit"]').is('[disabled]'))){
                this.$confirmLocationModal.modal('show')
            }else{
                setTimeout(() => {
                    this.resetLocationForm()
                    this.locationSubmit = false
                }, 1000)
            }
        }
    },

    doneLocation () {
        this.$confirmLocationModal.modal('hide')
        this.resetLocationForm()
    },
    cancelLocation () {
        this.$confirmLocationModal.one('hidden.bs.modal', () => {
            this.$addLocationModal.modal('show')
        });
        this.$confirmLocationModal.modal('hide')
    }, 


    locationRemove (e) {
        let $el  = $(e.currentTarget)
        let item = $el.attr('data-item')
        
        $('#removeLocationModal').find('.js-locationDel').attr('data-item', item);
        $('#removeLocationModal').modal('show');
    },
    locationDel (e) {
        let $el  = $(e.currentTarget)
        let item = $el.attr('data-item')
        let num = "";
        this.locations.forEach(function(element, i) {
            if(element.ind == item){
               num = i 
            }
        });
        this.locations.splice(num, 1) 
        this.renderLocation()
        $('#removeLocationModal').one('hidden.bs.modal', function(){
            notific.setNote('icn-success',
                'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'reviewInfo-success')
        });
        $('#removeLocationModal').modal('hide');
    },


    // Add Location

    waiversCtrl (e) {
        let $el  = $(e.currentTarget)
        if($el.val() == 'text'){
            $(".addText").show()
            $(".addDoc").hide()
        }else{
            $(".addText").hide()
            $(".addDoc").show()
        }
    },
    events () {
        this.$form.on('submit', this.sendForm.bind(this));
        
        this.$addLinkForm.on('submit', this.addLink.bind(this));
        this.$body.on('click', '.js-linkEdit', this.linkEdit.bind(this));
        this.$body.on('click', '.js-linkRemove', this.linkRemove.bind(this));
        this.$body.on('click', '.js-linkDel', this.linkDel.bind(this));
        this.$addLinkModal.on('hidden.bs.modal', this.detectLinkConfirm.bind(this));
        this.$body.on('click', '.js-doneLink   ', this.doneLink.bind(this));
        this.$body.on('click', '.js-cancelLink', this.cancelLink.bind(this));
        
        

        this.$addDocForm.on('submit', this.addDoc.bind(this));
        this.$body.on('click', '.js-docEdit', this.docEdit.bind(this));
        this.$body.on('click', '.js-docRemove', this.docRemove.bind(this));
        this.$body.on('click', '.js-docDel', this.docDel.bind(this));
        this.$addDocModal.on('hidden.bs.modal', this.detectDocConfirm.bind(this));
        this.$body.on('click', '.js-doneDoc', this.doneDoc.bind(this));
        this.$body.on('click', '.js-cancelDoc', this.cancelDoc.bind(this));

        this.$addLocationForm.on('submit', this.addLocation.bind(this));
        this.$body.on('click', '.js-locationEdit', this.locationEdit.bind(this));
        this.$body.on('click', '.js-locationRemove', this.locationRemove.bind(this));
        this.$body.on('click', '.js-locationDel', this.locationDel.bind(this));
        this.$addLocationModal.on('hidden.bs.modal', this.detectLocationConfirm.bind(this));
        this.$body.on('click', '.js-doneLocation', this.doneLocation.bind(this));
        this.$body.on('click', '.js-cancelLocation', this.cancelLocation.bind(this));
        this.$body.on('change', '.js-location', this.detectLocation.bind(this));

        
        this.$body.on('click', '.js-addNewLocation', this.createNewMap.bind(this));
        this.$body.on('change', '#addLocationModal [name="hasEmail"]', this.changeInput.bind(this));
        this.$body.on('change', '#addLocationModal [name="hasPhone"]', this.changeInput.bind(this));
        this.$body.on('change', '#addLocationModal [name="hasWebsite"]', this.changeInput.bind(this));




        this.$body.on("click", ".addCompany__img .btns", this.callTrigger.bind(this))
        this.$body.on("change", ".addCompany__img input", this.addImg.bind(this))
        this.$body.on("click", ".addCompany__img .link-default", this.removeImg.bind(this))
        this.$body.on("input change", ".js-counterStr", this.counterString.bind(this))
        this.$body.on("change", ".addCompany__waivers [name='waivers']", this.waiversCtrl.bind(this))
    },
    init () {
        this.cacheDom()
        this.renderLink()
        this.renderLocation()
        this.renderDoc()
        this.events()
    }
}
if($('.addCompany').length > 0){
    moduleAddCompany.init()
}


let uploadFileBox  = {
    cacheDom() {
        this.$body = $('body')        
    },
    changeInput (e) {
        let $el  = $(e.currentTarget)
        let $parents = $el.parents('.uploadFileBox')
        $parents.find('input').trigger('click')
    },
    change(e) {
        let $el  = $(e.currentTarget)
        let $parents = $el.parents('.uploadFileBox')

        let val =  $el.val()
        val = val.split(".")
        let item = val.length - 1
        
        if(val[item] == 'pdf' || val[item] == 'png' || val[item] == 'jpg'){
            $parents.find('.name').text($el[0].files[0].name)
        }else{
           $parents.find('.name').text('File (PDF, JPG, PNG)')
        }

        $el.valid()
    },
    events () {
        this.$body.on('click', '.uploadFileBox a.btns', this.changeInput.bind(this));
        this.$body.on('change', '.uploadFileBox input[type="file"]', this.change.bind(this));
    },
    init () {
        this.cacheDom()
        this.events()
    }
}
if($('.uploadFileBox').length > 0){
    uploadFileBox.init()
}

