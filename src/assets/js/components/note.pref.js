var moduleNotePref = {
    cacheDom() {
        this.$checkBtn = $(".js-checkNote");
        this.$unCheckBtn = $(".js-unCheckNote");
    },
	check(){
        $('.notePref__form input[type="checkbox"]').prop({
            checked: true
        })
    },
    unCheck(){
        $('.notePref__form input[type="checkbox"]').prop({
            checked: false
        })
    },
    events () {
        this.$checkBtn.on('click', this.check.bind(this));
        this.$unCheckBtn.on('click', this.unCheck.bind(this));
    },
    init () {
        this.cacheDom()
        this.events()
    }
}
if($('.js-checkNote').length > 0){
    moduleNotePref.init()
}