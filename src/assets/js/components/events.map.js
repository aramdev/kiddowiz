if($("#eventsListMap").length > 0){
var geojson = {
    "type": "FeatureCollection",
    "features": [
        {
            "type": "Feature",
            "properties": {
                "id": "ak16994521",
                "message": {
                    "title": 'Rock Climbing for Kids and Teenagers 3',
                    "price": '$160-$320',
                    "age": 'age: 8-14',
                },
                //"iconSize": [60, 60]
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -100.017, 45.457
                ]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "id": "ak16994527",
                "message": {
                    "title": 'Rock Climbing for Kids and Teenagers 1',
                    "price": '$160-$320',
                    "age": 'age: 8-15',
                },
                //"iconSize": [50, 50]
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                   -90.017, 32.457
                ]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "id": "ak16994532",
                "message": {
                    "title": 'Rock Climbing for Kids and Teenagers 6',
                    "price": '$160-$320',
                    "age": 'age: 8-15',
                },
                //"iconSize": [50, 50]
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                   -91, 33
                ]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "id": "ak16994525",
                "message": {
                    "title": 'Rock Climbing for Kids and Teenagers 2',
                    "price": '$160-$320',
                    "age": 'age: 8-16',
                },
                //"iconSize": [40, 40]
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -111.017, 39.457
                ]
            }
        }
    ]
};


var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v9',
    center: [-100.017, 39.457],
    zoom: 3.4
    
});

map.addControl(new mapboxgl.NavigationControl());
if ("ontouchstart" in document.documentElement){
    map.dragPan.disable();
}else{
    map.dragPan.enable();
}

map.on('load', function() {
    // Add a new source from our GeoJSON data and set the
    // 'cluster' option to true. GL-JS will add the point_count property to your source data.
    map.addSource("earthquakes", {
        type: "geojson",
        // Point to GeoJSON data. This example visualizes all M1.0+ earthquakes
        // from 12/22/15 to 1/21/16 as logged by USGS' Earthquake hazards program.
        data: geojson,
        cluster: true,
        clusterMaxZoom: 14, // Max zoom to cluster points on
        clusterRadius: 50 // Radius of each cluster when clustering points (defaults to 50)
    });

    map.addLayer({
        id: "clusters",
        type: "circle",
        source: "earthquakes",
        filter: ["has", "point_count"],
        paint: {
            // Use step expressions (https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-step)
            // with three steps to implement three types of circles:
            //   * Blue, 20px circles when point count is less than 100
            //   * Yellow, 30px circles when point count is between 100 and 750
            //   * Pink, 40px circles when point count is greater than or equal to 750
            "circle-color": [
                "step",
                ["get", "point_count"],
                "#ff5e1c",
                100,
                "#ff5e1c",
                750,
                "#ff5e1c"
            ],
            "circle-radius": [
                "step",
                ["get", "point_count"],
                20,
                100,
                30,
                750,
                40
            ]
        }
    });

    map.addLayer({
        id: "cluster-count",
        type: "symbol",
        source: "earthquakes",
        filter: ["has", "point_count"],
        layout: {
            "text-field": "{point_count_abbreviated}",
            "text-font": ["DIN Offc Pro Medium", "Arial Unicode MS Bold"],
            "text-size": 12,

        }
    });

    map.addLayer({
        id: "unclustered-point",
        type: "circle",
        source: "earthquakes",
        filter: ["!", ["has", "point_count"]],
        paint: {
            "circle-color": "#ff5e1c", // 5642ab
            "circle-radius": 5,
            "circle-stroke-width": 0,
            "circle-stroke-color": "#ff5e1c"
        }
    });

    // inspect a cluster on click
    map.on('click', 'clusters', function (e) {
        var features = map.queryRenderedFeatures(e.point, { layers: ['clusters'] });
        var clusterId = features[0].properties.cluster_id;
        map.getSource('earthquakes').getClusterExpansionZoom(clusterId, function (err, zoom) {
            if (err)
                return;

            map.easeTo({
                center: features[0].geometry.coordinates,
                zoom: zoom
            });
        });
    });


   

    let pointId = "";
    map.on('click', 'unclustered-point', function (e) {

        let features = map.queryRenderedFeatures(e.point, { layers: ['unclustered-point'] });
        let data = JSON.parse(features[0].properties.message)
        
        let $elem = $(".eventsList__map .moreInfo")
        let $container = $elem.find(".moreInfo__content")
        if(pointId != features[0].properties.id){
            $container.html("")
            $container.html(`
                <div class="eventBox">
                    <div class="eventBox__head">
                        <a class="eventBox__img" href="">
                            <!-- .placeholder -->
                            <!-- .placeholder__inner -->
                            <!-- %i.icn-photo -->
                            <!-- %span No Photo -->
                            <img alt="" src="http://lorempixel.com/250/200" srcset="http://lorempixel.com/250/200 1x, http://lorempixel.com/500/400 2x">
                        </a>
                        <a class="addWish eventBox__icon" data-target="#pleaseSingInModal" data-toggle="modal">
                            <i class="icn-favorite-o"></i>
                        </a>
                        <!-- // if not guest -->
                        <!-- %a.addWish.animated.js-addWish.eventBox__icon -->
                        <!-- %i.icn-favorite-o -->
                        <!-- // if not guest -->
                        <div class="eventBox__content">
                            <div class="eventBox__info">
                                <span class="dateAge">
                                    TODAY
                                    <i class="icn-radio"></i>
                                    AGE: 8-14
                                </span>
                                <!-- /%span.price $160-$320 -->
                            </div>
                            <div class="eventBox__title">
                                <a href="">
                                    ${data.title}
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="eventBox__info">
                        <div class="eventBox__label popoverPlace js-popovers" data-container="body" data-content="" data-html="true" data-toggle="popover">
                            Adventure Out
                            <div class="popovers">
                                <i class="icn-close popovers__close" data-place="left"></i>
                                <div class="popovers__header">
                                    <div class="popovers__head">
                                        <h2>Advanture Out</h2>
                                        <div class="popovers__addrres">
                                            Seattle, Washington
                                            <br>
                                            900 Poplar Pl S, Seattke, WA 98144
                                        </div>
                                    </div>
                                    <a href="">
                                        <img alt="" src="assets/img/pop-logo.png">
                                    </a>
                                </div>
                                <p class="popovers__text">
                                    Since our founding in 2004, the mission of Adventure Out has been simple: to help people fall back in love woth nature. We accomplish this by providing programs of the highest quality and safety standards that get people outdoors, physically active, and adventuring in nature.
                                </p>
                                <a class="link-primary" href="">Learn More</a>
                            </div>
                        </div>
                        <div class="rate">
                            <i class="icn-star active"></i>
                            <i class="icn-star active"></i>
                            <i class="icn-star active"></i>
                            <i class="icn-star"></i>
                            <i class="icn-star"></i>
                        </div>
                        <a class="commentsCount eventBox__comments" href="">
                            <i class="icn-comments"></i>
                            3
                        </a>
                        <div class="eventBox__address">
                            900 Poplar Pl S, Seattke, WA 98144
                        </div>
                    </div>
                    <div class="eventBox__text">
                        This camp is designed for those kids ages 8-14 who want to experience the thrill of outdoor rock climbing. Each half-day (4-hour) class will be held at Castle Rock State Park in the Los Gatos/Santa Cruz Hills. No previous experience is necessary and advanced coaching also available for kids that have climbed before
                    </div>
                    <div class="schedulesPricesMap eventBox__box">
                        <div class="schedulesPricesMap__label">-40%</div>
                        
                        <div class="schedulesPricesMap__boxText">
                            <div class="schedulesPricesMap__title">Camp “Young Climber”</div>
                            <div class="schedulesPricesMap__priceBox">
                                <span class="schedulesPricesMap__discount">$200</span>
                                <span class="schedulesPricesMap__price">$160</span>
                                for a seat
                            </div>
                        </div>
                        <div class="field-case">
                            <div class="field popoverPlace js-popovers" data-container="body" data-content="" data-html="true" data-toggle="popover">
                                Book
                                <div class="popovers popovers-sm">
                                    <i class="icn-close popovers__close" data-place="left"></i>
                                    <h3 class="popovers__title">Class “Rock Climbing for Kids”</h3>
                                    <p class="popovers__text">
                                        Kids 7-10 years old, all day since 9AM to 6PM skill level is not required (you have never engaged in rock climbing before)
                                    </p>
                                    <table class="popovers__table">
                                        <tr>
                                            <td>
                                                <strong>Seat available</strong>
                                            </td>
                                            <td>4</td>
                                        </tr>
                                        <tr>
                                            <td>Price</td>
                                            <td>
                                                <strong>$200 per month</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Date</strong>
                                            </td>
                                            <td>1 May, 2019</td>
                                        </tr>
                                    </table>
                                    <form action="">
                                        <div class="row popovers__priceBox">
                                            <div class="col-6">
                                                <div class="field-case">
                                                    <label class="field-name">Number</label>
                                                    <input class="field js-price" data-price="200" min="1" name="" type="number" value="1">
                                                    <div class="increment">
                                                        <div class="up"></div>
                                                        <div class="down"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="popovers__price">
                                                    <strong class="js-priceOutput">$200</strong>
                                                    per month
                                                </div>
                                            </div>
                                        </div>
                                        <input class="btns btns-primary btns-block" type="submit" value="Checkout">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="schedulesPricesMap eventBox__box">
                        <div class="schedulesPricesMap__boxText">
                            <div class="schedulesPricesMap__title">Camp “Young Climber”</div>
                            <div class="schedulesPricesMap__priceBox">
                                <span class="schedulesPricesMap__price">$250</span>
                            </div>
                        </div>
                        <div class="field-case">
                            <div class="field popoverPlace js-popovers" data-container="body" data-content="" data-html="true" data-toggle="popover">
                                Book
                                <div class="popovers popovers-sm">
                                    <i class="icn-close popovers__close" data-place="left"></i>
                                    <h3 class="popovers__title">Class “Rock Climbing for Kids”</h3>
                                    <p class="popovers__text">
                                        Kids 7-10 years old, all day since 9AM to 6PM skill level is not required (you have never engaged in rock climbing before)
                                    </p>
                                    <table class="popovers__table">
                                        <tr>
                                            <td>
                                                <strong>Seat available</strong>
                                            </td>
                                            <td>4</td>
                                        </tr>
                                        <tr>
                                            <td>Price</td>
                                            <td>
                                                <strong>$200 per month</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Date</strong>
                                            </td>
                                            <td>1 May, 2019</td>
                                        </tr>
                                    </table>
                                    <form action="">
                                        <div class="row popovers__priceBox">
                                            <div class="col-6">
                                                <div class="field-case">
                                                    <label class="field-name">Number</label>
                                                    <input class="field js-price" data-price="200" min="1" name="" type="number" value="1">
                                                    <div class="increment">
                                                        <div class="up"></div>
                                                        <div class="down"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="popovers__price">
                                                    <strong class="js-priceOutput">$200</strong>
                                                    per month
                                                </div>
                                            </div>
                                        </div>
                                        <input class="btns btns-primary btns-block" type="submit" value="Checkout">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="schedulesPricesMap eventBox__box">
                        <div class="schedulesPricesMap__boxText">
                            <div class="schedulesPricesMap__title">Camp “Young Climber”</div>
                            <div class="schedulesPricesMap__priceBox">
                                <span class="schedulesPricesMap__discount">$200</span>
                                <span class="schedulesPricesMap__price">$160</span>
                                for a seat
                            </div>
                        </div>
                        <div class="field-case">
                            <div class="field popoverPlace js-popovers" data-container="body" data-content="" data-html="true" data-toggle="popover">
                                Book
                                <div class="popovers popovers-sm">
                                    <i class="icn-close popovers__close" data-place="left"></i>
                                    <h3 class="popovers__title">Class “Rock Climbing for Kids”</h3>
                                    <p class="popovers__text">
                                        Kids 7-10 years old, all day since 9AM to 6PM skill level is not required (you have never engaged in rock climbing before)
                                    </p>
                                    <table class="popovers__table">
                                        <tr>
                                            <td>
                                                <strong>Seat available</strong>
                                            </td>
                                            <td>4</td>
                                        </tr>
                                        <tr>
                                            <td>Price</td>
                                            <td>
                                                <strong>$200 per month</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Date</strong>
                                            </td>
                                            <td>1 May, 2019</td>
                                        </tr>
                                    </table>
                                    <form action="">
                                        <div class="row popovers__priceBox">
                                            <div class="col-6">
                                                <div class="field-case">
                                                    <label class="field-name">Number</label>
                                                    <input class="field js-price" data-price="200" min="1" name="" type="number" value="1">
                                                    <div class="increment">
                                                        <div class="up"></div>
                                                        <div class="down"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="popovers__price">
                                                    <strong class="js-priceOutput">$200</strong>
                                                    per month
                                                </div>
                                            </div>
                                        </div>
                                        <input class="btns btns-primary btns-block" type="submit" value="Checkout">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="schedulesPricesMap eventBox__box" href="">
                        <div class="schedulesPricesMap__boxText">
                            <div class="schedulesPricesMap__title">42 more variants</div>
                        </div>
                        <i class="icn-angle-right"></i>
                    </a>

                    <a class="btns btns-primary eventBox__btn" href="">More Info</a>
                </div>            
            `)
            pointId = features[0].properties.id
        }
        moduleNumberInput.initPopovers()
        modulePopovers.init()
        $elem.addClass('active')

        
        let x = 0;  
        if(window.innerWidth > 767){
            x = 200
        }

        map.flyTo({
            center: features[0].geometry.coordinates,
            offset: [x, 0],
        });
    });

    
    let popup = new mapboxgl.Popup({ 
            offset: 15,
            closeButton: false,
            
        })

    
    map.on('mouseenter', 'unclustered-point', function (e) {
        var features = map.queryRenderedFeatures(e.point, { layers: ['unclustered-point'] });
        let data = JSON.parse(features[0].properties.message)
        
        map.getCanvas().style.cursor = 'pointer'
        popup.setLngLat(features[0].geometry.coordinates)
           .setHTML(`
                   <div class="mapPopup">
                       <div class="mapPopup__title">${data.title}</div>
                       <div class="mapPopup__price">${data.price}</div>
                       <div class="mapPopup__age">${data.age}</div>  
                   </div>
               `)
           .addTo(map);

    });

    map.on('mouseout', 'unclustered-point', function (e) {
        map.getCanvas().style.cursor = '';
        popup.remove();

    });


    
    
    map.on('mouseenter', 'clusters', function () {
        map.getCanvas().style.cursor = 'pointer';
    });
    map.on('mouseleave', 'clusters', function () {
        map.getCanvas().style.cursor = '';
    });

    $('.js-closeMoreInfo').click(function(e) {
        let $elem = $(this).parents(".moreInfo")
        let $container = $elem.find(".moreInfo__content")
        $container.html("")
        $elem.removeClass('active')
        pointId = ""
    });

});

}

