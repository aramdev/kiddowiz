var moduleToggleMenu = {
    cacheDom() {
        this.$btn = $('.js-toggleMenu')
    },
    toggleMenu (e) {
        if($(e.currentTarget).hasClass('open')){
            $(e.currentTarget).removeClass("open"); 
        }else{
            this.$btn.removeClass('open')
            $(e.currentTarget).addClass('open')
        }
    },    
    events () {
        this.$btn.on('click', this.toggleMenu.bind(this));
    },
    init () {
        this.cacheDom()
        this.events()
    }
}
if($('.js-toggleMenu').length > 0){
    moduleToggleMenu.init()
}

