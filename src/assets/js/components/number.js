var moduleNumberInput = {
    cacheDom  () {
        this.$input = $('[type="number"]')
        this.$increment = $('[type="number"] ~ .increment')
        this.$up = this.$increment.find('.up')
        this.$down = this.$increment.find('.down')

    },
    cacheDomPopovers  () {
        this.$input = $('.popovers [type="number"]')
        this.$increment = $('.popovers [type="number"] ~ .increment')
        this.$up = this.$increment.find('.up')
        this.$down = this.$increment.find('.down')
    },
	incUp (e) {
		let $el = $(e.currentTarget).parents('.field-case').find('[type="number"]')
		let max = $el.attr('max') || Infinity;
		let val = $el.val() || 1;
		val = parseInt(val);
		if(val < max){
			val++
		}
		$el.val(val)
		$el.trigger('input')
		$el.trigger('change')
		
	},
	incDown (e) {
		let $el = $(e.currentTarget).parents('.field-case').find('[type="number"]')
		let min = $el.attr('min') || -Infinity;
		let val = $el.val() || 1;
		val = parseInt(val);
		if(val > min){
			val--
		}
		$el.val(val)
		$el.trigger('input')
		$el.trigger('change')
		
	},
	onlyNum (e) {
		let $el = $(e.currentTarget)
		let max = $el.attr('max') || Infinity;
		let min = $el.attr('min') || -Infinity;
		let val = $el.val() || 1;
		$el.val($el.val().replace(/[^\d].+/, ""));
		if ((e.which < 48 || e.which > 57)) {
            e.preventDefault();
        }

        val = parseInt(val);
		if(val > max){
			$el.val(max)
			return false;
		}
		if(val < min){
			$el.val(min)
			return false;
		}

	},
	events () {
    	this.$up.on('click', this.incUp.bind(this));
		this.$down.on('click', this.incDown.bind(this));
		this.$input.on('keypress keyup blur change', this.onlyNum.bind(this));
		this.$input.on('change', function(){
			$(this).valid()
		});
	},
    init () {
        this.cacheDom()
        this.events()
    },
    initPopovers () {
        this.cacheDomPopovers()
        this.events()
    }
}
if($('[type="number"]').length > 0){
    moduleNumberInput.init()
}



var moduleNumber = {
    cacheDom  () {
        this.$input = $('.number')
        this.$increment = $('.number ~ .increment')
        this.$up = this.$increment.find('.up')
        this.$down = this.$increment.find('.down')

    },
    cacheDomPopovers  () {
        this.$input = $('.popovers .number')
        this.$increment = $('.popovers .number ~ .increment')
        this.$up = this.$increment.find('.up')
        this.$down = this.$increment.find('.down')
    },
	incUp (e) {
		let $el = $(e.currentTarget).parents('.field-case').find('.number')
		let max = $el.attr('max') || Infinity;
		let val = $el.val() || 0;
		val = parseInt(val);
		if(val < max){
			val++
		}
		$el.val(val)
		$el.trigger('input')
		$el.trigger('change')
		
	},
	incDown (e) {
		let $el = $(e.currentTarget).parents('.field-case').find('.number')
		let min = $el.attr('min') || -Infinity;
		let val = $el.val() || 0;
		val = parseInt(val);
		if(val > min){
			val--
		}
		$el.val(val)
		$el.trigger('input')
		$el.trigger('change')
		
	},
	onlyNum (e) {
		let $el = $(e.currentTarget)
		let max = $el.attr('max') || Infinity;
		let min = $el.attr('min') || -Infinity;
		let val = $el.val() || 0;
		let float = $el.attr('data-float') || false
		if(float == false){
			$el.val($el.val().replace(/[^\d].+/, ""));
		}else{
			$el.val($el.val().replace("^[0-9.]*$", ""));

		}
		/*if ((e.which < 48 || e.which > 57)) {
            e.preventDefault();
        }*/

        val = parseInt(val);
		if(val > max){
			$el.val(max)
			return false;
		}
		if(val < min){
			$el.val(min)
			return false;
		}

	},
	events () {
    	this.$up.on('click', this.incUp.bind(this));
		this.$down.on('click', this.incDown.bind(this));
		this.$input.on('keypress keyup blur change', this.onlyNum.bind(this));
		this.$input.on('change', function(){
			$(this).valid()
		});
	},
    init () {
        this.cacheDom()
        this.events()
    },
    initPopovers () {
        this.cacheDomPopovers()
        this.events()
    }
}
if($('.number').length > 0){
    moduleNumber.init()
}