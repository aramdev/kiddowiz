var moduleSocial = {
    cacheDom() {
        this.$btn = $('.js-social a')
    },
	showWin (e) {
        let $el = $(e.currentTarget)
        let win = $el.attr('data-href');
        window.open(win, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
    },
    events () {
        this.$btn.on('click', this.showWin.bind(this))
    },
    init () {
        this.cacheDom()
        this.events()
    }
}
if($('.js-social').length > 0){
    moduleSocial.init()
}