var moduleMultiSelect = {
    cacheDom() {
        this.$select = $('.js-select-multi')
    },
	selectInit () {
		this.$select.each(function () {
            $(this).select2({
                dropdownParent: $(this).parents(".field-case"),
                data: [],
                placeholder: 'Select One...',
                allowClear: false,
                minimumResultsForSearch: 20
            }).on("change", function (e) {
                $(this).valid(); //jquery validation script validate on change
            })
        });
	},    
    events () {},
    init () {
        this.cacheDom()
        this.selectInit()
    }
}
if($('.js-select-multi').length > 0){
    moduleMultiSelect.init()
}