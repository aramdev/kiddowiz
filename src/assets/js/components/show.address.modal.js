function  checkInView(container, elem, partial)
{
    var container = container;
    var contHeight = container.height();
    var contTop = container.scrollTop();
    var contBottom = contTop + contHeight ;
 
    var elemTop = $(elem).offset().top - container.offset().top;
    var elemBottom = elemTop + $(elem).height();
    
    var isTotal = (elemTop >= 0 && elemBottom <=contHeight);
    var isPart = ((elemTop < 0 && elemBottom > 0 ) || (elemTop > 0 && elemTop <= container.height())) && partial ;
    
    return  isTotal  || isPart ;
}


var geojson = {
    "type": "FeatureCollection",
    "name": "Advanture Out",
    "features": [
        {
            "type": "Feature",
            "properties": {
                "id": "ak16994521",
                "message": {
                    "title": 'Advanture Out 3',
                    "price": '$160-$320',
                    "age": 'age: 8-14',
                    "address": "175 Hilpert Square Apt. 059"
                },
                //"iconSize": [60, 60]
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -100.017, 45.457
                ]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "id": "ak16994522",
                "message": {
                    "title": 'Advanture Out 1',
                    "price": '$160-$320',
                    "age": 'age: 8-15',
                    "address": "45 Wilburn Groves Suite 004"
                },
                //"iconSize": [50, 50]
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                   -90.017, 32.457
                ]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "id": "ak16994523",
                "message": {
                    "title": 'Advanture Out 6',
                    "price": '$160-$320',
                    "age": 'age: 8-15',
                    "address": "18 Jensen Drives"
                },
                //"iconSize": [50, 50]
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                   -91, 33
                ]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "id": "ak16994524",
                "message": {
                    "title": 'Advanture Out 2',
                    "price": '$160-$320',
                    "age": 'age: 8-16',
                    "address": "175 Hilpert Square Apt. 21"
                },
                //"iconSize": [40, 40]
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -111.017, 39.457
                ]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "id": "ak16994525",
                "message": {
                    "title": 'Advanture Out 2',
                    "price": '$160-$320',
                    "age": 'age: 8-16',
                    "address": "18 Jensen Drives2600 Airport Way S, Seattle, WA 98134"
                },
                //"iconSize": [40, 40]
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -117.017, 39.457
                ]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "id": "ak16994526",
                "message": {
                    "title": 'Advanture Out 3',
                    "price": '$160-$320',
                    "age": 'age: 8-14',
                    "address": "175 Hilpert Apt. 059",
                },
                //"iconSize": [60, 60]
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -103.017, 35.457
                ]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "id": "ak16994527",
                "message": {
                    "title": 'Advanture Out 1',
                    "price": '$160-$320',
                    "age": 'age: 8-15',
                    "address": "45 Groves Suite 004"
                },
                //"iconSize": [50, 50]
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                   -92.017, 36.457
                ]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "id": "ak16994528",
                "message": {
                    "title": 'Advanture Out 6',
                    "price": '$160-$320',
                    "age": 'age: 8-15',
                    "address": "18 Jensen Jenn Drives"
                },
                //"iconSize": [50, 50]
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                   -93, 34.46
                ]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "id": "ak16994529",
                "message": {
                    "title": 'Advanture Out 2',
                    "price": '$160-$320',
                    "age": 'age: 8-16',
                    "address": "11 Hilpert Square Apt. 059"
                },
                //"iconSize": [40, 40]
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -121.017, 39.457
                ]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "id": "ak16994530",
                "message": {
                    "title": 'Advanture Out 2',
                    "price": '$160-$320',
                    "age": 'age: 8-16',
                    "address": "18 Jensen Airport Way, WA 98134"
                },
                //"iconSize": [40, 40]
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    -117.017, 44.457
                ]
            }
        }
    ]
};


var moduleShowAddressModal = {
    cacheDom() {
        this.$btn = $('.js-showAddressModal')
        this.$modal = $('#showAddressModal')
        this.$toggleBtn = this.$modal.find('.js-toggleSearch')
        this.$scContent = this.$modal.find('.searchByAddress__address')
        this.$input = this.$modal.find('.js-filterAddress')
        this.map = "";
    },
	showModal (e) {
        this.$modal.modal('show')
    },
    setMap () {
        this.$scContent.trigger('scroll')
    }, 
    setMarkers (markers){
        $(".modals__map.ext").prepend('<div class="loader-circle"></div>') 

        let output = [];
        
        markers.forEach(function(marker) {
            let result = geojson.features.filter(point => point.properties.message.address == marker);
            output.push(result[0])
        })

        var center;
        if(output.length > 0){
            center = output[0].geometry.coordinates
        }else{
            center = [-100.017, 45.457]
        }

        

        this.map = new mapboxgl.Map({
            container: 'mapAddress',
            style: 'mapbox://styles/mapbox/streets-v9',
            center: center,
            zoom: 3,
        });
        
        this.map.addControl(new mapboxgl.NavigationControl());
       
        let map = this.map
        this.map.on('load', function() {

            let x = 0;  
            if(window.innerWidth > 767){
                x = 150
            }

            map.flyTo({
                center: center,
                offset: [x, 0],
            });

            $(".modals__map.ext").find('.loader-circle').remove()
            
            
            
            if(output.length > 0){
                output.forEach(function(marker, index) {
                    var el = document.createElement('div');
                    el.className = 'marker';
                    if(index == 0){
                        el.className = 'marker active';
                        //el.style.width =  '17.5px';
                        //el.style.height = '25px';
                    }else{
                        el.className = 'marker';
                        //el.style.width =  '17.5px';
                        //el.style.height = '25px';
                    }
                    $(el).attr('data-address', marker.properties.message.address);
                    
                    let msg = marker.properties.message
                    var popup = new mapboxgl.Popup({ 
                            offset: 25,
                            closeButton: true,
                            
                        })
                    
                     
                    el.addEventListener('click', (e) => {
                        $('.mapboxgl-popup-close-button').trigger('click')
                        popup.setLngLat(marker.geometry.coordinates)
                          .setHTML(`
                                  <div class="mapPopup">
                                      <div class="mapPopup__title">${msg.title}</div>
                                      <div class="mapPopup__address">${msg.address}</div>
                                      <a class="btns btns-block btns-sm btns-primary href="">
                                           Learn More
                                      </a>     
                                  </div>
                              `)
                          .addTo(map);
                        e.stopPropagation()
                    });

                    new mapboxgl.Marker(el)
                        .setLngLat(marker.geometry.coordinates)
                        .addTo(map);
                });
            }




        })
    },
    toggleSearch(e){
        let $el = $(e.currentTarget)
        $el.parents('.searchByAddress').toggleClass('active');
    },
    getAddress(e) {
        $el = $(e.currentTarget)
        let showItems = [];
        let $span = $el.find('.item');
        let $this = this
        $span.removeClass('active')
        addActive = false
        $.each($span, (i, e) => {
            if(checkInView($el, $(e), false)){
                if(addActive == false){
                    $(e).addClass('active')
                    addActive = true
                }
                showItems.push($(e).text().trim())
            }
        });
        
        this.setMarkers(showItems);
    },
    setAddresses (address) {
        let addressList = this.$modal.find('.searchByAddress__address');
        addressList.html("")
        let add = address || geojson.features

        this.$modal.find('.modals__title').text(geojson.name)
        add.forEach(function(marker, index) {
            addressList.append(
                    `<span class="item" data-pos="${marker.geometry.coordinates}">
                        <i class="icn-map"></i>
                        <span>${marker.properties.message.address}<span>
                    </span>`);
        });
    },
    filterAddresses(e){
        let $el = $(e.currentTarget)
        let text = $el.val().toLowerCase()
        let output = []; 

        geojson.features.forEach(function(point) {
            let address = point.properties.message.address.toLowerCase()
            let flag = address.search(text);
            if(flag >= 0){
                output.push(point)
            }
            
        })

        this.setAddresses(output)
        this.$scContent.trigger('scroll')
    },
    showModalMap () {
        this.setAddresses()
        this.setMap()
    },
    hideModalMap(){
        this.$modal.find('.modals__title').html("")
        this.$modal.find('.searchByAddress__address').html("");
        this.$modal.find('#mapAddress').html("")
        this.$input.val("")
    },
    addActivePoint(e){
        $(".modals__map .searchByAddress__address .item").removeClass('active')
        let $el = $(e.currentTarget)
        $el.addClass('active')
        let text = $el.find('span').text().trim()
        let pos = $el.attr('data-pos'); 
        pos = pos.split(',')
        
        let x = 0;  
        if(window.innerWidth > 767){
            x = 150
        }

        this.map.flyTo({
            center: pos,
            offset: [x, 0],
        });
        
        $('#mapAddress').find('.marker').removeClass('active')
        $('#mapAddress').find(`.marker[data-address="${text}"]`).addClass('active')
    },
    events () {
        this.$btn.on('click', this.showModal.bind(this));
        this.$modal.on('shown.bs.modal', this.showModalMap.bind(this));
        this.$modal.on('hidden.bs.modal', this.hideModalMap.bind(this));
        this.$toggleBtn.on('click', this.toggleSearch.bind(this));
        this.$scContent.on('scroll', this.getAddress.bind(this));
        this.$input.on('input', this.filterAddresses.bind(this));
        this.$scContent.on('click', '.item', this.addActivePoint.bind(this));
    },
    init () {
        this.cacheDom()
        this.events()
    }
}
if($('.js-showAddressModal').length > 0){
    moduleShowAddressModal.init()
}

