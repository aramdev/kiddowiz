var moduleSettings = {
    cacheDom() {
        this.call = $(".js-call")
        this.change = $(".js-change")
        this.remove = $(".js-remove")
        this.send = $(".js-sendMail")
        this.$form = $("#dellAcc")
        this.$modal = $('#dellAccModal')
    },
    callTrigger(e){
        this.change.trigger('click')
    },
    addImg(e){
        let $el = $(e.currentTarget)
        let val =  $el.val()
        val = val.split(".")
        let item = val.length - 1
        
        let $parent = $el.parents('.settings__img')
        
        if(val[item] == 'jpg' || val[item] == 'png' || val[item] == 'gif'){
            let url = URL.createObjectURL(e.target.files[0])
            if($parent.find('img').length <= 0){
                $parent.find('.img').prepend(`<img src="${url}"/>`)
            }else{
                $parent.find('img').attr('src', url);
            }
            this.call.trigger('blur')
            $parent.find('.error').text('')
        }else{
           $parent.find('.error').text('You can add only .jpg, .png, .gif')
           $el.val("")
        }  

           
    },
    removeImg(e){
        let $el = $(e.currentTarget)
        let $parent = $el.parents('.settings__img')
        $parent.find('img').remove()
        this.change.val("")
        $parent.find('.error').text('')
    },
    sendMail(e){
        let $el = $(e.currentTarget)
        let $parent = $el.parents('.alert')
        $parent.html(`
            An email with a confirmation link 
            was sent to the specified address. 
            Please check you mailbox.
        `)
        notific.setNote('icn-success',
                        'Lorem ipsum dolor sit amet',
                        'reviewInfo-success')
    },
    dellAcc (e) {
        e.preventDefault()
        let $el = $(e.currentTarget)
        if($el.valid()){
            this.$modal.one('hidden.bs.modal', function(){
                $('#dellAccConfModal').modal('show')
            })
            this.$modal.modal('hide')
        }
    },
    events () {
        this.call.on("click", this.callTrigger.bind(this))
        this.change.on("change", this.addImg.bind(this))
        this.remove.on("click", this.removeImg.bind(this))
        this.send.on("click", this.sendMail.bind(this))
        this.$form.on("submit", this.dellAcc.bind(this))
    },
    init () {
        this.cacheDom()
        this.events()
    }
}
if($('.settings__img').length > 0){
    moduleSettings.init()
}

