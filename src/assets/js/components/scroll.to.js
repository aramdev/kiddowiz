var moduleScrollTo = {
    cacheDom() {
        this.$btn = $('[data-scroll]');
        this.$body = $('html, body');
        this.$head = $('.header');
    },
	scrollTo (e) { 
        let $el = $(e.currentTarget)
        let itemId = $el.attr('data-scroll'); 
        let head = this.$head.outerHeight();
        let pos = $(itemId).offset().top - head - 10
        this.$body.animate({scrollTop:pos+'px'}, 700);
    },    
    events () {
        this.$btn.on('click', this.scrollTo.bind(this));
    },
    init () {
        this.cacheDom()
        this.events()

    }
}
if($('[data-scroll]').length > 0){
    moduleScrollTo.init()
}



var moduleScrollReady = {
    cacheDom() {
        this.$body = $('html, body');
        this.$head = $('.header');
    },
    detectHash () { 
        if(window.location.hash){
            let itemId = window.location.hash; 
            let head = this.$head.outerHeight();
            let pos = $(itemId).offset().top - head - 10
            this.$body.animate({scrollTop:pos+'px'}, 700);    
        }
    },    
    init () {
        this.cacheDom()
        this.detectHash()
    }
}

window.onload = function(e){ 
   moduleScrollReady.init()
}
