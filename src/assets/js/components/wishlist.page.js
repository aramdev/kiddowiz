var moduleWishlistPage = {
    cacheDom() {
        this.$body = $('body')
        
        this.loadEventBtn  = $(".js-loadEvent")
        this.loadProcessEvent = false;
        this.iteratorEvent = 0

        this.loadEnrollmentBtn  = $(".js-loadEnrollment")
       	this.loadProcessEnroll = false;
       	this.iteratorEnroll = 0
    },
	removeEvent (e){
    	let $el = $(e.currentTarget)
        let $parent = $el.parents('.wishlistPage__events');
        let count = $parent.find('.eventItem').length
        let more = $('.js-loadEvent').length

        let $events = $(".countEvent")
        let countEvent = parseInt($events.text())
        countEvent --
        $events.text(countEvent)


        if(count <= 1 && more <= 0){
            $parent.next('.wishlistPage__add.ext').remove()
            $parent.remove()
        }else{
            $el.parents(".eventItem").remove();
        }
        this.headCounter()
    },
	removeEnrollment (e){
		let $el = $(e.currentTarget)
        let $parent = $el.parents('.wishlistPage__enrollment');
        var count = $parent.find('.browsHisItem').length
        var more = $('.js-loadEnrollment').length
        
        let $enroll = $(".countEnroll")
        let countEnroll = parseInt($enroll.text())
        countEnroll --
        $enroll.text(countEnroll)

        if(count <= 1 && more <= 0){
            $parent.remove()
        }else{
            $el.parents(".browsHisItem").remove();
        }

        this.headCounter()
	},

	headCounter(){
		let $wish = $('.js-wishCount').first()
		let $wishs = $('.js-wishCount')

		let count = parseInt($wish.text())
		count--
		$wishs.text(count)
		if(count == 0){
			$wishs.parents('.wishlist').removeClass('active')
			$('.wishlistPage__content').html(`
				<div class="emptyContent">
					<i class="icn-wish-empty"></i>
					<div class="h4">No events in your wishlis</div>
					<p>Click the Heart Icon next to any event to add it to your Wish List</p>
					<a class="btns btns-primary" href="">Explore Event's Catalog</a>
				</div>
			`);
		}

	},
	loadEvent (e) {
		let $el = $(e.currentTarget)
		if(this.loadProcessEnroll == false){
            this.loadProcessEnroll = true
            $el.prepend('<span class="connecting"></span>')
            setTimeout(() => {
                $el.find('.connecting').remove()
                $('.wishlistPage__events').append(` 
                    <div class="eventItem eventsList__item">
						<a class="eventItem__img" href="">
							<img alt="" src="http://loremflickr.com/250/200" srcset="http://loremflickr.com/250/200 1x, http://loremflickr.com/500/400 2x">
						</a>
						<div class="eventItem__content">
							<div class="eventItem__info">
								<span class="dateAge">
									TODAY
									<i class="icn-radio"></i>
									AGE: 8-14
								</span>
								<a class="addWish eventItem__icon ext">
									<i class="icn-favorite js-removeEvent"></i>
								</a>
							</div>
							<div class="eventItem__title">
								<a href="">
									Rock Climbing for Kids and Teenagers
								</a>
							</div>
							<div class="eventItem__info">
								<a class="eventItem__label ext" href="">
									Adventure Out
								</a>
								<div class="rate">
									<i class="icn-star active"></i>
									<i class="icn-star active"></i>
									<i class="icn-star active"></i>
									<i class="icn-star"></i>
									<i class="icn-star"></i>
								</div>
								<a class="commentsCount eventItem__comments" href="">
									<i class="icn-comments"></i>
									3
								</a>
							</div>
							<div class="eventItem__text ext">
								This camp is designed for those kids ages 8-14 who want to experience the thrill of outdoor rock climbing. Each half-day (4-hour) class will be held at Castle Rock State Park in the Los Gatos/Santa Cruz Hills. No previous experience is necessary and advanced coaching also available for kids that have climbed before
							</div>
							<div class="eventItem__tags">
								<a class="tag" href="">Outdoor Activities</a>
								<a class="tag" href="">SPORT</a>
								<a class="tag" href="">TEAM SPORT</a>
							</div>
						</div>
					</div>
					<div class="eventItem eventsList__item">
						<a class="eventItem__img" href="">
							<img alt="" src="http://loremflickr.com/250/200" srcset="http://loremflickr.com/250/200 1x, http://loremflickr.com/500/400 2x">
						</a>
						<div class="eventItem__content">
							<div class="eventItem__info">
								<span class="dateAge">
									TODAY
									<i class="icn-radio"></i>
									AGE: 8-14
								</span>
								<a class="addWish eventItem__icon ext">
									<i class="icn-favorite js-removeEvent"></i>
								</a>
							</div>
							<div class="eventItem__title">
								<a href="">
									Rock Climbing for Kids and Teenagers
								</a>
							</div>
							<div class="eventItem__info">
								<a class="eventItem__label ext" href="">
									Adventure Out
								</a>
								<div class="rate">
									<i class="icn-star active"></i>
									<i class="icn-star active"></i>
									<i class="icn-star active"></i>
									<i class="icn-star"></i>
									<i class="icn-star"></i>
								</div>
								<a class="commentsCount eventItem__comments" href="">
									<i class="icn-comments"></i>
									3
								</a>
							</div>
							<div class="eventItem__text ext">
								This camp is designed for those kids ages 8-14 who want to experience the thrill of outdoor rock climbing. Each half-day (4-hour) class will be held at Castle Rock State Park in the Los Gatos/Santa Cruz Hills. No previous experience is necessary and advanced coaching also available for kids that have climbed before
							</div>
							<div class="eventItem__tags">
								<a class="tag" href="">Outdoor Activities</a>
								<a class="tag" href="">SPORT</a>
								<a class="tag" href="">TEAM SPORT</a>
							</div>
						</div>
					</div>
                `)
                this.loadProcessEnroll = false;
                this.iteratorEnroll ++
                if(this.iteratorEnroll > 2){
                	$el.remove()
                }
            }, 1000)
        }
	},
	loadEnrollment (e) {
		let $el = $(e.currentTarget)
		if(this.loadProcessEvent == false){
            this.loadProcessEvent = true
            $el.prepend('<span class="connecting"></span>')
            setTimeout(() => {
                $el.find('.connecting').remove()
                $('.wishlistPage__enrollment').append(` 
                    <div class="browsHisItem listItem">
						<a class="browsHisItem__img" href="">
							<img alt="" src="http://loremflickr.com/90/65" srcset="http://loremflickr.com/90/65 1x, http://loremflickr.com/180/130 2x">
						</a>
						<div class="browsHisItem__content">
							<h4 class="browsHisItem__title">
								<a href="">Rock Climbing for Kids and Teenagers 1</a>
							</h4>
							<a class="link link-primary" href="">
								Adventure Out
							</a>
						</div>
						<i class="icn-favorite js-removeEnrollment"></i>
					</div>
					<div class="browsHisItem listItem">
						<a class="browsHisItem__img" href="">
							<img alt="" src="http://loremflickr.com/90/65" srcset="http://loremflickr.com/90/65 1x, http://loremflickr.com/180/130 2x">
						</a>
						<div class="browsHisItem__content">
							<h4 class="browsHisItem__title">
								<a href="">Rock Climbing for Kids and Teenagers 2</a>
							</h4>
							<a class="link link-primary" href="">
								Adventure Out
							</a>
						</div>
						<i class="icn-favorite js-removeEnrollment"></i>
					</div>
                `)
                this.loadProcessEvent = false;
                this.iteratorEvent ++
                if(this.iteratorEvent > 2){
                	$el.remove()
                }
            }, 1000)
        }
	},
    events () {
        this.$body.on('click', '.js-removeEvent', this.removeEvent.bind(this));
        this.$body.on('click', '.js-removeEnrollment', this.removeEnrollment.bind(this));
        
        this.loadEventBtn.on('click', this.loadEvent.bind(this))
		this.loadEnrollmentBtn.on('click', this.loadEnrollment.bind(this))
    },
    init () {
        this.cacheDom()
        this.events()
    }
}
if($('.wishlistPage').length > 0){
   moduleWishlistPage.init()
}



