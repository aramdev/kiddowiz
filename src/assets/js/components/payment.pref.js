var modulePaymentPref = {
    cacheDom () {
        this.$body = $('body')

        this.$modalDD = $('#directDepositModal')
        this.$infoDD = $('#directDepositInfo')
        this.$formDD = $('#directDeposit')

        this.cachingDD = {
            bankName: "First Century Bank",
            routing: "123456789",
            accountNumber: "1234567890",
            accountType: "Checking",
            beneficiartName: "John Show",
        }


        this.$modalPP = $('#payPalModal')
        this.$infoPP = $('#payPalInfo')
        this.$formPP = $('#payPalForm')

        this.cachingPP = false /*{
            email: 'user@user.com'
        }*/


        this.$modalWT = $('#wireTransferModal')
        this.$infoWT = $('#wireTransferInfo')
        this.$formWT = $('#wireTransfer')

        this.cachingWT  = false /*{
            bankSwiftCode: 'xxxxxx',
            bankName: 'xxxxxx',
            accountCurrency: 'xxxxx',
            accountNumber: '1234567890',
            accounttype: 'Checking',
            fullName: 'xxxxxxx',
            nameAcc: 'xxxxxxx',
            address: 'xxxxxxx',
            location: 'xxxxxxx',
            phone: "+1 (546) 546-5465"
        }*/
        
    },
    addDD () {
        this.$modalDD.modal('show')
    },
    editDD () {
        this.$modalDD.find('input[type="submit"]').val('Save')
        this.$modalDD.modal('show')  


        this.$modalDD.find('[name="bank"]').val(this.cachingDD.bankName)
        this.$modalDD.find('[name="routing"]').val(this.cachingDD.routing)
        this.$modalDD.find('[name="accNumber"]').val(this.cachingDD.accountNumber)
        this.$modalDD.find('[name="accType"]').val(this.cachingDD.accountType).trigger('change')
        this.$modalDD.find('[name="beneficiary"]').val(this.cachingDD.beneficiartName)
    },
    submitDD (e) {
        e.preventDefault()
        let $el = $(e.currentTarget)
        if($el.valid()){


            this.cachingDD = {}
            this.cachingDD.bankName = this.$modalDD.find('[name="bank"]').val()
            this.cachingDD.routing = this.$modalDD.find('[name="routing"]').val()
            this.cachingDD.accountNumber = this.$modalDD.find('[name="accNumber"]').val()
            this.cachingDD.accountType = this.$modalDD.find('[name="accType"]').val()
            this.cachingDD.beneficiartName = this.$modalDD.find('[name="beneficiary"]').val()

            this.renderDD()

        
            this.$modalDD.modal('hide')  
        
            notific.setNote('icn-success',
                        'Your data has been successfully changed.',
                        'reviewInfo-success')
        }
    },
    renderDD(){
        if(this.cachingDD){
            this.$infoDD.html(`
                <div class="preferencesItem__title">Account data</div>
                <div class="payment__info">
                    <div class="item">
                        <span>Bank Name:</span>
                        <span>${this.cachingDD.bankName}</span>
                    </div>
                    <div class="item">
                        <span>Routing (ABA):</span>
                        <span>${this.cachingDD.routing}</span>
                    </div>
                    <div class="item">
                        <span>Account Number:</span>
                        <span>${this.cachingDD.accountNumber}</span>
                    </div>
                    <div class="item">
                        <span>Account Type:</span>
                        <span>${this.cachingDD.accountType}</span>
                    </div>
                    <div class="item">
                        <span>Beneficiart Name:</span>
                        <span>${this.cachingDD.beneficiartName}</span>
                    </div>
                </div>
                <a class="payment__edit js-editDD" tabindex="">
                    <i class="icn-edit"></i>
                    Edit Payment Account Data
                </a>
            `)
        }
    },
    addPP () {
        this.$modalPP.modal('show')
    },
    editPP () {
        this.$modalPP.find('input[type="submit"]').val('Save')
        this.$modalPP.modal('show')  
        this.$modalPP.find('[name="email"]').val(this.cachingPP.email)
    },
    submitPP (e) {
        e.preventDefault()
        let $el = $(e.currentTarget)
        if($el.valid()){
            
            let email = $el.find('[name="email"]').val()

            this.cachingPP = {
                email: email //'user@user.com',
            }

            this.renderPP()
            this.$modalPP.modal('hide')  
            notific.setNote('icn-success',
                        'Your data has been successfully changed.',
                        'reviewInfo-success')
        }
    },
    renderPP(){
        if(this.cachingPP){
            this.$infoPP.html(`
                <div class="preferencesItem__title">Account data</div>
                <div class="payment__info">
                    <div class="item">
                        <span>PayPal Email Address:</span>
                        <span>${this.cachingPP.email}</span>
                    </div>
                </div>
                <a class="payment__edit js-editPP" tabindex="">
                    <i class="icn-edit"></i>
                    Edit Payment Account Data
                </a>
            `)
        }
    },
    addWT () {
        this.$modalWT.modal('show')
    },
    getBySwift () {
        let $swift = this.$formWT.find('[name="swiftCode"]') 
        if($swift.val() !== "" && !$swift.hasClass('error')){
            // ADSDUS44
            $.ajax({
                url: `https://api.bank.codes/swift/?format=json&api_key=061b510c59d0f356f71a0bd83e8149ca&swift=${$swift.val()}`, 
                success: (result) => {
                    let $el = $('[data-address]')
                    $el.prepend('<div class="loader-circle"></div>')
                    $el.removeClass('hidden')

                    if(result.valid == "true"){
                        $swift.parents('#wireTransfer').find('[name="bankName"]').val(result.bank) 
                    }
                    $el.find('.loader-circle').remove() 

                    //console.log(result)
                    

                    
                }
            });
        }else{
            $swift.addClass('error')
            $swift.parents('.field-case').append('<label id="swiftCode-error" class="error" for="swiftCode">Please enter your Bank SWIFT Code.</label>')
        }
    }, 
    editWT () {
        this.$modalWT.find('input[type="submit"]').val('Save')
        this.$modalWT.modal('show')  
        $('[data-address]').removeClass('hidden')

        

        this.$modalWT.find('[name="swiftCode"]').val(this.cachingWT.bankSwiftCode)
        this.$modalWT.find('[name="bankName"]').val(this.cachingWT.bankName)
        this.$modalWT.find('[name="accCurrency"]').val(this.cachingWT.accountCurrency)
        this.$modalWT.find('[name="accNumber"]').val(this.cachingWT.accountNumber)
        this.$modalWT.find('[name="accType"]').val(this.cachingWT.accounttype).trigger('change')


        this.$modalWT.find('[name="fullName"]').val(this.cachingWT.fullName)
        this.$modalWT.find('[name="nameAcc"]').val(this.cachingWT.nameAcc)
        this.$modalWT.find('[name="address"]').val(this.cachingWT.address)
        this.$modalWT.find('[name="location"]').val(this.cachingWT.location)
        this.$modalWT.find('[name="phone"]').val(this.cachingWT.phone)
    },
    submitWT (e) {
        e.preventDefault()
        let $el = $(e.currentTarget)
        if($el.valid()){


            this.cachingWT = {}
            this.cachingWT.bankSwiftCode = this.$modalWT.find('[name="swiftCode"]').val()
            this.cachingWT.bankName = this.$modalWT.find('[name="bankName"]').val()
            this.cachingWT.accountCurrency = this.$modalWT.find('[name="accCurrency"]').val()
            this.cachingWT.accountNumber = this.$modalWT.find('[name="accNumber"]').val()
            this.cachingWT.accounttype = this.$modalWT.find('[name="accType"]').val()

                                        
            this.cachingWT.fullName = this.$modalWT.find('[name="fullName"]').val()
            this.cachingWT.nameAcc = this.$modalWT.find('[name="nameAcc"]').val()
            this.cachingWT.address = this.$modalWT.find('[name="address"]').val()
            this.cachingWT.location = this.$modalWT.find('[name="location"]').val()
            this.cachingWT.phone = this.$modalWT.find('[name="phone"]').val()


            this.renderWT()

        
            this.$modalWT.modal('hide')  
        
            notific.setNote('icn-success',
                        'Your data has been successfully changed.',
                        'reviewInfo-success')
        }
    },
    renderWT(){
        if(this.cachingWT){
            this.$infoWT.html(`
                <div class="preferencesItem__title">Account data</div>
                <div class="payment__info">
                    <div class="item">
                        <span>Bank SWIFT Code:</span>
                        <span>${this.cachingWT.bankSwiftCode}</span>
                    </div>
                    <div class="item">
                        <span>Bank Name:</span>
                        <span>${this.cachingWT.bankName}</span>
                    </div>
                    <div class="item">
                        <span>Account Currency:</span>
                        <span>${this.cachingWT.accountCurrency}</span>
                    </div>
                    <div class="item">
                        <span>Account Number:</span>
                        <span>${this.cachingWT.accountNumber}</span>
                    </div>
                    <div class="item">
                        <span>Account type:</span>
                        <span>${this.cachingWT.accounttype}</span>
                    </div>
                </div>
                <a class="payment__edit js-editWT" tabindex="">
                    <i class="icn-edit"></i>
                    Edit Payment Account Data
                </a>
            `)
        }
    },
    detectGet(){
        var url_string = window.location.href
        var url = new URL(url_string);
        var btn = url.searchParams.get("btn");
        if(btn){
            $(`.js-${btn}`).trigger('click')
        }
    },
    events () {
        this.$body.on('click', '.js-addDD', this.addDD.bind(this));
        this.$body.on('click', '.js-editDD', this.editDD.bind(this));
        this.$body.on('submit', '#directDeposit', this.submitDD.bind(this));


        this.$body.on('click', '.js-addPP', this.addPP.bind(this));
        this.$body.on('click', '.js-editPP', this.editPP.bind(this));
        this.$body.on('submit', '#payPalForm', this.submitPP.bind(this));



        this.$body.on('click', '.js-addWT', this.addWT.bind(this));
        this.$body.on('click', '.js-editWT', this.editWT.bind(this));
        this.$body.on('submit', '#wireTransfer', this.submitWT.bind(this));
        this.$body.on('click', '.jsGetSw', this.getBySwift.bind(this));
    },
    init () {
        this.cacheDom()
        this.renderDD()
        this.renderPP()
        this.renderWT()
        this.events()
        this.detectGet()
    }
}
if($('.paymentPref').length > 0){
    modulePaymentPref.init()
}





