if($('#pleaseSingIn').length > 0){
	var feedbackModule = new Vue({
		el: '#pleaseSingIn',
		data: {
			method: 'new'
		},
		methods: {
			changeModals () {
				$("#pleaseSingInModal").one('hidden.bs.modal', function () {
					$("#forgotModal").modal('show')
			    })
			    $("#pleaseSingInModal").modal('hide')
			} 
		},
	})
}