var modulePopovers = {
    cacheDom() {
        this.$btn = $('.js-popovers');
        this.$price = $('.js-price');
        this.$popovers = $('.popovers');
        this.$html = $('html');
        
    },
    addPopover (e) {
        let $el = $(e.currentTarget)
        let $content = $el.find('.popovers')
        $el.popover({
            content: $content,
            container: 'body',
            placement: function (context, source) {
                if (window.innerWidth < 768){
                    return "bottom";
                }
                return "right";
            }
        }).on('shown.bs.popover', function () {
            var $popup = $(this).attr('aria-describedby');
            $(`[id="${$popup}"]`).find('.popovers__close').click(function(event) {
                $(this).parents('.popover').popover('hide')
            });
        }).on('show.bs.popover', function(){
            $('.popover').popover('hide')
        })
    },    
    hideAllPopovers (e){
        if (typeof $(e.target).data('original-title') == 'undefined' &&
            !$(e.target).parents().is('.popover.in')) {
            $('[data-original-title]').popover('hide');
            this.$price.val(1).trigger('change')
        }
    },
    pricing (e) {
        let $el = $(e.currentTarget)
        let $output = $el.parents('.popovers__priceBox').find('.js-priceOutput');
        let price = parseInt($el.attr('data-price'));
        let val = parseInt($el.val()) || 1;
        let result = '$' + (price * val)

        $output.text(result)
    },
    events () {
        this.$btn.on('mouseover', this.addPopover.bind(this));
        this.$html.on('click', this.hideAllPopovers.bind(this));
        this.$popovers.on('click', function(e){e.stopPropagation()});
        this.$price.on('change input', this.pricing.bind(this));
    },
    init () {
        this.cacheDom()
        this.events()
    }
}
if($('.js-popovers').length > 0){
   modulePopovers.init()
}


