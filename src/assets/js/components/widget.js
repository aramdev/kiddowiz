var moduleWidget = {
    cacheDom() {
        this.$carousel = $('.widget__slider')
    },
	runCorusel: function(){
        this.$carousel.each(function(index, el) {
            let items = $(this).attr('data-items');
            $(this).owlCarousel({
                loop:true,
                nav:true,
                dots: true,
                center: false,
                margin: 15,
                navText: ['<i class="icn-angle-left"></i>', '<i class="icn-angle-right"></i>'],
                items: items
            });    
        });
    },
    events () {},
    init () {
        this.cacheDom()
        this.runCorusel()
    }
}
if($('.widget__slider').length > 0){
    moduleWidget.init()

}