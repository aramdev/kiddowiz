var moduleAddWish = {
    cacheDom() {
        this.$btn = $('.js-addWish')
        this.$count = $('.js-wishCount');
    },
	addWish (e) { 
        let $el = $(e.currentTarget)
        if($el.hasClass('active')){
            $el.find('i').removeClass('icn-favorite').addClass('icn-favorite-o')
            $el.removeClass('active heartBeat')
            let num = this.$count.first().text()
            num = parseInt(num) 
            num--
            this.$count.text(num)
            if(num == 0){
                this.$count.parents('.wishlist').removeClass('active')    
            }
            
        }else{
            $el.find('i').removeClass('icn-favorite-o').addClass('icn-favorite')
            $el.addClass('active heartBeat')
            this.$count.parents('.wishlist').addClass('active')
            let num = this.$count.first().text()
            num = parseInt(num) 
            num++
            this.$count.text(num)
        }
    },    
    events () {
        this.$btn.on('click', this.addWish.bind(this));
    },
    init () {
        this.cacheDom()
        this.events()
    }
}
if($('.js-addWish').length > 0){
    moduleAddWish.init()
}

