var moduleSocNet = {
    cacheDom() {
        this.$item = $(".socNetItem");
        this.$connectBtn = this.$item.find('.js-connect')
        this.$disconnectBtn = this.$item.find('.js-disconnect')
        
    },

    connect(e){
        let $el = $(e.currentTarget)
        $parent = $el.parents('.socNetItem')
        $el.html('Connecting <span class="connecting"></span>')
        this.$item.css({"pointer-events" : "none"})

        setTimeout(() => {
            $parent.find('.socNetItem__body').addClass('active')
            $el.html('Connect')
            $el.hide()
            this.$item.css({"pointer-events" : "auto"})
            notific.setNote('icn-success',
                            'Уou are connecting. Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                            'reviewInfo-success')
        }, 1000)

    },
    disconnect(e){
        let $el = $(e.currentTarget)
        $parent = $el.parents('.socNetItem')
        $el.append('<span class="connecting"></span>')
        this.$item.css({"pointer-events" : "none"})

        setTimeout(() => {
            $parent.find('.socNetItem__body').removeClass('active')
            $el.find('.connecting').remove()
            $parent.find('.js-connect').show()
            this.$item.css({"pointer-events" : "auto"})
            
            notific.setNote('icn-success',
                            'Уou are disconnecting. Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                            'reviewInfo-success')
        }, 1000)

    },
    events () {
        this.$connectBtn.on('click', this.connect.bind(this));
        this.$disconnectBtn.on('click', this.disconnect.bind(this));
       
    },
    init () {
        this.cacheDom()
        this.events()
    }
}
if($('.socNetItem').length > 0){
    moduleSocNet.init()
}