class SlimeScroll {
    constructor (container) {
        this.$scrollBar = $(container);
        this.time
    }
    scrollInit () {
        let height = this.$scrollBar.attr('data-height');
        let $this = this
        if(isMobile == false){
            this.$scrollBar.slimScroll({
                height: height,
                alwaysVisible: true,
            }).on('scroll', () => {
                clearTimeout($this.time)
                $this.$scrollBar.siblings('.slimScrollBar').addClass('active')
                $this.time = setTimeout(() => {
                    $this.$scrollBar.siblings('.slimScrollBar').removeClass('active')
                }, 500)
            })
        }
    }
    init(){
        this.scrollInit() 
    }
}


if($('[data-scrollBar="slimeScroll"]').length > 0){
    $('[data-scrollBar="slimeScroll"]').each(function(index, el) {
        let itemId = $(this).attr('id');
        let scrollBar = new SlimeScroll(`#${itemId}`)
        scrollBar.init()
    });
}




