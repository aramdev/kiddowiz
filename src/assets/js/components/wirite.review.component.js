Vue.component('app-writerevue', {
    props: {
        type: {
            default: "new"
        },
        mini: {
            default: false
        },
        rate: {
            default: true
        },
        rating: {
            type: Number,
            default: 0
        },
        value: {
           defaut: '' 
        }
    },
    template: `
        <div class="writeReview" :class="{ active: isActive, mini: mini}">
            <form action="" class="writeReview__form">
                <div class="writeReview__rate" v-if="rate">
                    <span>Rating:</span>
                    <div class="rate">
                        <i class="icn-star"
                                v-for="(star, i) in stars" :key="i"
                                @click="setRateing(i)"
                                :class="{active : star.active}"
                            >
                        </i>
                    </div>
                    <input name="rate" type="hidden">
                </div>
                <div class="field-case">
                    <textarea class="resize writeReview__field" 
                            name="msg" 
                            :placeholder="placeholder" rows="3"
                            @focus="activated()"
                            v-model="value"

                        >
                    </textarea>
                </div>
                <div class="writeReview__btns">
                    <a class="link link-default" @click="deactivated($event)">Cancel</a>
                    <input class="btns btns-sm btns-primary" @click="send($event)" 
                        :class="{disabled : isDisabled}" type="button" :value="type == 'edit' ? 'Update' : 'Send'">
                </div>
            </form>
        </div>
        `,
    data(){
        return {
            isActive: false,
            stars: [
                {active: false},
                {active: false},
                {active: false},
                {active: false},
                {active: false},
            ]
        }
    },
    created() {
        for(let j = 0;  j < this.rating; j++){
            this.stars[j].active = true
        }
    },
    mounted(){
        autosize($('textarea.resize'))
    },
    methods: {
        activated(){
            this.isActive = true
        },
        deactivated(e){
            this.isActive = false
            if(this.type == 'new'){
                this.value = ""
                this.rating = 0
                $(e.currentTarget).parents('.review__content').find('.review__inputBox').removeClass('open')
            }else{
                $(e.currentTarget).parents('.review__content').find('.review__inputBoxEdit').removeClass('open')
                $(e.currentTarget).parents('.review__content').find('.review__text').removeClass('hidden')
            }
        },
        setRateing(i){
            this.rating = i + 1
            
        },
        send(e){
            this.deactivated(e) 
            //$(e.currentTarget).parents('.review__content').find('.review__inputBoxEdit').removeClass('open')
            //$(e.currentTarget).parents('.review__content').find('.review__text').removeClass('hidden')
        }
    },
    computed: {
        isDisabled(){
            if(this.value == '' || this.value == undefined){
                return true
            }else{
                return false
            }
        },
        placeholder(){
            return this.rate ? "Write a review..." : "Write a comment..."
        },
    },
    watch:{
        rating(){
            for(let j = 0;  j < this.stars.length; j++){
                this.stars[j].active = false
            }
            for(let j = 0;  j <= this.rating - 1; j++){
                this.stars[j].active = true
            }
        }
    }
});
