'use strict'

const gulp  = require('gulp');
const $     = require('gulp-load-plugins')();


module.exports = function(options) {
	return function(){
		return gulp.src(options.src)
	        .pipe($.include())
			.pipe($.cached(options.taskName))
	        //.pipe($.babel({
	        //    presets: ['@babel/env']
	        //}))
			//.pipe($.jsMinify())
	        .pipe(gulp.dest(options.dest))
	};
};