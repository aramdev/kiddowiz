'use strict'

const gulp = require('gulp');
const del  = require('del');
const $    = require('gulp-load-plugins')();

module.exports = function(options) {
	return function(){
		return gulp.src(options.src)
			.pipe($.sprite(
				'sprites.png', {
					imagePath: 'dist/img',
					cssPath: './src/sass/utils/',
					preprocessor: 'sass'
				}
			))
			.pipe(gulp.dest(options.dest))
			
	};
};    